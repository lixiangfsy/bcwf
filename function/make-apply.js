/*global Promise */
'use strict';
var logger = require('../function/logger')();
var mapPersoncd = require('../function/map-personcd');

var assembleProcessorinfo = require('../function/assemble-processorinfo');
var duplicateFile = require('../function/duplicate-file');
var proceed = require('../function/proceed');

var applyDao = require('../dao/apply-dao');
var dao = require('../dao/dao');
var flowDao = require('../dao/flow-dao');
var processDao = require('../dao/process-dao');
var taskDao = require('../dao/task-dao');

/**
 * 申請書を作成する
 * @param  {Object} req
 * @param  {string} req.applyFormCd 諸届CD
 * @param  {string} req.applyTitle  申請名称
 * @param  {string} req.userId      ユーザID
 * @param  {string} [req.personCd]  パーソンCD - わかるなら設定してもよい。その場合ユーザIDからの変換は行われない
 * @param  {string} req.accountCd   所属CD
 * @resolve {Object}
 */
module.exports = function (req) {
    return new Promise(function (resolve, reject) {
        var applyId,
            client,
            mapPersonCd,
            startTaskId;
        mapPersoncd(req)
            .then(function (mapPersonCdRes) {
                mapPersonCd = mapPersonCdRes.personCd;
                return dao.pool().connect();
            })
            .then(function (res) {
                client = res;
                return dao.prmsBegin(client);
            })
            .then(function () {
                return new Promise(function (resolve, reject) {// リクエスト情報の諸届CDから開始位置テーブルを検索する
                    flowDao.prmsSelectOFlowStart({
                        applyFormCd: req.applyFormCd,
                    }, client)
                        .then(function (prmsSelectOFlowStartRes) {
                            var i,
                                isCondition,
                                jsonCondition;
                            jsonCondition = {};

                            // Conditionと合致するレコードの判断する Condition例えば: 所属
                            // ToDo：将来ちゃんと実装する
                            isCondition = function () {
                                return true;
                            };
                            // 条件にヒットするものがなければ、「申請できない」で返す
                            for (i = 0; i < prmsSelectOFlowStartRes.rowCount; i += 1) {
                                jsonCondition = JSON.parse(prmsSelectOFlowStartRes.rows[i].condition);
                                if (isCondition(jsonCondition)) {
                                    break;
                                }
                            }
                            if (i < prmsSelectOFlowStartRes.rowCount) {
                                return {
                                    flowCd: prmsSelectOFlowStartRes.rows[i].flowcd,
                                    startProcessCd: prmsSelectOFlowStartRes.rows[i].processcd,
                                };
                            }
                            logger.warn('申請できません');
                            return Promise.reject();
                        })
                        .then(function (prmsGetStartProcessCdRes) {
                            return new Promise(function (resolve, reject) {
                                var applyDatetime,
                                    arr_orderno,
                                    arr_processcd,
                                    arr_taskid,
                                    now;
                                arr_orderno = [];
                                arr_processcd = [];
                                arr_taskid = [];
                                now = new Date();
                                applyDatetime = now.getFullYear() + '/' + ('0' + (now.getMonth() + 1)).slice(-2) + '/' + ('0' + now.getDate()).slice(-2) + ' ' + ('0' + now.getHours()).slice(-2) + ':' + ('0' + now.getMinutes()).slice(-2);

                                applyDao.prmsSelectApplyIdSeq(client)
                                    .then(function (prmsSelectApplyIdSeqRes) {
                                        applyId = prmsSelectApplyIdSeqRes.rows[0].applyid;
                                        return processDao.prmsSelectProcesscd({
                                            flowCd: prmsGetStartProcessCdRes.flowCd,
                                        }, client);
                                    })
                                    .then(function (prmsSelectProcesscdRes) {
                                        var i,
                                            promises;
                                        promises = [];
                                        for (i = 0; i < prmsSelectProcesscdRes.rowCount; i += 1) {
                                            promises.push(taskDao.prmsSelectTaskIdSeq(client));
                                            arr_processcd[i] = prmsSelectProcesscdRes.rows[i].processcd;
                                            arr_orderno[i] = prmsSelectProcesscdRes.rows[i].orderno;
                                        }
                                        return Promise.all(promises);
                                    })
                                    .then(function (prmsSelectTaskIdSeqRes) {
                                        var i;
                                        for (i = 0; i < prmsSelectTaskIdSeqRes.length; i += 1) {
                                            arr_taskid[i] = prmsSelectTaskIdSeqRes[i].rows[0].taskid;
                                            if (arr_processcd[i] === prmsGetStartProcessCdRes.startProcessCd) {
                                                startTaskId = arr_taskid[i];
                                            }
                                        }
                                        return applyDao.prmsInsertTApply({
                                            applyId: applyId,
                                            applyTitle: req.applyTitle,
                                            applyDatetime: applyDatetime,
                                            applyFormCd: req.applyFormCd,
                                            startTaskId: startTaskId,
                                            personCd: mapPersonCd,
                                            accountCd: req.accountCd,
                                        }, client);
                                    })
                                    .then(function () {
                                        var i,
                                            promises,
                                            prmsGenerateTask;
                                        promises = [];
                                        prmsGenerateTask = function (idx) {
                                            return new Promise(function (resolve, reject) {
                                                taskDao.prmsInsertTTask({
                                                    taskId: arr_taskid[idx],
                                                    applyId: applyId,
                                                    processCd: arr_processcd[idx],
                                                    startTaskId: startTaskId,
                                                    orderNo : arr_orderno[idx],
                                                }, client)
                                                    .then(function () {
                                                        return taskDao.prmsInsertRTaskProcessor({
                                                            taskId: arr_taskid[idx],
                                                            processCd: arr_processcd[idx],
                                                            personCd: mapPersonCd,
                                                            startTaskId: startTaskId,
                                                        }, client);
                                                    })
                                                    .then(function () {
                                                        return taskDao.prmsInsertTaskRAccountProcessor({
                                                            taskId: arr_taskid[idx],
                                                            processCd: arr_processcd[idx],
                                                        }, client);
                                                    })
                                                    .then(function () {
                                                        return taskDao.prmsInsertTaskRPositionProcessor({
                                                            taskId: arr_taskid[idx],
                                                            processCd: arr_processcd[idx],
                                                        }, client);
                                                    })
                                                    .then(function () {
                                                        return taskDao.prmsInsertTaskRRoleProcessor({
                                                            taskId: arr_taskid[idx],
                                                            processCd: arr_processcd[idx],
                                                        }, client);
                                                    })
                                                    .then(function () {
                                                        return taskDao.prmsInsertTaskRPersonTypeProcessor({
                                                            taskId: arr_taskid[idx],
                                                            processCd: arr_processcd[idx],
                                                        }, client);
                                                    })
                                                    .then(function () {
                                                        return taskDao.prmsInsertRTaskReference({
                                                            taskId: arr_taskid[idx],
                                                            processCd: arr_processcd[idx],
                                                        }, client);
                                                    })
                                                    .then(function () {
                                                        return taskDao.prmsInsertTaskRAccountReference({
                                                            taskId: arr_taskid[idx],
                                                            processCd: arr_processcd[idx],
                                                        }, client);
                                                    })
                                                    .then(function () {
                                                        return taskDao.prmsInsertTaskRPositionReference({
                                                            taskId: arr_taskid[idx],
                                                            processCd: arr_processcd[idx],
                                                        }, client);
                                                    })
                                                    .then(function () {
                                                        return taskDao.prmsInsertTaskRRoleReference({
                                                            taskId: arr_taskid[idx],
                                                            processCd: arr_processcd[idx],
                                                        }, client);
                                                    })
                                                    .then(function () {
                                                        return taskDao.prmsInsertTaskRPersonTypeReference({
                                                            taskId: arr_taskid[idx],
                                                            processCd: arr_processcd[idx],
                                                        }, client);
                                                    })
                                                    .then(function () {
                                                        resolve();
                                                    })
                                                    .catch(function (err) {
                                                        if (err !== undefined) {
                                                            logger.error(err);
                                                        }
                                                        reject(err);
                                                    });
                                            });
                                        };
                                        for (i = 0; i < arr_processcd.length; i += 1) {
                                            promises.push(prmsGenerateTask(i));
                                        }
                                        return Promise.all(promises);
                                    })
                                    .then(function () {
                                        resolve();
                                    })
                                    .catch(function (error) {
                                        if (error !== undefined) {
                                            logger.error(error);
                                        }
                                        reject(error);
                                    });
                            });
                        })
                        .then(function () {
                            resolve();
                        })
                        .catch(function (error) {
                            if (error !== undefined) {
                                logger.error(error);
                            }
                            reject(error);
                        });
                });
            })
            .then(function () {
                return dao.prmsCommit(client);
            })
            .then(function () {
                client.release();

                //次タスクが自動タスクか否か判定する（処理者の有無の判定）
                return assembleProcessorinfo.prmsGetAllProcessorInfoByTaskId({
                    taskid: startTaskId,
                });
            })
            .then(function (prmsGetAllProcessorInfoByTaskIdRes) {
                logger.info(prmsGetAllProcessorInfoByTaskIdRes);
                if (prmsGetAllProcessorInfoByTaskIdRes.person.length === 0 &&
                        prmsGetAllProcessorInfoByTaskIdRes.account.length === 0 &&
                        prmsGetAllProcessorInfoByTaskIdRes.position.length === 0 &&
                        prmsGetAllProcessorInfoByTaskIdRes.role.length === 0 &&
                        prmsGetAllProcessorInfoByTaskIdRes.personType.length === 0
                        ) {
                    // スタートタスクが自動タスクの場合、proceedを実行する
                    return proceed({
                        applyId: applyId,
                        taskId: startTaskId,
                        personCd: mapPersonCd,
                        autoTaskFlag : true,
                    });
                }
                return Promise.resolve();
            })
            .then(function () {
                resolve({
                    applyId: applyId,
                });
            })
            .catch(function (error) {
                if (error !== undefined) {
                    logger.error(error);
                }
                if (client) {
                    dao.prmsRollback(client)
                        .then(function () {
                            logger.info('ロールバック成功');
                            client.release();
                        });
                }
                reject(error);
            });
    });
};
