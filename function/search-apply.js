/*global Promise */
'use strict';
var applyDao = require('../dao/apply-dao');

/**
 * ユーザーの情報を取得する
 * @param  {Object} req
 * @param  {string} req.userId
 * @param  {string} req.password
 * @resolve {Object}
 */
var selectActiveApplyListByCondition = function (req) {
    return applyDao.prmsSelectActiveApplyListByCondition(req);
};

exports.selectActiveApplyListByCondition = selectActiveApplyListByCondition;
