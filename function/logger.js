/*global module:true */
module.exports = function (category) {
    'use strict';
    var log4js;
    log4js = require('log4js');
    log4js.configure('./config/log4js.config.json');
    return log4js.getLogger(category);
};
