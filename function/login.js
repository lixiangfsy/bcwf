/*global Promise */
'use strict';
var useridDao = require('../dao/userid-dao');
var topDao = require('../dao/top-dao');

/**
 * ユーザーの情報を取得する
 * @param  {Object} req
 * @param  {string} req.userId
 * @param  {string} req.password
 * @resolve {Object}
 */
var userInfo = function (req) {
    return useridDao.prmsGetUserInfoByUserId(req);
};

/**
 * topの内容を取得する
 * @param  {Object} req
 * @resolve {Object}
 */
var prmsGetTop = function (req) {
    return topDao.prmsGetTop(req);
};

exports.prmsGetTop = prmsGetTop;
exports.userInfo = userInfo;
