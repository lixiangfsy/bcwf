/*global Promise */
'use strict';
var assembleProcessorinfo = require('../function/assemble-processorinfo');
var logger = require('../function/logger')();

var actionDao = require('../dao/action-dao');
var applyDao = require('../dao/apply-dao');
var dao = require('../dao/dao');
var flowDao = require('../dao/flow-dao');
var processDao = require('../dao/process-dao');
var processDataDao = require('../dao/process-data-dao');
var processMasterDao = require('../dao/process-master-dao');
var taskDao = require('../dao/task-dao');

/**
 * プロセスを次に進める
 * @param  {Object}     req
 * @param  {integer}    req.applyId 申請ID
 * @param  {integer}    req.taskId タスクID
 * @param  {integer}    req.personCd パーソンCD
 * @param  {integer}    req.autoTaskFlag 自動タスクFlag
 * @resolve {Object}
 */
module.exports = function (req) {
    return new Promise(function (resolve, reject) {
        var resProceedApply;
        resProceedApply = {};

        assembleProcessorinfo.prmsGetIntensivePersonProcessorByTaskId({
            taskid: req.taskId,
        })
            .then(function (prmsGetIntensivePersonProcessorByTaskIdRes) {
                var i;
                if (req.personCd !== undefined) {
                    for (i = 0; i < prmsGetIntensivePersonProcessorByTaskIdRes.length; i += 1) {
                        if (req.personCd === prmsGetIntensivePersonProcessorByTaskIdRes[i]) {
                            return Promise.resolve();
                        }
                    }
                }
                if (req.autoTaskFlag !== undefined && req.autoTaskFlag === true) {
                    return Promise.resolve();
                }
                logger.error('タスクを処理する権限がないためプロセスを次に進めることが出来ません。');
                return Promise.reject();
            })
            .then(function () {
                var arrayObjNextTask,
                    objNextTask;
                arrayObjNextTask = [];
                objNextTask = {
                    autoTaskFlag: false,
                };

                // ループ処理の完了を受け取るPromise
                return new Promise(function (resolve, reject) {
                    var execTaskLoop;
                    execTaskLoop = function (paramTask) {
                        var paramTaskId;
                        logger.info('loop start');
                        logger.info(paramTask);
                        if (paramTask !== undefined) {
                            paramTaskId = paramTask.taskId;
                            arrayObjNextTask = [];
                        }
                        return new Promise(function (resolve, reject) {
                            flowDao.prmsGetOFlowRProcessAfter({
                                applyId: req.applyId
                            })
                                .then(function (prmsGetOFlowRProcessAfterRes) {
                                    var actionName,
                                        client;
                                    // 自動タスクの場合、if文内の処理を実行する
                                    if (paramTask.autoTaskFlag === true) {
                                        resProceedApply.nextTasks = prmsGetOFlowRProcessAfterRes;
                                        return dao.pool().connect()
                                            .then(function (res) {
                                                client = res;
                                                return processDao.prmsUpdateActivestatus({
                                                    applyId: req.applyId,
                                                    taskId: paramTaskId,
                                                }, client);
                                            })
                                            .then(function () {
                                                logger.info('1');
                                                var i,
                                                    promises;
                                                promises = [];
                                                for (i = 0; i < resProceedApply.nextTasks.rowCount; i += 1) {
                                                    objNextTask.processCd = resProceedApply.nextTasks.rows[i].afterprocesscd;
                                                    objNextTask.taskId = resProceedApply.nextTasks.rows[i].taskid;
                                                    promises.push(processDao.prmsUpdateActivestatus({
                                                        applyId: req.applyId,
                                                        taskId: resProceedApply.nextTasks.rows[i].taskid
                                                    }, client));
                                                }
                                                return Promise.all(promises);
                                            })
                                            .then(function () {
                                                // プロセス名称・アクション名称の取得
                                                return processDao.prmsSelectProcessNameByTaskId({
                                                    taskId: paramTaskId,
                                                }, client);
                                            })
                                            .then(function (prmsSelectProcessNameByTaskIdRes) {
                                                // プロセスマスタの取得
                                                logger.info('2');
                                                if (prmsSelectProcessNameByTaskIdRes.rows[0].actionname !== undefined) {
                                                    actionName = prmsSelectProcessNameByTaskIdRes.rows[0].actionname;
                                                }
                                                return processMasterDao.prmsSelectOProcessMasterByTaskId({
                                                    taskId: paramTaskId,
                                                }, client);
                                            })
                                            .then(function (prmsSelectOProcessMasterByTaskIdRes) {
                                                // 自動タスクの実行
                                                logger.info('3');
                                                // ToDo プレーンタスク返すとエラーになるから今はプレーンタスクを実行しないようにしている
                                                if (prmsSelectOProcessMasterByTaskIdRes.rows[0] !== undefined && prmsSelectOProcessMasterByTaskIdRes.rows[0].sourcefilename !== 'task.js') {
                                                    return require('../task/' + prmsSelectOProcessMasterByTaskIdRes.rows[0].sourcefilename)({
                                                        applyId: req.applyId,
                                                        taskId: paramTaskId,
                                                        nextTasks: resProceedApply.nextTasks,
                                                    });
                                                }
                                            })
                                            .then(function () {
                                                //次タスクが自動タスクか否か判定する（処理者の有無の判定）
                                                var i,
                                                    promises;
                                                promises = [];
                                                logger.info('4');
                                                for (i = 0; i < resProceedApply.nextTasks.rowCount; i += 1) {
                                                    promises.push(assembleProcessorinfo.prmsGetAllProcessorInfoByTaskId({
                                                        taskid: resProceedApply.nextTasks.rows[i].taskid,
                                                    }, client));
                                                }
                                                return Promise.all(promises);
                                            })
                                            .then(function (prmsGetAllProcessorInfoByTaskIdRes) {
                                                var i;
                                                logger.info('5');
                                                for (i = 0; i < resProceedApply.nextTasks.rowCount; i += 1) {
                                                    arrayObjNextTask[i] = objNextTask;
                                                    if (prmsGetAllProcessorInfoByTaskIdRes[i].person.length === 0 &&
                                                            prmsGetAllProcessorInfoByTaskIdRes[i].account.length === 0 &&
                                                            prmsGetAllProcessorInfoByTaskIdRes[i].position.length === 0 &&
                                                            prmsGetAllProcessorInfoByTaskIdRes[i].role.length === 0 &&
                                                            prmsGetAllProcessorInfoByTaskIdRes[i].personType.length === 0
                                                            ) {
                                                        arrayObjNextTask[i].autoTaskFlag = true;
                                                    } else {
                                                        arrayObjNextTask[i].autoTaskFlag = false;
                                                    }
                                                }
                                                return Promise.resolve();
                                            })
                                            .then(function () {
                                                var now;
                                                now = new Date();
                                                logger.info('6');
                                                return actionDao.prmsInsertTAction({
                                                    taskId: paramTaskId,
                                                    personCd: req.personCd,
                                                    action: actionName || '',
                                                    saveDate: now.getFullYear() + '/' + ('0' + (now.getMonth() + 1)).slice(-2) + '/' + ('0' + now.getDate()).slice(-2) + ' ' + ('0' + now.getHours()).slice(-2) + ':' + ('0' + now.getMinutes()).slice(-2) + ':' + ('0' + now.getSeconds()).slice(-2),
                                                }, client);
                                            })
                                            .then(function () {
                                                logger.info('7');
                                                return dao.prmsCommit(client);
                                            });
                                    }
                                })
                                .then(function () {
                                    resolve();
                                })
                                .catch(function (error) {
                                    if (error !== undefined) {
                                        logger.error(error);
                                    }
                                    reject(error);
                                });
                        })
                            .then(function () {
                                //ループを抜けるかどうかの判定
                                if (arrayObjNextTask.length > 0) {
                                    // 再帰的に実行
                                    logger.info('LOOPもう一回');
                                    return execTaskLoop(arrayObjNextTask[0]);
                                }
                                return Promise.resolve();
                            });
                    };
                    // 初回実行
                    execTaskLoop({
                        autoTaskFlag: true,
                        taskId: req.taskId,
                    })
                        .then(function () {
                            logger.info('LOOP抜ける');
                            resolve();
                        })
                        .catch(function (error) {
                            if (error !== undefined) {
                                logger.error(error);
                            }
                            reject(error);
                        });
                });
            })
            .then(function () {
                logger.info('finish');
                resolve(resProceedApply);
            })
            .catch(function (error) {
                logger.info('Error and finish');
                if (error !== undefined) {
                    logger.error(error);
                }
                reject(error);
            });
    });
};
