/*global Promise */
'use strict';
var logger = require('../function/logger')();
var gadgetDao = require('../dao/gadget-dao');
var ejs = require('ejs');

var rsGadget;

/**
 * ガジェットを表示
 * @param  {Object} req
 * @param  {Object} req multipart/form-dataリクエスト
 * @return {Object} Promise
 */
var prmsGetGadget = function (req){
	return new Promise(function (resolve, reject) {
		gadgetDao.prmsGetGadgetInfo(req)
			.then(function (res) {
				if (res.rowCount === 0) {
	            	resolve();
	            } else {
	                resolve(res);
	            }
            })
            .catch(function (err) {
	            if (err !== undefined) {
	            	logger.error(err);
	            }
	            reject();
            });
	});
}

/**
 * 初期化ガジェット
 * @param  {Object} rows
 */
var setGadgets = function (rows){
	rsGadget = rows;
}


/**
 * ガジェットを取得する
 * @param  {string} key
 * @param  {string} style
 * @return {Object} 特定のRow情報
 */
var getInfo = function (key, style){
	var gadgetInfo = null;
	for (var i = 0; i < rsGadget.length; i++) {
		if(rsGadget[i].gadgetid === key){
			gadgetInfo = rsGadget[i];
			break;
		}
	}
	if(gadgetInfo != null){
		switch(style) {
	        case 1:
	            setPosition(gadgetInfo);
	            break;
	        case 2:
	            setSize(gadgetInfo);
	            break;
	        case 3:
	            setPositionAndSize(gadgetInfo);
	            break;
	        default:
	            break;
		}
	}
	return gadgetInfo;
}


/**
 * 場所を設定
 */
var setPosition = function (param){
	param.htmlcontent = ejs.render(param.htmlcontent, {position: param.position});
}

/**
 * 大きさを設定
 */
var setSize = function (param){
	param.htmlcontent = ejs.render(param.htmlcontent, {size: param.sizestyle});
}

/**
 * 場所大きさを設定
 */
var setPositionAndSize = function (param){
	param.htmlcontent = ejs.render(param.htmlcontent, {position: param.position, size: param.sizestyle});
}

exports.prmsGetGadget = prmsGetGadget;
exports.getInfo = getInfo;
exports.setGadgets = setGadgets;