/*global Promise */
'use strict';
var logger = require('../function/logger')();

var useridDao = require('../dao/userid-dao');

/**
 * 他システムのユーザIDと本システムのパーソンCDのひもづけ
 * @param  {Object} req
 * @param  {string} req.userId      ユーザID
 * @param  {string} [req.personCd]  パーソンCD - パーソンCDがあるとそのままパーソンCDを返す
 * @resolve {Object}
 */
module.exports = function (req) {
    return new Promise(function (resolve, reject) {
        if (req && req.personCd) {
            resolve({personCd: req.personCd});
        } else {
            useridDao.prmsSelectPersonCd(req)
                .then(function (queryRes) {
                    if (queryRes.rowCount === 0) {
                        logger.error('渡されたユーザIDに紐づく本システム側のパーソンコードが存在しません。');
                        reject();
                    } else {
                        resolve({personCd: queryRes.rows[0].personcd});
                    }
                })
                .catch(function (error) {
                    if (error !== undefined) {
                        logger.error(error);
                    }
                    reject();
                });
        }
    });
};
