/*global Promise */
(function () {
    'use strict';
    var logger,
        makeApplyDao;

    makeApplyDao = require('../dao/makeApplyDao');
    logger = require('../function/commonLogger');

    module.exports = {
        // 添付ファイルテーブル追加する
        prmsInsertAttachment: function (params) {
            return new Promise(function (resolve, reject) {
                makeApplyDao.prmsAddAttachment(params)
                    .then(function (res) {
                        resolve(res);
                    })
                    .catch(function (err) {
                        if (err !== undefined) {
                            logger.error(err);
                        }
                        reject();
                    });
            });
        }
    };
}());
