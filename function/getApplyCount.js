/*global Promise */
'use strict';
var applyDao = require('../dao/apply-dao');

/**
 * 自分が申請した申請書の件数を取得（代理申請含む）
 * @param  {Object} req.seesion.personcd パーソンコード
 * @resolve {Object}
 */
var getActiveApplyCount = function (req) {
	console.log('aaaaaaaaaaaaaa:'+req.session.personcd);
    return applyDao.prmsSelectActiveApplyCount({personCd: req.session.personcd});
};

/**
 * 承認一覧の件数を取得
 * @param  {Object} req.seesion.personcd パーソンコード
 * @resolve {Object}
 */
var getApproveCount = function (req) {
    return applyDao.prmsGetApproveCount({personCd: req.session.personcd});
};

/**
 * 閲覧一覧の件数を取得
 * @param  {Object} req.seesion.personcd パーソンコード
 * @resolve {Object}
 */
var getBrowseCount = function (req) {
    return applyDao.prmsGetBrowseCount({personCd: req.session.personcd});
};

exports.getActiveApplyCount = getActiveApplyCount;
exports.getApproveCount = getApproveCount;
exports.getBrowseCount = getBrowseCount;
