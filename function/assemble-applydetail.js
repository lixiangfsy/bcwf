/*global Promise */
'use strict';
var moment = require('moment');

var logger = require('../function/logger')();

var actionDao = require('../dao/action-dao');
var applyDao = require('../dao/apply-dao');
var attachmentDao = require('../dao/attachment-dao');
var commentDao = require('../dao/comment-dao');
var flowDao = require('../dao/flow-dao');
var taskDao = require('../dao/task-dao');

/**
 * 申請詳細画面に表示する情報を収集する
 * @param  {Object}     req
 * @param  {boolean}    req.createNewFlag 新規作成フラグ
 * @param  {string}     req.applyId 申請ID
 * @resolve {Object}
 */
module.exports = function (req) {
    return new Promise(function (resolve, reject) {
        var taskidRowNo,
            resolveRes;

        taskidRowNo = {};
        resolveRes = {
            applyInfo: {},
            attachmentInfoList: [],
            commentInfoList: [],
            processorsInfoList: [],
            referencesInfoList: [],
            actionHistoryInfoList: [],
            afterProcessList: [],
            makeApplyFlag: req.makeApplyFlag,
            applicantFlag: false,// ToDo 代理申請機能実装時に利用予定
        };
        applyDao.prmsGetApplyInfo({
            applyId: req.applyId,
        })
            .then(function (prmsGetApplyInfoRes) {
                if (prmsGetApplyInfoRes.rowCount) {
                    resolveRes.applyInfo = {
                        applyId: prmsGetApplyInfoRes.rows[0].applyid,
                        applyFormName: prmsGetApplyInfoRes.rows[0].applyformname,
                        applyDate: moment(prmsGetApplyInfoRes.rows[0].applydatetime).format('YYYY/MM/DD'),
                        startTaskId: prmsGetApplyInfoRes.rows[0].starttaskid,
                        personName: prmsGetApplyInfoRes.rows[0].personname,
                        applyTitle: prmsGetApplyInfoRes.rows[0].applytitle,
                        accountCd: prmsGetApplyInfoRes.rows[0].accountcd,
                        accountName: prmsGetApplyInfoRes.rows[0].accountname,
                        flowCd: prmsGetApplyInfoRes.rows[0].flowcd,
                    };
                }
                return attachmentDao.prmsGetAttachmentInfo({
                    applyId: req.applyId,
                });
            })
            .then(function (prmsGetAttachmentInfoRes) {
                var i;
                for (i = 0; i < prmsGetAttachmentInfoRes.rowCount; i += 1) {
                    resolveRes.attachmentInfoList.push({
                        fileId: prmsGetAttachmentInfoRes.rows[i].fileid,
                        attachDate: moment(prmsGetAttachmentInfoRes.rows[i].attachdate).format('YYYY/MM/DD HH:mm'),
                        personName: prmsGetAttachmentInfoRes.rows[i].personname,
                        filePath: prmsGetAttachmentInfoRes.rows[i].filepath,
                        fileName: prmsGetAttachmentInfoRes.rows[i].filename,
                    });
                }
                return commentDao.prmsGetCommentInfo({
                    applyId: req.applyId,
                });
            })
            .then(function (prmsGetCommentInfoRes) {
                var i;
                for (i = 0; i < prmsGetCommentInfoRes.rowCount; i += 1) {
                    resolveRes.commentInfoList.push({
                        commentDate: moment(prmsGetCommentInfoRes.rows[i].commentdate).format('YYYY/MM/DD HH:mm'),
                        personName: prmsGetCommentInfoRes.rows[i].personname,
                        comment: prmsGetCommentInfoRes.rows[i].comment,
                    });
                }
                return taskDao.prmsGetTaskInfo({
                    applyId: req.applyId,
                });
            })
            .then(function (prmsGetTaskInfoRes) {
                var i;
                for (i = 0; i < prmsGetTaskInfoRes.rowCount; i += 1) {
                    taskidRowNo[prmsGetTaskInfoRes.rows[i].taskid] = i;
                    resolveRes.processorsInfoList[i] = {
                        taskId: prmsGetTaskInfoRes.rows[i].taskid,
                        processCd: prmsGetTaskInfoRes.rows[i].processcd,
                        processName: prmsGetTaskInfoRes.rows[i].processname,
                        actionName: prmsGetTaskInfoRes.rows[i].actionname,
                        activeStatus: prmsGetTaskInfoRes.rows[i].activestatus,
                        processorPersonInfoList: [],
                        processorAccountInfoList: [],
                        processorTypeInfoList: [],
                        processorPositionInfoList: [],
                        processorRoleInfoList: [],
                    };
                    resolveRes.referencesInfoList[i] = {
                        taskId: prmsGetTaskInfoRes.rows[i].taskid,
                        processName: prmsGetTaskInfoRes.rows[i].processname,
                        referencePersonInfoList: [],
                        referenceAccountInfoList: [],
                        referenceTypeInfoList: [],
                        referencePositionInfoList: [],
                        referenceRoleInfoList: [],
                    };
                }
                return taskDao.prmsGetProcessorsInfo({
                    applyId: req.applyId,
                });
            })
            .then(function (prmsGetProcessorsInfoRes) {
                var i;
                for (i = 0; i < prmsGetProcessorsInfoRes.rowCount; i += 1) {
                    resolveRes.processorsInfoList[taskidRowNo[prmsGetProcessorsInfoRes.rows[i].taskid]].processorPersonInfoList.push({
                        personCd: prmsGetProcessorsInfoRes.rows[i].personcd,
                        personName: prmsGetProcessorsInfoRes.rows[i].personname,
                        accountCd: prmsGetProcessorsInfoRes.rows[i].accountcd,
                        accountName: prmsGetProcessorsInfoRes.rows[i].accountname,
                    });
                }
                return taskDao.prmsGetAccountProcessorInfo({
                    applyId: req.applyId,
                });
            })
            .then(function (prmsGetAccountProcessorInfoRes) {
                var i;
                for (i = 0; i < prmsGetAccountProcessorInfoRes.rowCount; i += 1) {
                    resolveRes.processorsInfoList[taskidRowNo[prmsGetAccountProcessorInfoRes.rows[i].taskid]].processorAccountInfoList.push({
                        accountCd: prmsGetAccountProcessorInfoRes.rows[i].accountcd,
                        accountName: prmsGetAccountProcessorInfoRes.rows[i].accountname,
                    });
                }
                return taskDao.prmsGetPersonTypeProcessorInfo({
                    applyId: req.applyId,
                });
            })
            .then(function (prmsGetPersonTypeProcessorInfoRes) {
                var i;
                for (i = 0; i < prmsGetPersonTypeProcessorInfoRes.rowCount; i += 1) {
                    resolveRes.processorsInfoList[taskidRowNo[prmsGetPersonTypeProcessorInfoRes.rows[i].taskid]].processorTypeInfoList.push({
                        personTypeCd: prmsGetPersonTypeProcessorInfoRes.rows[i].persontypecd,
                        personTypeName: prmsGetPersonTypeProcessorInfoRes.rows[i].persontypename,
                    });
                }
                return taskDao.prmsGetPositionProcessorInfo({
                    applyId: req.applyId,
                });
            })
            .then(function (prmsGetPositionProcessorInfoRes) {
                var i;
                for (i = 0; i < prmsGetPositionProcessorInfoRes.rowCount; i += 1) {
                    resolveRes.processorsInfoList[taskidRowNo[prmsGetPositionProcessorInfoRes.rows[i].taskid]].processorPositionInfoList.push({
                        positionCd: prmsGetPositionProcessorInfoRes.rows[i].positioncd,
                        positionName: prmsGetPositionProcessorInfoRes.rows[i].positionname,
                    });
                }
                return taskDao.prmsGetRoleProcessorInfo({
                    applyId: req.applyId,
                });
            })
            .then(function (prmsGetRoleProcessorInfoRes) {
                var i;
                for (i = 0; i < prmsGetRoleProcessorInfoRes.rowCount; i += 1) {
                    resolveRes.processorsInfoList[taskidRowNo[prmsGetRoleProcessorInfoRes.rows[i].taskid]].processorRoleInfoList.push({
                        roleCd: prmsGetRoleProcessorInfoRes.rows[i].rolecd,
                        roleName: prmsGetRoleProcessorInfoRes.rows[i].rolename,
                    });
                }
                return taskDao.prmsGetReferencesInfo({
                    applyId: req.applyId,
                });
            })
            .then(function (prmsGetReferencesInfoRes) {
                var i;
                for (i = 0; i < prmsGetReferencesInfoRes.rowCount; i += 1) {
                    resolveRes.referencesInfoList[taskidRowNo[prmsGetReferencesInfoRes.rows[i].taskid]].referencePersonInfoList.push({
                        personCd: prmsGetReferencesInfoRes.rows[i].personcd,
                        personName: prmsGetReferencesInfoRes.rows[i].personname,
                        accountCd: prmsGetReferencesInfoRes.rows[i].accountcd,
                        accountName: prmsGetReferencesInfoRes.rows[i].accountname,
                    });
                }
                return taskDao.prmsGetAccountReferenceInfo({
                    applyId: req.applyId,
                });
            })
            .then(function (prmsGetAccountReferenceInfoRes) {
                var i;
                for (i = 0; i < prmsGetAccountReferenceInfoRes.rowCount; i += 1) {
                    resolveRes.referencesInfoList[taskidRowNo[prmsGetAccountReferenceInfoRes.rows[i].taskid]].referenceAccountInfoList.push({
                        accountCd: prmsGetAccountReferenceInfoRes.rows[i].accountcd,
                        accountName: prmsGetAccountReferenceInfoRes.rows[i].accountname,
                    });
                }
                return taskDao.prmsGetPersonTypeReferenceInfo({
                    applyId: req.applyId,
                });
            })
            .then(function (prmsGetPersonTypeReferenceInfoRes) {
                var i;
                for (i = 0; i < prmsGetPersonTypeReferenceInfoRes.rowCount; i += 1) {
                    resolveRes.referencesInfoList[taskidRowNo[prmsGetPersonTypeReferenceInfoRes.rows[i].taskid]].referenceTypeInfoList.push({
                        personTypeCd: prmsGetPersonTypeReferenceInfoRes.rows[i].persontypecd,
                        personTypeName: prmsGetPersonTypeReferenceInfoRes.rows[i].persontypename,
                    });
                }
                return taskDao.prmsGetPositionReferenceInfo({
                    applyId: req.applyId,
                });
            })
            .then(function (prmsGetPositionReferenceInfoRes) {
                var i;
                for (i = 0; i < prmsGetPositionReferenceInfoRes.rowCount; i += 1) {
                    resolveRes.referencesInfoList[taskidRowNo[prmsGetPositionReferenceInfoRes.rows[i].taskid]].referencePositionInfoList.push({
                        positionCd: prmsGetPositionReferenceInfoRes.rows[i].positioncd,
                        positionName: prmsGetPositionReferenceInfoRes.rows[i].positionname,
                    });
                }
                return taskDao.prmsGetRoleReferenceInfo({
                    applyId: req.applyId,
                });
            })
            .then(function (prmsGetRoleReferenceInfoRes) {
                var i;
                for (i = 0; i < prmsGetRoleReferenceInfoRes.rowCount; i += 1) {
                    resolveRes.referencesInfoList[taskidRowNo[prmsGetRoleReferenceInfoRes.rows[i].taskid]].referenceRoleInfoList.push({
                        roleCd: prmsGetRoleReferenceInfoRes.rows[i].rolecd,
                        roleName: prmsGetRoleReferenceInfoRes.rows[i].rolename,
                    });
                }
                return actionDao.prmsGetActionHistoryInfo({
                    applyId: req.applyId,
                });
            })
            .then(function (prmsGetActionHistoryInfoRes) {
                var i;
                for (i = 0; i < prmsGetActionHistoryInfoRes.rowCount; i += 1) {
                    resolveRes.actionHistoryInfoList.push({
                        actionDate: moment(prmsGetActionHistoryInfoRes.rows[i].actiondate).format('YYYY/MM/DD HH:mm'),
                        action: prmsGetActionHistoryInfoRes.rows[i].action,
                        taskId: prmsGetActionHistoryInfoRes.rows[i].taskid,
                        actionPersonInfo: {
                            personCd: prmsGetActionHistoryInfoRes.rows[i].personcd,
                            personName: prmsGetActionHistoryInfoRes.rows[i].personname,
                        },
                    });
                }
                return flowDao.prmsGetOFlowRProcessAfter({
                    applyId: req.applyId,
                });
            })
            .then(function (prmsGetOFlowRProcessAfterRes) {
                var i;
                for (i = 0; i < prmsGetOFlowRProcessAfterRes.rowCount; i += 1) {
                    resolveRes.afterProcessList.push({
                        afterProcessCd: prmsGetOFlowRProcessAfterRes.rows[i].afterprocesscd,
                    });
                }
                resolve(resolveRes);
            })
            .catch(function (error) {
                if (error !== undefined) {
                    logger.error(error);
                }
                reject(error);
            });
    });
};
