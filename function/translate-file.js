/*global Promise */
'use strict';
var fs = require('fs');
var path = require('path');

var logger = require('../function/logger')();

var attachmentDao = require('../dao/attachment-dao');

/**
 * ファイルコピー（元ファイルは削除しない）
 * @param  {integer} applyId 申請ID
 * @param  {string} personCd パーソンCD
 * @param  {Object} originFileInfo 元ファイル
 * @resolve {Object}
 */
module.exports = function (req, client) {
    return new Promise(function (resolve, reject) {
        // フォルダが存在しない場合、申請IDでフォルダを作成する。
        var desPath;

        desPath = path.join(__dirname, '../public/uploads', req.applyId);
        fs.mkdir(desPath, function (err) {
            if (err) {
                if (err.code === 'EEXIST') {
                    resolve(); // ignore the error if the folder already exists
                } else {
                    logger.error(err);
                    reject();
                }
            } else {
                resolve(); // successfully created folder
            }
        });
    })
        .then(function () {
            var i,
                promises;
            promises = [];
            for (i = 0; i < req.originFileInfo.length; i += 1) {
                promises.push(attachmentDao.prmsSelectFileIdSeq());
            }
            return Promise.all(promises);
        })
        .then(function (res) {
            var i,
                prmsRenameFiles,
                promises;
            prmsRenameFiles = function (oldPath, fileid) {
                return new Promise(function (resolve, reject) {
                    var newPath;
                    logger.info(req.applyId);
                    logger.info(fileid);
                    logger.info(oldPath);

                    newPath = path.join(__dirname, '../public/uploads', req.applyId, fileid + path.extname(oldPath));
                    //logger.info('applyId:' + req.body.applyId);
                    //logger.info('fileid:' + fileid);
                    fs.copyFile(
                        oldPath,
                        newPath,
                        function (err) {
                            if (err) {
                                logger.error(err);
                                reject();
                            } else {
                                fs.unlink(oldPath, function (err) {
                                    if (err) {
                                        logger.error(err);
                                        reject();
                                    } else {
                                        resolve({
                                            fileId: fileid,
                                            filePath: newPath,
                                        });
                                    }
                                });
                            }
                        }
                    );
                });
            };
            promises = [];
            logger.info(req);
            for (i = 0; i < req.originFileInfo.length; i += 1) {
                logger.info(req.originFileInfo[i]);
                promises.push(prmsRenameFiles(
                    req.originFileInfo[i].path ? req.originFileInfo[i].path : req.originFileInfo[i].filepath,
                    res[i].rows[0].fileid
                ));
            }
            return Promise.all(promises);
        })
        .then(function (res) {
            var i,
                promises;

            promises = [];
            for (i = 0; i < res.length; i += 1) {
                promises.push(attachmentDao.prmsInsertTAttachment({
                    fileId: res[i].fileId,
                    applyId: req.applyId,
                    personCd: req.personCd,
                    attachDate: req.saveDate,
                    filePath: res[i].filePath,
                    fileName: req.originFileInfo[i].name ? req.originFileInfo[i].name : req.originFileInfo[i].filename,
                    fileSize: req.originFileInfo[i].size ? req.originFileInfo[i].size : 0,
                }, client));
            }
            return Promise.all(promises);
        });
};
