/*global Promise */
'use strict';
var connectMultiparty = require('connect-multiparty')();
var fs = require('fs');
var path = require('path');
var util = require('util');

var logger = require('../function/logger')();
var translateFile = require('../function/translate-file');

var actionDao = require('../dao/action-dao');
var applyDao = require('../dao/apply-dao');
var attachmentDao = require('../dao/attachment-dao');
var commentDao = require('../dao/comment-dao');
var dao = require('../dao/dao');

/**
 * 申請書を操作(保存、申請、承認など)
 * @param  {Object} req multipart/form-dataリクエスト
 * @return {Object} Promise
 */
module.exports = function (req) {
    return new Promise(function (resolve, reject) {
        var client,
            deleteFilePath,
            listDeleteFileIds;

        deleteFilePath = [];
        listDeleteFileIds = [];

        new Promise(function (resolve, reject) {
            // マルチパートデータをパースする
            var resDummy;
            resDummy = {};
            connectMultiparty(req, resDummy, function (err) {
                if (err) {
                    logger.error(err);
                    reject();
                } else {
                    logger.info('connect-multiparty success');
                    resolve();
                }
            });
        })
            .then(function () {
                return dao.pool().connect();
            })
            .then(function (res) {
                client = res;
                return dao.prmsBegin(client);
            })
            .then(function () {
                var i,
                    promises,
                    prmsSelectFilePathByFileId;

                prmsSelectFilePathByFileId = function (fileId) {
                    return new Promise(function (resolve, reject) {
                        attachmentDao.prmsSelectFilePathByFileId(fileId)
                            .then(function (res) {
                                deleteFilePath.push(res.rows[0].filepath);
                                resolve(res);
                            })
                            .catch(function (err) {
                                logger.error(err);
                                reject();
                            });
                    });
                };

                if (req.body.deleteFileIds !== '' && req.body.isChangedAttach === 'true') {
                    listDeleteFileIds = req.body.deleteFileIds.split(',');
                }

                promises = [];
                for (i = 0; i < listDeleteFileIds.length; i += 1) {
                    logger.info(listDeleteFileIds);
                    promises.push(prmsSelectFilePathByFileId({
                        fileId: listDeleteFileIds[i],
                    }));
                }
                return Promise.all(promises);
            })
            .then(function () {
                var i,
                    promises,
                    prmsDeleteFiles;

                prmsDeleteFiles = function (filepath) {
                    return new Promise(function (resolve, reject) {
                        var promiseUnlink;
                        promiseUnlink = util.promisify(fs.unlink);

                        promiseUnlink(path.join(filepath))
                            .then(function (res) {
                                logger.info('promiseUnlink成功！');
                                resolve(res);
                            })
                            .catch(function (err) {
                                if (err.code === 'ENOENT') {
                                    logger.info('ENOENT');
                                    resolve();
                                } else {
                                    logger.error(err);
                                    reject();
                                }
                            });
                    });
                };
                promises = [];
                for (i = 0; i < listDeleteFileIds.length; i += 1) {
                    promises.push(prmsDeleteFiles(deleteFilePath[i]));
                }
                return Promise.all(promises);
            })
            .then(function () {
                var i,
                    promises,
                    prmsDeleteTAttachment;
                prmsDeleteTAttachment = attachmentDao.prmsDeleteTAttachment;
                promises = [];
                for (i = 0; i < listDeleteFileIds.length; i += 1) {
                    promises.push(prmsDeleteTAttachment({
                        fileId: listDeleteFileIds[i],
                    }, client));
                }
                return Promise.all(promises);
            })
            .then(function () {
                if (req.files.fileUpload && req.files.fileUpload.length > 0 && req.body.isChangedAttach) {
                    return translateFile({
                        applyId: req.body.applyId,
                        personCd: req.body.personCd,
                        saveDate: req.body.saveDate,
                        originFileInfo: req.files.fileUpload,
                        client: client,
                    });
                }
                return Promise.resolve();
            })
            .then(function () {
                if (req.body.comment !== '') {
                    return commentDao.prmsInsertTComment({
                        applyId: req.body.applyId,
                        personCd: req.body.personCd,
                        saveDate: req.body.saveDate,
                        comment: req.body.comment,
                    }, client);
                }
                return Promise.resolve();
            })
            .then(function () {
                return actionDao.prmsInsertTAction({
                    taskId: req.body.taskId,
                    personCd: req.body.personCd,
                    action: req.body.action,
                    saveDate: req.body.saveDate,
                }, client);
            })
            .then(function () {
                logger.info('beginsave');
                return applyDao.prmsUpdateTApply({
                    applyTitle: req.body.applyTitle,
                    saveDate: req.body.saveDate,
                    accountCd: req.body.accountCd,
                    applyId: req.body.applyId,
                }, client);
            })
            .then(function () {
                return dao.prmsCommit(client);
            })
            .then(function () {
                client.release();
                resolve();
            })
            .catch(function (err) {
                logger.info('reject1:');
                if (err !== undefined) {
                    logger.error(err);
                }
                if (client) {
                    dao.prmsRollback(client)
                        .then(function () {
                            logger.info('ロールバック成功');
                            client.release();
                        });
                }
                reject();
            });
    });
};
