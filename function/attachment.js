/*global Promise */
(function () {
    'use strict';
    var logger,
        moment;
    logger = require('../function/logger')();
    moment = require('moment');

    module.exports = {
        prmsUploadAttachment: function (req) {
            var express,
                fs,
                logger,
                path,
                router;
            logger = require('../function/logger')();
            express = require('express');
            router = express.Router();
            path = require('path');
            fs = require('fs');

            return new Promise(function (resolve, rejecet) {
                var prmsDoAttach;
                prmsDoAttach = function () {
                    return new Promise(function (resolve, rejecet) {
                        console.log(req.files.formData);  // Todo delete
                        console.log(req.body.applyId);
                        var desFile,
                            desPath,
                            fileList,
                            fileSize,
                            jsonarray,
                            response;

                        desPath = path.join(__dirname, './../public/uploads', req.body.applyId);
                        console.log(desPath);
                        // フォルダが存在しない場合、申請IDでフォルダを作成する
                        fs.exists(desPath, function (exists) {
                            if (!exists) {
                                fs.mkdir(desPath, function (err) {
                                    if (err) {
                                        console.error(err);
                                    }
                                });
                            }
                        });
                        //

                        desFile = path.join(__dirname, './../public/uploads', req.body.applyId, req.files.formData.originalFilename);
                        fs.readFile(req.files.formData.path, function (err, data) {
                            fs.writeFile(desFile, data, function (err) {
                                if (err) {
                                    console.log(err);
                                    rejecet(err);
                                } else {
                                    // Todo delete start ダミーデータ
                                    fileList = "[{'attachdate':'2018/11/01', 'personname':'山本 太郎', filepath: 'AAA232200222/DDL.sql', filename: 'DDL.sql'}," +
                                               " {'attachdate':'2018/11/01', 'personname':'山本 太郎', filepath: 'AAA232200222/DDL.sql', filename: 'DDL.sql'}," +
                                               " {'attachdate':'2018/11/01', 'personname':'山本 太郎', filepath: 'AAA232200222/DDL.sql', filename: 'DDL.sql'}," +
                                               " {'attachdate':'2018/11/01', 'personname':'山本 太郎', filepath: 'AAA232200222/DDL.sql', filename: 'DDL.sql'}]";
                                    jsonarray = eval('('+fileList+')');
                                    // Todo delete end
                                    // DBに保存する　APIを呼び出す
                                    console.log(req.files.formData);
                                    fileSize = req.files.formData.size;
                                    // DBから添付レコードを再取得する
                                    response = {
                                        message: 'アップロード成功',
                                        fileList: jsonarray
                                    };
                                    resolve(response);
                                }
                                //res.end(JSON.stringify(response));
                            });
                        });

                    });
                };
                prmsDoAttach()
                    .then(function(response){
                        resolve(response);
                    })
                    .catch(function(){
                        rejecet();
                    });


            });

        }
    };
}());
