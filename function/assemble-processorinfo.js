/*global Promise */
'use strict';
var logger = require('../function/logger')();

var applyDao = require('../dao/apply-dao');
var flowDao = require('../dao/flow-dao');
var personinfoDao = require('../dao/personinfo-dao');
var taskDao = require('../dao/task-dao');

module.exports = {
    /**
     * タスクIDから処理者情報（パーソン、所属、役職、役割、パーソンタイプそれぞれ全て）を取得
     * @param  {Object} req
     * @param  {Integer} req.taskId タスクID
     * @param  {Object} [client] プール又はクライアントインスタンス
     * @return {Object} Promise
     */
    prmsGetAllProcessorInfoByTaskId: function (req, client) {
        return new Promise(function (resolve, reject) {
            var allProcessorInfo;
            allProcessorInfo = {
                account: [],
                person : [],
                personType : [],
                position : [],
                role : [],
            };
            taskDao.prmsGetProcessorsInfoByTaskId(req, client)
                .then(function (prmsGetProcessorsInfoByTaskIdRes) {
                    var i;
                    for (i = 0; i < prmsGetProcessorsInfoByTaskIdRes.rowCount; i += 1) {
                        allProcessorInfo.person.push(prmsGetProcessorsInfoByTaskIdRes.rows[i].personcd);
                    }
                    return taskDao.prmsGetAccountProcessorInfoByTaskId(req, client);
                })
                .then(function (prmsGetAccountProcessorInfoByTaskIdRes) {
                    var i;
                    for (i = 0; i < prmsGetAccountProcessorInfoByTaskIdRes.rowCount; i += 1) {
                        allProcessorInfo.account.push(prmsGetAccountProcessorInfoByTaskIdRes.rows[i].accountcd);
                    }
                    return taskDao.prmsGetPersonTypeProcessorInfoByTaskId(req, client);
                })
                .then(function (prmsGetPersonTypeProcessorInfoByTaskIdRes) {
                    var i;
                    for (i = 0; i < prmsGetPersonTypeProcessorInfoByTaskIdRes.rowCount; i += 1) {
                        allProcessorInfo.personType.push(prmsGetPersonTypeProcessorInfoByTaskIdRes.rows[i].persontypecd);
                    }
                    return taskDao.prmsGetPositionProcessorInfoByTaskId(req, client);
                })
                .then(function (prmsGetPositionProcessorInfoByTaskIdRes) {
                    var i;
                    for (i = 0; i < prmsGetPositionProcessorInfoByTaskIdRes.rowCount; i += 1) {
                        allProcessorInfo.position.push(prmsGetPositionProcessorInfoByTaskIdRes.rows[i].positioncd);
                    }
                    return taskDao.prmsGetRoleProcessorInfoByTaskId(req, client);
                })
                .then(function (prmsGetRoleProcessorInfoByTaskIdRes) {
                    var i;
                    for (i = 0; i < prmsGetRoleProcessorInfoByTaskIdRes.rowCount; i += 1) {
                        allProcessorInfo.role.push(prmsGetRoleProcessorInfoByTaskIdRes.rows[i].rolecd);
                    }
                    resolve(allProcessorInfo);
                })
                .catch(function (error) {
                    reject(error);
                });
        });
    },

    /**
     * タスクの処理者（パーソン、所属、役職、役割、パーソンタイプそれぞれ全て）をパーソン単位に集約する
     * @param  {Object} prmsGetIntensivePersonProcessorByTaskIdReq
     * @param  {Integer} prmsGetIntensivePersonProcessorByTaskIdReq.taskId タスクID
     * @param  {Object} [client] プール又はクライアントインスタンス
     * @return {Object} Promise
     */
    prmsGetIntensivePersonProcessorByTaskId: function (prmsGetIntensivePersonProcessorByTaskIdReq, client) {
        var prmsGetIntensivePersonProcessorByTaskIdRes;
        prmsGetIntensivePersonProcessorByTaskIdRes = [];
        return new Promise(function (resolve, reject) {
            taskDao.prmsGetProcessorsInfoByTaskId(prmsGetIntensivePersonProcessorByTaskIdReq, client)
                .then(function (prmsGetProcessorsInfoByTaskIdRes) {
                    var i;
                    for (i = 0; i < prmsGetProcessorsInfoByTaskIdRes.rowCount; i += 1) {
                        prmsGetIntensivePersonProcessorByTaskIdRes.push(prmsGetProcessorsInfoByTaskIdRes.rows[i].personcd);
                    }
                    return taskDao.prmsGetAccountProcessorInfoByTaskId(prmsGetIntensivePersonProcessorByTaskIdReq, client);
                })
                .then(function (prmsGetAccountProcessorInfoByTaskIdRes) {
                    var i,
                        accountProcessor;

                    accountProcessor = [];
                    for (i = 0; i < prmsGetAccountProcessorInfoByTaskIdRes.rowCount; i += 1) {
                        accountProcessor.push(prmsGetAccountProcessorInfoByTaskIdRes.rows[i].accountcd);
                    }
                    return new Promise(function (resolve, reject) {
                        personinfoDao.prmsGetPersonByAccount(accountProcessor, client)
                            .then(function (prmsGetPersonByAccountRes) {
                                for (i = 0; i < prmsGetPersonByAccountRes.rowCount; i += 1) {
                                    prmsGetIntensivePersonProcessorByTaskIdRes.push(prmsGetPersonByAccountRes.rows[i].personcd);
                                }
                                return taskDao.prmsGetPersonTypeProcessorInfoByTaskId(prmsGetIntensivePersonProcessorByTaskIdReq, client);
                            })
                            .then(function (prmsGetPersonTypeProcessorInfoByTaskIdRes) {
                                resolve(prmsGetPersonTypeProcessorInfoByTaskIdRes);
                            })
                            .catch(function (error) {
                                if (error !== undefined) {
                                    logger.error(error);
                                }
                                reject();
                            });
                    });
                })
                .then(function (prmsGetPersonTypeProcessorInfoByTaskIdRes) {
                    var i,
                        personTypeProcessor;

                    personTypeProcessor = [];
                    for (i = 0; i < prmsGetPersonTypeProcessorInfoByTaskIdRes.rowCount; i += 1) {
                        personTypeProcessor.push(prmsGetPersonTypeProcessorInfoByTaskIdRes.rows[i].persontypecd);
                    }
                    return new Promise(function (resolve, reject) {
                        personinfoDao.prmsGetPersonByPersonType(personTypeProcessor, client)
                            .then(function (prmsGetPersonByPersonTypeRes) {
                                for (i = 0; i < prmsGetPersonByPersonTypeRes.rowCount; i += 1) {
                                    prmsGetIntensivePersonProcessorByTaskIdRes.push(prmsGetPersonByPersonTypeRes.rows[i].personcd);
                                }
                                return taskDao.prmsGetPositionProcessorInfoByTaskId(prmsGetIntensivePersonProcessorByTaskIdReq, client);
                            })
                            .then(function (prmsGetPositionProcessorInfoByTaskIdRes) {
                                resolve(prmsGetPositionProcessorInfoByTaskIdRes);
                            })
                            .catch(function (error) {
                                if (error !== undefined) {
                                    logger.error(error);
                                }
                                reject();
                            });
                    });
                })
                .then(function (prmsGetPositionProcessorInfoByTaskIdRes) {
                    var i,
                        positionProcessor;

                    positionProcessor = [];
                    for (i = 0; i < prmsGetPositionProcessorInfoByTaskIdRes.rowCount; i += 1) {
                        positionProcessor.push(prmsGetPositionProcessorInfoByTaskIdRes.rows[i].positioncd);
                    }
                    return new Promise(function (resolve, reject) {
                        personinfoDao.prmsGetPersonByPosition(positionProcessor, client)
                            .then(function (prmsGetPersonByPositionRes) {
                                for (i = 0; i < prmsGetPersonByPositionRes.rowCount; i += 1) {
                                    prmsGetIntensivePersonProcessorByTaskIdRes.push(prmsGetPersonByPositionRes.rows[i].personcd);
                                }
                                return taskDao.prmsGetRoleProcessorInfoByTaskId(prmsGetIntensivePersonProcessorByTaskIdReq, client);
                            })
                            .then(function (prmsGetRoleProcessorInfoByTaskIdRes) {
                                resolve(prmsGetRoleProcessorInfoByTaskIdRes);
                            })
                            .catch(function (error) {
                                if (error !== undefined) {
                                    logger.error(error);
                                }
                                reject();
                            });
                    });
                })
                .then(function (prmsGetRoleProcessorInfoByTaskIdRes) {
                    var i,
                        roleProcessor;

                    roleProcessor = [];
                    for (i = 0; i < prmsGetRoleProcessorInfoByTaskIdRes.rowCount; i += 1) {
                        roleProcessor.push(prmsGetRoleProcessorInfoByTaskIdRes.rows[i].rolecd);
                    }
                    return new Promise(function (resolve, reject) {
                        personinfoDao.prmsGetPersonByRole(roleProcessor, client)
                            .then(function (prmsGetPersonByRoleRes) {
                                for (i = 0; i < prmsGetPersonByRoleRes.rowCount; i += 1) {
                                    prmsGetIntensivePersonProcessorByTaskIdRes.push(prmsGetPersonByRoleRes.rows[i].personcd);
                                }
                                resolve();
                            })
                            .catch(function (error) {
                                if (error !== undefined) {
                                    logger.error(error);
                                }
                                reject();
                            });
                    });
                })
                .then(function () {
                    resolve(prmsGetIntensivePersonProcessorByTaskIdRes);
                })
                .catch(function (error) {
                    if (error !== undefined) {
                        logger.error(error);
                    }
                    reject();
                });
        });
    },

    /**
     * タスクの参照者（パーソン、所属、役職、役割、パーソンタイプそれぞれ全て）をパーソン単位に集約する
     * @param  {Object} prmsGetIntensivePersonReferenceByTaskIdReq
     * @param  {Integer} prmsGetIntensivePersonReferenceByTaskIdReq.taskId タスクID
     * @param  {Object} [client] プール又はクライアントインスタンス
     * @return {Object} Promise
     */
    prmsGetIntensivePersonReferenceByTaskId: function (prmsGetIntensivePersonReferenceByTaskIdReq, client) {
        var prmsGetIntensivePersonReferenceByTaskIdRes;
        prmsGetIntensivePersonReferenceByTaskIdRes = [];
        return new Promise(function (resolve, reject) {
            taskDao.prmsGetReferenceInfoByTaskId(prmsGetIntensivePersonReferenceByTaskIdReq, client)
                .then(function (prmsGetReferenceInfoByTaskIdRes) {
                    var i;
                    for (i = 0; i < prmsGetReferenceInfoByTaskIdRes.rowCount; i += 1) {
                        prmsGetIntensivePersonReferenceByTaskIdRes.push(prmsGetReferenceInfoByTaskIdRes.rows[i].personcd);
                    }
                    return taskDao.prmsGetAccountReferenceInfoByTaskId(prmsGetIntensivePersonReferenceByTaskIdReq, client);
                })
                .then(function (prmsGetAccountReferenceInfoByTaskIdRes) {
                    var i,
                        accountReference;

                    accountReference = [];
                    for (i = 0; i < prmsGetAccountReferenceInfoByTaskIdRes.rowCount; i += 1) {
                        accountReference.push(prmsGetAccountReferenceInfoByTaskIdRes.rows[i].accountcd);
                    }
                    return new Promise(function (resolve, reject) {
                        personinfoDao.prmsGetPersonByAccount(accountReference, client)
                            .then(function (prmsGetPersonByAccountRes) {
                                for (i = 0; i < prmsGetPersonByAccountRes.rowCount; i += 1) {
                                    prmsGetIntensivePersonReferenceByTaskIdRes.push(prmsGetPersonByAccountRes.rows[i].personcd);
                                }
                                return taskDao.prmsGetPersonTypeReferenceInfoByTaskId(prmsGetIntensivePersonReferenceByTaskIdReq, client);
                            })
                            .then(function (prmsGetPersonTypeReferenceInfoByTaskIdRes) {
                                resolve(prmsGetPersonTypeReferenceInfoByTaskIdRes);
                            })
                            .catch(function (error) {
                                if (error !== undefined) {
                                    logger.error(error);
                                }
                                reject();
                            });
                    });
                })
                .then(function (prmsGetPersonTypeReferenceInfoByTaskIdRes) {
                    var i,
                        personTypeReference;

                    personTypeReference = [];
                    for (i = 0; i < prmsGetPersonTypeReferenceInfoByTaskIdRes.rowCount; i += 1) {
                        personTypeReference.push(prmsGetPersonTypeReferenceInfoByTaskIdRes.rows[i].persontypecd);
                    }
                    return new Promise(function (resolve, reject) {
                        personinfoDao.prmsGetPersonByPersonType(personTypeReference, client)
                            .then(function (prmsGetPersonByPersonTypeRes) {
                                for (i = 0; i < prmsGetPersonByPersonTypeRes.rowCount; i += 1) {
                                    prmsGetIntensivePersonReferenceByTaskIdRes.push(prmsGetPersonByPersonTypeRes.rows[i].personcd);
                                }
                                return taskDao.prmsGetPositionReferenceInfoByTaskId(prmsGetIntensivePersonReferenceByTaskIdReq, client);
                            })
                            .then(function (prmsGetPositionReferenceInfoByTaskIdRes) {
                                resolve(prmsGetPositionReferenceInfoByTaskIdRes);
                            })
                            .catch(function (error) {
                                if (error !== undefined) {
                                    logger.error(error);
                                }
                                reject();
                            });
                    });
                })
                .then(function (prmsGetPositionReferenceInfoByTaskIdRes) {
                    var i,
                        positionReference;

                    positionReference = [];
                    for (i = 0; i < prmsGetPositionReferenceInfoByTaskIdRes.rowCount; i += 1) {
                        positionReference.push(prmsGetPositionReferenceInfoByTaskIdRes.rows[i].positioncd);
                    }
                    return new Promise(function (resolve, reject) {
                        personinfoDao.prmsGetPersonByPosition(positionReference, client)
                            .then(function (prmsGetPersonByPositionRes) {
                                for (i = 0; i < prmsGetPersonByPositionRes.rowCount; i += 1) {
                                    prmsGetIntensivePersonReferenceByTaskIdRes.push(prmsGetPersonByPositionRes.rows[i].personcd);
                                }
                                return taskDao.prmsGetRoleReferenceInfoByTaskId(prmsGetIntensivePersonReferenceByTaskIdReq, client);
                            })
                            .then(function (prmsGetRoleReferenceInfoByTaskIdRes) {
                                resolve(prmsGetRoleReferenceInfoByTaskIdRes);
                            })
                            .catch(function (error) {
                                if (error !== undefined) {
                                    logger.error(error);
                                }
                                reject();
                            });
                    });
                })
                .then(function (prmsGetRoleReferenceInfoByTaskIdRes) {
                    var i,
                        roleReference;

                    roleReference = [];
                    for (i = 0; i < prmsGetRoleReferenceInfoByTaskIdRes.rowCount; i += 1) {
                        roleReference.push(prmsGetRoleReferenceInfoByTaskIdRes.rows[i].rolecd);
                    }
                    return new Promise(function (resolve, reject) {
                        personinfoDao.prmsGetPersonByRole(roleReference, client)
                            .then(function (prmsGetPersonByRoleRes) {
                                for (i = 0; i < prmsGetPersonByRoleRes.rowCount; i += 1) {
                                    prmsGetIntensivePersonReferenceByTaskIdRes.push(prmsGetPersonByRoleRes.rows[i].personcd);
                                }
                                resolve();
                            })
                            .catch(function (error) {
                                if (error !== undefined) {
                                    logger.error(error);
                                }
                                reject();
                            });
                    });
                })
                .then(function () {
                    resolve(prmsGetIntensivePersonReferenceByTaskIdRes);
                })
                .catch(function (error) {
                    if (error !== undefined) {
                        logger.error(error);
                    }
                    reject();
                });
        });
    },
};
