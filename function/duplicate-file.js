/*global Promise */
'use strict';
var fs = require('fs');
var path = require('path');

var logger = require('../function/logger')();

var attachmentDao = require('../dao/attachment-dao');

/**
 * ファイルコピー、保存（元ファイルは削除しない）
 * @param  {Object} req
 * @param  {Integer} req.applyId 申請ID
 * @param  {String} req.personCd パーソンCD
 * @param  {Object} req.originFileInfo 元ファイル
 * @param  {Object} [client] プール又はクライアントインスタンス
 * @resolve {Object}
 */
module.exports = function (req, client) {
    return new Promise(function (resolve, reject) {
        // フォルダが存在しない場合、申請IDでフォルダを作成する。
        var desPath;

        desPath = path.join(__dirname, '../public/uploads', req.applyId);
        fs.mkdir(desPath, function (err) {
            logger.info('duplicate-1');
            if (err) {
                if (err.code === 'EEXIST') {
                    resolve(); // ignore the error if the folder already exists
                } else {
                    logger.error(err);
                    reject();
                }
            } else {
                resolve(); // successfully created folder
            }
        });
    })
        .then(function () {
            var i,
                promises;
            promises = [];
            logger.info('duplicate-2');
            for (i = 0; i < req.originFileInfo.length; i += 1) {
                promises.push(attachmentDao.prmsSelectFileIdSeq());
            }
            return Promise.all(promises);
        })
        .then(function (res) {
            var i,
                prmsCopyFile,
                promises;
            prmsCopyFile = function (oldPath, fileId) {
                return new Promise(function (resolve, reject) {
                    var newPath;

                    newPath = path.join(__dirname, '../public/uploads', req.applyId, fileId + path.extname(oldPath));
                    logger.info('duplicate-3');
                    logger.info(oldPath + ',' + fileId);
                    fs.copyFile(
                        oldPath,
                        newPath,
                        function (err) {
                            if (err) {
                                logger.error(err);
                                reject();
                            } else {
                                logger.info('duplicate-4');
                                fs.stat(
                                    newPath,
                                    function (err, stats) {
                                        if (err) {
                                            logger.error(err);
                                            reject();
                                        }
                                        resolve({
                                            fileId: fileId,
                                            filePath: newPath,
                                            fileSize: stats.size
                                        });
                                    }
                                );
                            }
                        }
                    );
                });
            };

            promises = [];
            for (i = 0; i < req.originFileInfo.length; i += 1) {
                promises.push(prmsCopyFile(
                    req.originFileInfo[i].path || req.originFileInfo[i].filePath,
                    res[i].rows[0].fileid
                ));
            }
            return Promise.all(promises);
        })
        .then(function (res) {
            var i,
                promises;

            promises = [];
            logger.info('duplicate-5');
            for (i = 0; i < res.length; i += 1) {
                promises.push(attachmentDao.prmsInsertTAttachment({
                    fileId: res[i].fileId,
                    applyId: req.applyId,
                    personCd: req.personCd,
                    attachDate: req.saveDate,
                    filePath: res[i].filePath,
                    fileName: req.originFileInfo[i].name || req.originFileInfo[i].fileName,
                    fileSize: res[i].fileSize,
                }, client));
            }
            logger.info('duplicate-6');
            return Promise.all(promises);
        });
};
