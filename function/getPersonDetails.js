/*global Promise */
(function () {
    'use strict';
    var getPersonDetailsDao,
        logger;

    getPersonDetailsDao = require('../dao/getPersonDetailsDao');
    logger = require('../function/logger')();

    module.exports = {
        // 所属と役職を取得する
        prmsGetAccount: function (params) {
            return new Promise(function (resolve, reject) {
                getPersonDetailsDao.prmsGetAccount(params)
                    .then(function (res) {
                        if (res.rowCount === 0) {
                            resolve();
                        } else {
                            resolve(res);
                        }
                    })
                    .catch(function (err) {
                        if (err !== undefined) {
                            logger.error(err);
                        }
                        reject();
                    });
            });
        },

        // 役割を取得する
        prmsGetRole: function (params) {
            return new Promise(function (resolve, reject) {
                getPersonDetailsDao.prmsGetRole(params)
                    .then(function (res) {
                        if (res.rowCount === 0) {
                            resolve();
                        } else {
                            resolve(res);
                        }
                    })
                    .catch(function (err) {
                        if (err !== undefined) {
                            logger.error(err);
                        }
                        reject();
                    });
            });
        },

        // パーソン区分を取得する
        prmsGetPersonType: function (params) {
            return new Promise(function (resolve, reject) {
                getPersonDetailsDao.prmsGetPersonType(params)
                    .then(function (res) {
                        if (res.rowCount === 0) {
                            resolve();
                        } else {
                            resolve(res);
                        }
                    })
                    .catch(function (err) {
                        if (err !== undefined) {
                            logger.error(err);
                        }
                        reject();
                    });
            });
        }
    };
}());
