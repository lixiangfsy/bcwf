/*global Promise */
'use strict';
var logger = require('../function/logger')();
var express = require('express');
var router = express.Router();
var moment = require('moment');

var main = function (req, res) {
    var baseDate,
        makeApply,
        mapPersonCd,
        myInfo,
        personinfoDao;
    baseDate = moment().format('YYYY/MM/DD');
    myInfo = {};
    mapPersonCd = require('../function/map-personcd');
    personinfoDao = require('../dao/personinfo-dao');
    makeApply = function () {
        return new Promise(function (resolve, reject) {
            if (req && req.body && req.body.applyId) {
                resolve({
                    applyId: req.body.applyId,
                    makeApplyFlag: false,
                });
            } else {
                require('../function/make-apply')(req.body)
                    .then(function (result) {
                        resolve({
                            applyId: result.applyId,
                            makeApplyFlag: true,
                        });
                    })
                    .catch(function (error) {
                        if (error !== undefined) {
                            logger.error(error);
                        }
                        reject();
                    });
            }
        });
    };
    mapPersonCd(req && req.body)
        .then(function (result) {
            myInfo.personCd = result.personCd;
            return personinfoDao.prmsGetAccount({
                personCd: result.personCd,
                baseDate: baseDate,
            });
        })
        .then(function (result) {
            myInfo.accountInfoList = result.rows;
            return personinfoDao.prmsGetPersonType({
                personCd: result.personCd,
                baseDate: baseDate,
            });
        })
        .then(function (result) {
            myInfo.personTypeInfoList = result.rows;
            return personinfoDao.prmsGetRole({
                personCd: result.personCd,
                baseDate: baseDate,
            });
        })
        .then(function (result) {
            myInfo.roleInfoList = result.rows;
            return makeApply();
        })
        .then(function (result) {
            return require('../function/assemble-applydetail')(result);
        })
        .then(function (result) {
            if (result.applyInfo.applyId !== undefined) {
                res.render('applydetail', {json: result, myInfo: myInfo});
            } else {
                res.status(200).send('該当する申請は存在しません。');
            }
        })
        .catch(function (error) {
            if (error !== undefined) {
                logger.error(error);
            }
            res.status(500).send('Sorry we have something error!');
        });
};

// 申請詳細画面へ遷移
router.post('/', function (req, res) {
    main(req, res);
});

router.get('/', function (req, res) {
    logger.info(req.query);
    if (!req.query.acc || !req.query.app || !(req.query.uid || req.query.pcd)) {
        res.status(400).send('Bad Request');
        return;
    }
    req.body = req.body || {};
    req.body.accessToken = req.query.acc;
    req.body.applyId = req.query.app;
    req.body.userId = req.query.uid;
    req.body.personCd = req.query.pcd;
    req.body.baseDate = moment().format('YYYY/MM/DD');
    main(req, res);
});

module.exports = router;
