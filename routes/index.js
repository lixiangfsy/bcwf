'use strict';
var logger = require('../function/logger')();
var express = require('express');
var router = express.Router();

// ToDo とりあえずデバッグ用と思われるので今は残すが、後で消す
router.get('/:applyId', function (req, res) {
    res.status(500).send('Sorry we have nothing!');
});
module.exports = router;
