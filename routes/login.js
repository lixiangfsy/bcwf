'use strict';
var logger = require('../function/logger')();
var express = require('express');
var router = express.Router();
var ejs = require('ejs');

router.get('/', function(req, res) {
    res.render('login');
});

router.post('/', function(req, res) {
    var apiLogin,
        htmlArray,
        i;
    apiLogin = require('../api/login');
    if (req.body.userId !== undefined && req.body.userId !== '' && req.body.password !== undefined && req.body.password !== '') {
        apiLogin(req)
            .then(function(result) {
                if (result.resultCode === 0) {
                    htmlArray = [];
                    for (i = 0; i < result.topContent.rowCount; i += 1 ) {
                        htmlArray.push(ejs.render(result.topContent.rows[i].htmlcontent, {account: result.account, username: result.username, position: result.topContent.rows[i].position, size: result.topContent.rows[i].sizestyle}));
                    }
                    res.render('top', {htmlcontent: htmlArray});
                } else {
                    res.render('login');
                }
            })
            .catch(function(error) {
                if (error !== undefined) {
                    logger.error(error);
                }
            });
    } else {
        res.render('login');
    }
});

module.exports = router;
