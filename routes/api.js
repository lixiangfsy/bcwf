'use strict';
var logger = require('../function/logger')();
var express = require('express');
var router = express.Router();

// API
router.post('/:apiName', function (req, res) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
    try {
        require('../api/' + req.params.apiName)(req)
            .then(function (ret) {
                res.json(ret);
            })
            .catch(function (err) {
                if (err !== undefined) {
                    logger.error(err);
                }
                res.status(500).send('Sorry we have anything error!');
            });
    } catch (err) {
        res.status(404).send('Sorry cant find that!');
    }
});
module.exports = router;
