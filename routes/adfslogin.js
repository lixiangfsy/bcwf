'use strict';
var logger = require('../function/logger')();
var express = require('express');
var router = express.Router();
var ejs = require('ejs');

router.get('/', function(req, res) {
    res.render('login');
});

router.post('/', function(req, res) {
    var apiLogin,
        htmlArray;
    apiLogin = require('../api/login');
    if (req.body.SAMLResponse !== undefined && req.body.SAMLResponse !== '') {
        var str = new Buffer(req.body.SAMLResponse,'base64').toString();
        req.body.userId = str.substring(str.indexOf('<NameID>')+8,str.indexOf('</NameID>'));
        apiLogin(req)
            .then(function(result) {
                if (result.resultCode === 0) {
                    htmlArray = [];
                    for (var i = 0; i < result.topContent.rowCount; i += 1 ) {
                        htmlArray.push(ejs.render(result.topContent.rows[i].htmlcontent, {account: result.account, username: result.username, position: result.topContent.rows[i].position, size: result.topContent.rows[i].sizestyle}));
                    }
                    res.render('top', {htmlcontent: htmlArray});
                } else {
                    res.render('login');
                }
            })
            .catch(function(error) {
                if (error !== undefined) {
                    logger.error(error);
                }
            });
    } else {
        res.render('login');
    }
});

module.exports = router;
