'use strict';
var logger = require('../function/logger')();
var express = require('express');
var router = express.Router();

// Todo 実装しない ファイルダウロード
router.get('/:filePath/:fileName', function (req, res) {
    var fileName,
        filePath,
        fs,
        path,
        stats,
        tmpFilePath;

    path = require('path');
    fs = require('fs');
    fileName = req.params.fileName;
    tmpFilePath = req.params.filePath;
    filePath = path.join(__dirname, './../public/uploads', filePath, fileName);
    stats = fs.statSync(filePath);
    if (stats.isFile()) {
        res.set({
            'Content-Type': 'application/octet-stream',
            'Content-Disposition': 'attachment; filename=' + fileName,
            'Content-Length': stats.size
        });
        fs.createReadStream(filePath).pipe(res);
    } else {
        res.end(404);
    }
});
module.exports = router;
