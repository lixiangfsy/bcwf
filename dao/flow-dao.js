/*global Promise */
'use strict';
var pool = require('../dao/dao').pool();

module.exports = {
    /**
     * 諸届に割当てられているフローとそのスタート位置を取得
     * @param  {Object} req
     * @param  {String} req.applyFormCd 諸届CD
     * @param  {Object} [client] プール又はクライアントインスタンス
     * @return {Object} Promise
     */
    prmsSelectOFlowStart: function (req, client) {
        var sql;
        client = client || pool;
        sql =
            ' SELECT ' +
            '     applyform_r_flow.applyformcd ' +
            '     , flow.flowcd ' +
            '     , flow.flowname ' +
            '     , fstart.processcd ' +
            '     , process.processname ' +
            '     , fstart.orderno ' +
            '     , fstart.condition ' +
            ' FROM ' +
            '     e_flow flow ' +
            ' INNER JOIN ' +
            '     applyform_r_flow ' +
            '     ON  applyform_r_flow.flowcd = flow.flowcd ' +
            ' INNER JOIN ' +
            '     o_flow_start fstart ' +
            '     ON fstart.flowcd = applyform_r_flow.flowcd ' +
            ' INNER JOIN ' +
            '     e_process process ' +
            '     ON process.processcd = fstart.processcd ' +
            ' WHERE ' +
            '     applyform_r_flow.applyformcd = $1 ' +
            ' ORDER BY fstart.orderno ';
        return client.query(
            sql,
            [
                req.applyFormCd
            ]
        );
    },

    //瀋陽マージ

    /**
     * 次のプロセスを取得する
     * @param  {Object} req
     * @param  {Integer} req.applyId 申請ID
     * @param  {Object} [client] プール又はクライアントインスタンス
     * @return {Object} Promise
     */
    prmsGetOFlowRProcessAfter: function (req, client) {
        var sql;
        client = client || pool;
        sql =
            ' SELECT ' +
            '     afterprocesscd ' +
            '     ,nexttask.taskid ' +
            ' FROM ' +
            '     o_flow_r_process_after processafter ' +
            ' INNER JOIN ' +
            '     t_task ' +
            ' ON  t_task.processcd = processafter.processcd ' +
            ' AND t_task.activestatus = 1 ' +
            ' INNER JOIN ' +
            '     t_apply ' +
            ' ON  t_apply.applyid = t_task.applyid ' +
            ' INNER JOIN ' +
            '     applyform_r_flow ' +
            ' ON  applyform_r_flow.applyformcd = t_apply.applyformcd ' +
            ' AND applyform_r_flow.flowcd = processafter.flowcd ' +
            ' INNER JOIN ( ' +
            '     SELECT ' +
            '         applyid ' +
            '         ,taskid ' +
            '         ,processcd ' +
            '     FROM ' +
            '         t_task ' +
            '     WHERE ' +
            '         applyid = $1 ' +
            '     AND ' +
            '         activestatus = 0 ' +
            ' ) nexttask ' +
            ' ON  nexttask.processcd = processafter.afterprocesscd ' +
            ' WHERE ' +
            '     t_apply.applyid = $1 ';
        return client.query(
            sql,
            [
                req.applyId
            ]
        );
    },
};
