/*global */
'use strict';
var pg = require('pg');

var postgreSqlConfig = require('../config/postgre-sql.config.json');

module.exports = {
    pool: function () {
        var Pool;

        Pool = pg.Pool;
        return new Pool(postgreSqlConfig);
    },
    prmsBegin: function (client) {
        return client.query('BEGIN');
    },
    prmsCommit: function (client) {
        return client.query('COMMIT');
    },
    prmsRollback: function (client) {
        return client.query('ROLLBACK');
    },
};
