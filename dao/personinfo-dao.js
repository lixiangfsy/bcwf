/*global Promise */
'use strict';
var pool = require('../dao/dao').pool();

module.exports = {
    /**
     * 所属と役職を取得する
     * @param  {Object} prmsGetAccountReq
     * @param  {string} prmsGetAccountReq.personCd パーソンCD
     * @param  {string} prmsGetAccountReq.baseDate 基準日
     * @return {Object} Promise
     */
    prmsGetAccount: function (prmsGetAccountReq, client) {
        var sql;
        client = client || pool;
        sql =
            'SELECT ' +
            '    p_account.accountcd AS accountcd ' +
            '   ,o_account_name.accountname AS accountname ' +
            '   ,p_account.positioncd AS positioncd ' +
            '   ,o_position_name.positionname AS positionname ' +
            'FROM ' +
            '    e_person person ' +
            'INNER JOIN ' +
            '    account_r_person p_account ' +
            '    ON  p_account.personcd = person.personcd ' +
            '    AND person.deleteflg = FALSE ' +
            'INNER JOIN ( ' +
            '    SELECT ' +
            '        accountcd ' +
            '       ,MAX(validstartdate) validstartdate ' +
            '    FROM ' +
            '        o_account_name ' +
            '    WHERE ' +
            '        o_account_name.deleteflg = FALSE ' +
            '        AND o_account_name.validstartdate <= $2 ' +
            '    GROUP BY ' +
            '        accountcd ' +
            '    ) account ' +
            '    ON  account.accountcd = p_account.accountcd ' +
            'INNER JOIN ' +
            '    o_account_name ' +
            '    ON  o_account_name.accountcd = account.accountcd ' +
            '    AND o_account_name.validstartdate = account.validstartdate ' +
            'LEFT JOIN ( ' +
            '    SELECT ' +
            '        positioncd ' +
            '       ,MAX(validstartdate) validstartdate ' +
            '    FROM ' +
            '        o_position_name ' +
            '    WHERE ' +
            '        o_position_name.deleteflg = FALSE ' +
            '    AND o_position_name.validstartdate <= $2 ' +
            '    GROUP BY ' +
            '        positioncd ' +
            '    ) position ' +
            '    ON  position.positioncd = p_account.positioncd ' +
            'LEFT JOIN ' +
            '    o_position_name ' +
            '    ON  o_position_name.positioncd = position.positioncd ' +
            '    AND o_position_name.validstartdate = position.validstartdate ' +
            'WHERE person.personcd = $1 ';
        return client.query(
            sql,
            [
                prmsGetAccountReq.personCd,
                prmsGetAccountReq.baseDate
            ]
        );
    },

    /**
     * パーソン区分を取得する
     * @param  {Object} prmsGetAccountReq
     * @param  {string} prmsGetAccountReq.personCd パーソンCD
     * @param  {string} prmsGetAccountReq.baseDate 基準日
     * @return {Object} Promise
     */
    prmsGetPersonType: function (prmsGetPersonTypeReq, client) {
        var sql;
        client = client || pool;
        sql =
            'SELECT ' +
            '    p_persontype.persontypecd AS persontypecd ' +
            '   ,c_persontype.persontypename AS persontypename ' +
            'FROM ' +
            '    e_person person ' +
            'INNER JOIN ' +
            '    person_r_persontype p_persontype ' +
            '    ON  p_persontype.personcd = person.personcd ' +
            '    AND person.deleteflg = FALSE ' +
            'INNER JOIN ' +
            '    c_persontype ' +
            '    ON  c_persontype.persontypecd = p_persontype.persontypecd ' +
            'WHERE ' +
            '    person.personcd = $1 ' +
            'AND c_persontype.deleteflg = FALSE ' +
            'AND c_persontype.validstartdate <= $2 ';
        return client.query(
            sql,
            [
                prmsGetPersonTypeReq.personCd,
                prmsGetPersonTypeReq.baseDate
            ]
        );
    },

    /**
     * 役割を取得する
     * @param  {Object} prmsGetAccountReq
     * @param  {string} prmsGetAccountReq.personCd パーソンCD
     * @param  {string} prmsGetAccountReq.baseDate 基準日
     * @return {Object} Promise
     */
    prmsGetRole: function (prmsGetRoleReq, client) {
        var sql;
        client = client || pool;
        sql =
            'SELECT ' +
            '    p_role.rolecd AS rolecd ' +
            '   ,o_role_name.rolename AS rolename ' +
            'FROM ' +
            '    e_person person ' +
            'INNER JOIN ' +
            '    person_r_role p_role ' +
            '    ON  p_role.personcd = person.personcd ' +
            '    AND person.deleteflg = FALSE ' +
            'INNER JOIN ( ' +
            'SELECT ' +
            '    rolecd ' +
            '   ,MAX(validstartdate) validstartdate ' +
            'FROM ' +
            '    o_role_name ' +
            'WHERE ' +
            '    deleteflg =FALSE ' +
            'AND validstartdate <= $2 ' +
            'GROUP BY ' +
            '    rolecd ' +
            ') roleName ' +
            'ON  roleName.rolecd = p_role.rolecd ' +
            'INNER JOIN ' +
            '    o_role_name ' +
            '    ON  o_role_name.rolecd = roleName.rolecd ' +
            '    AND o_role_name.validstartdate = roleName.validstartdate ' +
            'WHERE ' +
            '    person.personcd = $1 ';
        return client.query(
            sql,
            [
                prmsGetRoleReq.personCd,
                prmsGetRoleReq.baseDate
            ]
        );
    },

    /**
     * 所属からパーソンを取得する
     * @param  {Object} prmsGetPersonByAccountReq
     * @param  {string} prmsGetPersonByAccountReq.accountCd 所属CD
     * @return {Object} Promise
     */
    prmsGetPersonByAccount: function (prmsGetPersonByAccountReq, client) {
        var sql;
        client = client || pool;
        sql =
            'SELECT ' +
            '    e_person.personcd ' +
            'FROM ' +
            '    e_person ' +
            'INNER JOIN ' +
            '    account_r_person ' +
            'ON  account_r_person.personcd = e_person.personcd ' +
            'WHERE ' +
            '    e_person.deleteflg = FALSE ' +
            'AND ' +
            '    account_r_person.accountcd = ANY(string_to_array($1, \',\'))';
        return client.query(
            sql,
            [
                prmsGetPersonByAccountReq.join(','),
            ]
        );
    },

    /**
     * 役職からパーソンを取得する
     * @param  {Object} prmsGetPersonByPositionReq
     * @param  {string} prmsGetPersonByPositionReq.positionCd 役職CD
     * @return {Object} Promise
     */
    prmsGetPersonByPosition: function (prmsGetPersonByPositionReq, client) {
        var sql;
        client = client || pool;
        sql =
            'SELECT ' +
            '    e_person.personcd ' +
            'FROM ' +
            '    e_person ' +
            'INNER JOIN ' +
            '    account_r_person ' +
            'ON  account_r_person.personcd = e_person.personcd ' +
            'WHERE ' +
            '    e_person.deleteflg = FALSE ' +
            'AND ' +
            '    account_r_person.positioncd = ANY(string_to_array($1, \',\'))';
        return client.query(
            sql,
            [
                prmsGetPersonByPositionReq.join(','),
            ]
        );
    },

    /**
     * パーソンタイプからパーソンを取得する
     * @param  {Object} prmsGetPersonByPersonTypeReq
     * @param  {string} prmsGetPersonByPersonTypeReq.persontypeCd パーソンタイプCD
     * @return {Object} Promise
     */
    prmsGetPersonByPersonType: function (prmsGetPersonByPersonTypeReq, client) {
        var sql;
        client = client || pool;
        sql =
            'SELECT ' +
            '    e_person.personcd ' +
            'FROM ' +
            '    e_person ' +
            'INNER JOIN ' +
            '    person_r_persontype ' +
            'ON  person_r_persontype.personcd = e_person.personcd ' +
            'WHERE ' +
            '    e_person.deleteflg = FALSE ' +
            'AND ' +
            '    person_r_persontype.persontypecd = ANY(string_to_array($1, \',\'))';
        return client.query(
            sql,
            [
                prmsGetPersonByPersonTypeReq.join(','),
            ]
        );
    },

    /**
     * パーソンタイプからパーソンを取得する
     * @param  {Object} prmsGetPersonByRoleReq
     * @param  {string} prmsGetPersonByRoleReq.roleCd 役割CD
     * @return {Object} Promise
     */
    prmsGetPersonByRole: function (prmsGetPersonByRoleReq, client) {
        var sql;
        client = client || pool;
        sql =
            'SELECT ' +
            '    e_person.personcd ' +
            'FROM ' +
            '    e_person ' +
            'INNER JOIN ' +
            '    person_r_role ' +
            'ON  person_r_role.personcd = e_person.personcd ' +
            'WHERE ' +
            '    e_person.deleteflg = FALSE ' +
            'AND ' +
            '    person_r_role.rolecd = ANY(string_to_array($1, \',\'))';
        return client.query(
            sql,
            [
                prmsGetPersonByRoleReq.join(','),
            ]
        );
    },

    /**
     * パーソン名称を取得する
     * @param  {Object} prmsGetPersonNameReq
     * @param  {string} prmsGetPersonNameReq.personCd
     * @return {Object} Promise
     */
    prmsGetPersonName: function (prmsGetPersonNameReq, client) {
        var sql;
        client = client || pool;
        sql =
            'SELECT ' +
            '    o_person_name.personname ' +
            'FROM ' +
            '    o_person_name ' +
            'INNER JOIN ( ' +
            '    SELECT ' +
            '        personcd ' +
            '       ,MAX(validstartdate) AS validstartdate ' +
            '    FROM ' +
            '        o_person_name ' +
            '    WHERE ' +
            '        deleteflg = FALSE ' +
            '    AND ' +
            '        personcd = $1 ' +
            '    AND ' +
            '        validstartdate < CURRENT_TIMESTAMP ' +
            '    GROUP BY ' +
            '        personcd ' +
            ')   maxValidstartdate ' +
            'ON  maxValidstartdate.personcd = o_person_name.personcd ' +
            'AND maxValidstartdate.validstartdate = o_person_name.validstartdate ' +
            ' ' +
            'WHERE ' +
            '    o_person_name.deleteflg = FALSE ' +
            'AND ' +
            '    o_person_name.validstartdate < CURRENT_TIMESTAMP ' +
            'AND ' +
            '    o_person_name.personcd = $1 ';
        return client.query(
            sql,
            [
                prmsGetPersonNameReq.personCd,
            ]
        );
    },
};
