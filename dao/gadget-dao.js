/*global Promise */
'use strict';
var pool = require('../dao/dao').pool();

module.exports = {
	/**
     * gadgetを取得する
     * @param  {Object} req
     * @param  {integer} req.body.pageId 画面ID
     * @param  {string} req.body.gadgetId ガジェットID
     * @param  {Object} [client] プール又はクライアントインスタンス
     * @return {Object} Promise
     */
	prmsGetGadgetInfo: function (req, client) {
        var condition,
            paramArray,
            sql;
            paramArray = [req.pageId];
        client = client || pool;
        condition = '';
        if (req.gadgetId !== undefined &&  req.gadgetId !== '') {
            condition = ' AND t1.gadgetid = $2 ';
            paramArray.push(req.gadgetId);
        }
        sql =
            ' SELECT ' + 
            ' 	 t1.gadgetid, ' + 
            '    t1.htmlcontent, ' + 
            '    t2.position, ' + 
            '    t2.sizestyle ' +
            ' FROM ' + 
            '    e_gadget t1 ' +
            ' INNER JOIN ' + 
            '    page_r_gadget t2 ' +
            ' ON ' + 
            '    t1.gadgetid = t2.gadgetid ' +
            ' AND t2.pageid = $1 ' +
            condition +
            ' INNER JOIN ' + 
            '    e_page t3 ' +
            ' ON ' + 
            '    t2.pageid = t3.pageid ';
        return client.query(
            sql,
            paramArray
        );
    }
};