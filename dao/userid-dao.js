/*global Promise */
'use strict';
var pool = require('../dao/dao').pool();

module.exports = {
    // 他システムのユーザIDと本システムのパーソンCDをマッピングする
    prmsSelectPersonCd: function (prmsSelectPersonCdReq, client) {
        var sql;
        client = client || pool;
        sql =
            'SELECT ' +
            '    personcd ' +
            'FROM ' +
            '    e_userid ' +
            'WHERE ' +
            '    userid = $1 ';
        return client.query(
            sql,
            [
                prmsSelectPersonCdReq.userId
            ]
        );
    },

    /**
     * ユーザーIDとパスワードを通して、ユーザー情報を取得する
     * @param  {Object} prmsGetUserInfo
     * @param  {string} prmsGetUserInfo.userId ユーザーID
     * @param  {string} prmsGetUserInfo.password パスワード
     * @param  {Object} [client] プール又はクライアントインスタンス
     * @return {Object} Promise
     */
    prmsGetUserInfoByUserId: function (prmsGetUserInfoReq, client) {
        var sql;
        client = client || pool;
        sql =
            'SELECT ' +
            '    userInfo.userid ' +
            '    , e_password."password" ' +
            '    , e_password.personcd ' +
            'FROM ' +
            '    ( ' +
            '        SELECT ' +
            '            e_userid.userid ' +
            '            , e_person.personcd ' +
            '            , e_person.deleteflg ' +
            '        FROM ' +
            '            e_userid ' +
            '        INNER JOIN ' +
            '            e_person ' +
            '            ON e_userid.personcd = e_person.personcd ' +
            '     ) AS userInfo ' +
            'INNER JOIN ' +
            '    e_password ' +
            '    ON userInfo.personcd = e_password.personcd ' +
            'WHERE ' +
            '    userInfo.userid = e_password.userid ' +
            '    AND userInfo.personcd = e_password.personcd ' +
            '    AND userInfo.deleteflg = FALSE ' +
            '    AND e_password.deleteflg = FALSE ' +
            '    AND e_password.validstartdate <= CURRENT_TIMESTAMP ';
        if (prmsGetUserInfoReq.password === undefined || prmsGetUserInfoReq.password === '') {
            sql += ' AND userInfo.userid = $1 ';
            return client.query(
                sql,
                [
                    prmsGetUserInfoReq.userId
                ]
            );
        } else {
            sql += ' AND userInfo.userid = $1 ' +
                   ' AND e_password."password" = $2 ';
            return client.query(
                sql,
                [
                    prmsGetUserInfoReq.userId,
                    prmsGetUserInfoReq.password
                ]
            );
        }
    }
};
