/*global Promise */
'use strict';
var pool = require('../dao/dao').pool();

module.exports = {
    /**
     * プロセス引数を取得
     * @param  {Object} req
     * @param  {String} req.taskId タスクID
     * @param  {Object} [client] プール又はクライアントインスタンス
     * @return {Object} Promise
     */
    prmsSelectOProcessMasterByTaskId: function (req, client) {
        var sql;
        client = client || pool;
        sql =
            ' SELECT ' +
            '     O_Process_Master.SourceFileName ' +
            ' FROM O_Process_Master ' +
            ' INNER JOIN T_Task ' +
            ' ON ' +
            '     T_Task.ProcessCd = O_Process_Master.ProcessCd ' +
            ' WHERE ' +
            '     T_Task.TaskId = $1 ';
        return client.query(
            sql,
            [
                req.taskId,
            ]
        );
    },
};
