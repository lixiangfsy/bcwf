/*global Promise */
'use strict';
var logger = require('../function/logger')();

var pool = require('../dao/dao').pool();

module.exports = {
    /**
     * パーソンテーブルからメールアドレスを取得する
     * @param  {string[]} params パーソンCD
     * @resolve {Object}
     */
    prmsSelectOPersonMailaddress : function (params, client) {
        var sql;
        client = client || pool;
        sql =
            'SELECT' +
            '   mailaddress ' +
            'FROM o_person_mailaddress ' +
            'WHERE personcd = ANY(string_to_array($1, \',\'))';
        return client.query(
            sql,
            [
                params.join(',')
            ]
        );
    }
};
