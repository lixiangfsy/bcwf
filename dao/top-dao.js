/*global Promise */
'use strict';
var pool = require('../dao/dao').pool();

module.exports = {
    /**
     * top画面を取得する
     * @param  {Object} prmsGetTop
     * @param  {Object} [client] プール又はクライアントインスタンス
     * @return {Object} Promise
     */
    prmsGetTop: function (prmsGetTop, client) {
        var sql;
        client = client || pool;
        sql =
            ' SELECT ' +
            '     e_gadget.htmlcontent ' +
            '     ,page_r_gadget."position" ' +
            '     ,page_r_gadget.sizestyle ' +
            ' FROM ' +
            '     e_page ' +
            ' INNER JOIN ' +
            '     page_r_gadget ' +
            ' ON  e_page.pageid = page_r_gadget.pageid ' +
            ' INNER JOIN e_gadget ' +
            ' ON  page_r_gadget.gadgetid = e_gadget.gadgetid ' +
            ' WHERE ' +
            '     e_page.pageid = $1 ' +
            ' ORDER BY ' +
            '     page_r_gadget."gadgetIndex"';
        return client.query(
            sql,
            [prmsGetTop.pageid]
        );
    }
};
