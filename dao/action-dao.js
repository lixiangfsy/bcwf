/*global Promise */
'use strict';
var pool = require('../dao/dao').pool();

module.exports = {
    /**
     * コメントテーブル追加
     * @param  {Object} req
     * @param  {Object} req.applyId
     * @param  {Object} req.personCd
     * @param  {Object} req.action
     * @param  {Object} req.saveDate
     * @param  {Object} [client] プール又はクライアントインスタンス
     * @return {Object} Promise
     */
    prmsInsertTAction: function (req, client) {
        var sql;
        client = client || pool;
        sql =
            ' INSERT ' +
            ' INTO h_action( ' +
            '     taskid ' +
            '     , personcd ' +
            '     , action ' +
            '     , actiondate ' +
            ' ) VALUES ($1, $2, $3, $4) ';
        return client.query(
            sql,
            [
                req.taskId,
                req.personCd,
                req.action,
                req.saveDate,
            ]
        );
    },

    /**
     * 操作履歴を取得
     * @param  {Object} req
     * @param  {Integer} req.applyId 申請ID
     * @param  {Object} [client] プール又はクライアントインスタンス
     * @return {Object} Promise
     */
    prmsGetActionHistoryInfo: function (req, client) {
        var sql;
        client = client || pool;
        sql =
            ' SELECT ' +
            '     h_action.taskid ' +
            '     , h_action.personcd ' +
            '     , o_person_name.personname ' +
            '     , h_action.action ' +
            '     , h_action.actiondate ' +
            ' FROM ' +
            '     h_action ' +
            ' INNER JOIN ' +
            '     t_task ' +
            '     ON ' +
            '         t_task.taskid = h_action.taskid ' +
            '     AND ' +
            '         t_task.applyid = $1 ' +
            ' INNER JOIN ( ' +
            '     SELECT ' +
            '         personcd ' +
            '         , MAX(validstartdate) validstartdate ' +
            '     FROM ' +
            '         o_person_name ' +
            '     WHERE ' +
            '         deleteflg = FALSE ' +
            '     GROUP BY personcd ' +
            ' ) personName ' +
            '     ON ' +
            '     personName.personcd = h_action.personcd ' +
            ' INNER JOIN ' +
            '     o_person_name ' +
            '     ON ' +
            '         o_person_name.personcd = personName.personcd ' +
            '     AND ' +
            '         o_person_name.validstartdate = personName.validstartdate ' +
            '  ' +
            ' ORDER BY h_action.actiondate DESC ';
        return client
            .query(
                sql,
                [
                    req.applyId
                ]
            );
    },
};
