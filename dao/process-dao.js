/*global Promise */
'use strict';
var pool = require('../dao/dao').pool();

module.exports = {
    /**
     * フローCD取得
     * @param  {Object} req
     * @param  {String} req.flowCd フローCD
     * @return {Object} Promise
     */
    prmsSelectProcesscd: function (req, client) {
        var sql;
        client = client || pool;
        sql =
            ' SELECT ' +
            '     e_process.processcd AS processcd ' +
            '     , o_process_master.orderno AS orderno ' +
            ' FROM ' +
            '     e_process ' +
            ' INNER JOIN ' +
            '     flow_r_process ' +
            '     ON flow_r_process.processcd = e_process.processcd ' +
            '     AND flow_r_process.flowcd = $1 ' +
            ' LEFT JOIN ' +
            '     o_process_master ' +
            '     ON o_process_master.processcd = e_process.processcd ' +
            ' ORDER BY ' +
            '     e_process.processcd ';
        return client.query(
            sql,
            [
                req.flowCd,
            ]
        );
    },

    /**
     * アクティブステータスを次に進める
     * @param  {Object} prmsUpdateActivestatusReq
     * @param  {integer} prmsUpdateActivestatusReq.applyId 申請ID
     * @param  {integer} prmsUpdateActivestatusReq.taskId タスクID
     * @return {Object} Promise
     */
    prmsUpdateActivestatus: function (prmsUpdateActivestatusReq, client) {
        var sql;
        client = client || pool;
        sql =
            'UPDATE ' +
            '    t_task ' +
            'SET ' +
            '    activestatus = activestatus + 1 ' +
            'WHERE ' +
            '    applyid = $1 ' +
            'AND taskid = $2 ' +
            'AND activestatus <= 2 ';
        return client.query(
            sql,
            [
                prmsUpdateActivestatusReq.applyId,
                prmsUpdateActivestatusReq.taskId,
            ]
        );
    },

    /**
     * プロセス名称、アクション名を取得
     * @param  {Object} req
     * @param  {String} req.taskId タスクID
     * @return {Object} Promise
     */
    prmsSelectProcessNameByTaskId: function (req, client) {
        var sql;
        client = client || pool;
        sql =
            ' SELECT ' +
            '     e_process.processname ' +
            '     , e_process.actionname ' +
            ' FROM ' +
            '     e_process ' +
            ' INNER JOIN ' +
            '     t_task ' +
            '     ON e_process.processcd = t_task.processcd ' +
            ' WHERE ' +
            '     t_task.taskid = $1 ';
        return client.query(
            sql,
            [
                req.taskId,
            ]
        );
    },
};
