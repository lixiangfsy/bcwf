/*global Promise */
'use strict';
var pool = require('../dao/dao').pool();

module.exports = {
    /**
     * アクティブステータスを次に進める
     * @param  {Object} req
     * @param  {integer} req.applyId 申請ID
     * @param  {integer} req.taskId タスクID
     * @return {Object} Promise
     */
    prmsUpdateActivestatus: function (req, client) {
        var sql;
        client = client || pool;
        sql =
            ' UPDATE ' +
            '     t_task ' +
            ' SET ' +
            '     activestatus = activestatus + 1 ' +
            ' WHERE ' +
            '     applyid = $1 ' +
            '     AND taskid = $2 ' +
            '     AND activestatus <= 2 ';
        return client.query(
            sql,
            [
                req.applyId,
                req.taskId,
            ]
        );
    },

    /**
     * タスクID取得
     * @param  {Object} req
     * @param  {Object} [client] プール又はクライアントインスタンス
     * @return {Object} Promise
     */
    prmsSelectTaskIdSeq: function (req, client) {
        var sql;
        client = client || pool;
        sql =
            ' SELECT nextval(\'t_task_taskid_seq\') as taskid ';
        return client.query(
            sql,
            []
        );
    },

    /**
     * タスクテーブル追加
     * @param  {Object} req
     * @param  {Integer} req.taskId タスクID
     * @param  {Integer} req.applyId 申請ID
     * @param  {String} req.processCd パーソンCD
     * @param  {Integer} req.startTaskId スタートタスクID
     * @param  {Object} [client] プール又はクライアントインスタンス
     * @return {Object} Promise
     */
    prmsInsertTTask: function (req, client) {
        var sql;
        client = client || pool;
        sql =
            ' INSERT ' +
            ' INTO t_task ( ' +
            '     taskid ' +
            '     , applyid ' +
            '     , processcd ' +
            '     , activestatus ' +
            '     , orderno ' +
            ' ) VALUES (' +
            '     $1 ' +
            '     , $2 ' +
            '     , $3 ' +
            '     , $4 ' +
            '     , $5 ' +
            ' ) ';
        return client.query(
            sql,
            [
                req.taskId,
                req.applyId,
                req.processCd,
                req.taskId === req.startTaskId ? 1 : 0,
                req.orderNo || 0,
            ]
        );
    },

    /**
     * 処理者テーブル追加
     * @param  {Object} req
     * @param  {Integer} req.taskId タスクID
     * @param  {String} req.processCd プロセスCD
     * @param  {String} req.personCd パーソンCD
     * @param  {Integer} req.startTaskId スタートタスクID
     * @param  {Object} [client] プール又はクライアントインスタンス
     * @return {Object} Promise
     */
    prmsInsertRTaskProcessor: function (req, client) {
        var sql,
            palam;
        client = client || pool;
        if (req.taskId === req.startTaskId) {
            //スタートタスクの場合は自分のみを処理者として登録する
            sql =
                ' INSERT INTO person_r_task_processor ( ' +
                '     taskid ' +
                '     , personcd ' +
                ' ) ' +
                ' SELECT ' +
                '     $1, ' +
                '     personcd ' +
                ' FROM ' +
                '     process_r_person ' +
                ' WHERE ' +
                '     processcd = $2 ' +
                ' AND ' +
                '     personcd = $3 ';
            palam = [
                req.taskId,
                req.processCd,
                req.personCd,
            ];
        } else {
            sql =
                ' INSERT INTO person_r_task_processor ( ' +
                '     taskid ' +
                '     , personcd ' +
                ' ) ' +
                ' SELECT ' +
                '     $1, ' +
                '     personcd ' +
                ' FROM ' +
                '     process_r_person ' +
                ' WHERE ' +
                '     processcd = $2 ';
            palam = [
                req.taskId,
                req.processCd,
            ];
        }
        return client.query(sql, palam);
    },

    /**
     * 処理所属テーブル追加
     * @param  {Object} req
     * @param  {Integer} req.taskId タスクID
     * @param  {String} req.processCd プロセスCD
     * @param  {Object} [client] プール又はクライアントインスタンス
     * @return {Object} Promise
     */
    prmsInsertTaskRAccountProcessor: function (req, client) {
        var sql;
        client = client || pool;
        sql =
            ' INSERT INTO Task_R_Account_Processor ( ' +
            '     taskid ' +
            '     , accountcd ' +
            ' ) ' +
            ' SELECT ' +
            '     $1, ' +
            '     accountcd ' +
            ' FROM ' +
            '     Process_R_Account ' +
            ' WHERE ' +
            '     processcd = $2 ';
        return client.query(
            sql,
            [
                req.taskId,
                req.processCd,
            ]
        );
    },

    /**
     * 処理役職テーブル追加
     * @param  {Object} req
     * @param  {Integer} req.taskId タスクID
     * @param  {String} req.processCd プロセスCD
     * @param  {Object} [client] プール又はクライアントインスタンス
     * @return {Object} Promise
     */
    prmsInsertTaskRPositionProcessor: function (req, client) {
        var sql;
        client = client || pool;
        sql =
            ' INSERT INTO Task_R_Position_Processor ( ' +
            '     taskid ' +
            '     , positioncd ' +
            ' ) ' +
            ' SELECT ' +
            '     $1, ' +
            '     positioncd ' +
            ' FROM ' +
            '     Process_R_Position ' +
            ' WHERE ' +
            '     processcd = $2 ';
        return client.query(
            sql,
            [
                req.taskId,
                req.processCd,
            ]
        );
    },

    /**
     * 処理役割テーブル追加
     * @param  {Object} req
     * @param  {Integer} req.taskId タスクID
     * @param  {String} req.processCd プロセスCD
     * @param  {Object} [client] プール又はクライアントインスタンス
     * @return {Object} Promise
     */
    prmsInsertTaskRRoleProcessor: function (req, client) {
        var sql;
        client = client || pool;
        sql =
            ' INSERT INTO Task_R_Role_Processor ( ' +
            '     taskid ' +
            '     , rolecd ' +
            ' ) ' +
            ' SELECT ' +
            '     $1, ' +
            '     rolecd ' +
            ' FROM ' +
            '     Process_R_Role ' +
            ' WHERE ' +
            '     processcd = $2 ';
        return client.query(
            sql,
            [
                req.taskId,
                req.processCd,
            ]
        );
    },

    /**
     * 処理パーソンテーブル追加
     * @param  {Object} req
     * @param  {Integer} req.taskId タスクID
     * @param  {String} req.processCd プロセスCD
     * @param  {Object} [client] プール又はクライアントインスタンス
     * @return {Object} Promise
     */
    prmsInsertTaskRPersonTypeProcessor: function (req, client) {
        var sql;
        client = client || pool;
        sql =
            ' INSERT INTO Task_R_PersonType_Processor ( ' +
            '     taskid ' +
            '     , persontypecd ' +
            ' ) ' +
            ' SELECT ' +
            '     $1, ' +
            '     persontypecd ' +
            ' FROM ' +
            '     Process_R_PersonType ' +
            ' WHERE ' +
            '     processcd = $2 ';
        return client.query(
            sql,
            [
                req.taskId,
                req.processCd,
            ]
        );
    },

    /**
     * 参照者テーブル追加
     * @param  {Object} req
     * @param  {Integer} req.taskId タスクID
     * @param  {String} req.processCd プロセスCD
     * @param  {Object} [client] プール又はクライアントインスタンス
     * @return {Object} Promise
     */
    prmsInsertRTaskReference: function (req, client) {
        var sql;
        client = client || pool;
        sql =
            ' INSERT INTO Person_R_Task_Reference ( ' +
            '     taskid ' +
            '     , personcd ' +
            ' ) ' +
            ' SELECT ' +
            '     $1, ' +
            '     personcd ' +
            ' FROM ' +
            '     Process_R_Person_Reference ' +
            ' WHERE ' +
            '     processcd = $2 ';
        return client.query(
            sql,
            [
                req.taskId,
                req.processCd,
            ]
        );
    },

    /**
     * 参照所属テーブル追加
     * @param  {Object} req
     * @param  {Integer} req.taskId タスクID
     * @param  {String} req.processCd プロセスCD
     * @param  {Object} [client] プール又はクライアントインスタンス
     * @return {Object} Promise
     */
    prmsInsertTaskRAccountReference: function (req, client) {
        var sql;
        client = client || pool;
        sql =
            ' INSERT INTO Task_R_Account_Reference ( ' +
            '     taskid,  ' +
            '     accountcd ' +
            ' ) ' +
            ' SELECT ' +
            '     $1, ' +
            '     accountcd ' +
            ' FROM ' +
            '     Process_R_Account_Reference ' +
            ' WHERE ' +
            '     processcd = $2 ';
        return client.query(
            sql,
            [
                req.taskId,
                req.processCd,
            ]
        );
    },

    /**
     * 参照役職テーブル追加
     * @param  {Object} req
     * @param  {Integer} req.taskId タスクID
     * @param  {String} req.processCd プロセスCD
     * @param  {Object} [client] プール又はクライアントインスタンス
     * @return {Object} Promise
     */
    prmsInsertTaskRPositionReference: function (req, client) {
        var sql;
        client = client || pool;
        sql =
            ' INSERT INTO Task_R_Position_Reference ( ' +
            '     taskid ' +
            '     , positioncd ' +
            ' ) ' +
            ' SELECT ' +
            '     $1, ' +
            '     positioncd ' +
            ' FROM ' +
            '     Process_R_Position_Reference ' +
            ' WHERE ' +
            '     processcd = $2 ';
        return client.query(
            sql,
            [
                req.taskId,
                req.processCd,
            ]
        );
    },

    /**
     * 参照役割テーブル追加
     * @param  {Object} req
     * @param  {Integer} req.taskId タスクID
     * @param  {String} req.processCd プロセスCD
     * @param  {Object} [client] プール又はクライアントインスタンス
     * @return {Object} Promise
     */
    prmsInsertTaskRRoleReference: function (req, client) {
        var sql;
        client = client || pool;
        sql =
            ' INSERT INTO Task_R_Role_Reference ( ' +
            '     taskid ' +
            '     , rolecd ' +
            ' ) ' +
            ' SELECT ' +
            '     $1, ' +
            '     rolecd ' +
            ' FROM ' +
            '     Process_R_Role_Reference ' +
            ' WHERE ' +
            '     processcd = $2 ';
        return client.query(
            sql,
            [
                req.taskId,
                req.processCd,
            ]
        );
    },

    /**
     * 参照パーソンテーブル追加
     * @param  {Object} req
     * @param  {Integer} req.taskId タスクID
     * @param  {String} req.processCd プロセスCD
     * @param  {Object} [client] プール又はクライアントインスタンス
     * @return {Object} Promise
     */
    prmsInsertTaskRPersonTypeReference: function (req, client) {
        var sql;
        client = client || pool;
        sql =
            ' INSERT INTO Task_R_PersonType_Reference ( ' +
            '     taskid ' +
            '     , persontypecd ' +
            ' ) ' +
            ' SELECT ' +
            '     $1, ' +
            '     persontypecd ' +
            ' FROM ' +
            '     Process_R_PersonType_Reference ' +
            ' WHERE ' +
            '     processcd = $2 ';
        return client.query(
            sql,
            [
                req.taskId,
                req.processCd,
            ]
        );
    },

    // 瀋陽マージ

    /**
     * TASK情報を取得
     * @param  {Object} req
     * @param  {Integer} req.applyId 申請ID
     * @param  {Object} [client] プール又はクライアントインスタンス
     * @return {Object} Promise
     */
    prmsGetTaskInfo: function (req, client) {
        var sql;
        client = client || pool;
        sql =
            ' SELECT ' +
            '     t_task.taskid ' +
            '   , t_task.processcd ' +
            '   , e_process.processname ' +
            '   , o_process_action.actionname ' +
            '   , t_task.activestatus ' +
            ' FROM ' +
            '     t_task ' +
            ' INNER JOIN ' +
            '     e_process ' +
            '     ON e_process.processcd = t_task.processcd ' +
            ' LEFT JOIN ' +
            '     o_process_action ' +
            '     ON o_process_action.processcd = t_task.processcd ' +
            ' WHERE ' +
            '     t_task.applyid = $1 ' +
            'ORDER BY t_task.orderno';
        return client.query(
            sql,
            [
                req.applyId
            ]
        );
    },

    /**
     * 処理者情報を取得
     * @param  {Object} req
     * @param  {Integer} req.applyId 申請ID
     * @param  {Object} [client] プール又はクライアントインスタンス
     * @return {Object} Promise
     */
    prmsGetProcessorsInfo: function (req, client) {
        var sql;
        client = client || pool;
        sql =
            ' SELECT ' +
            '     processor.taskid ' +
            '     , processor.personcd ' +
            '     , o_person_name.personname ' +
            '     , o_account_name.accountcd ' +
            '     , o_account_name.accountname ' +
            ' FROM ' +
            '     person_r_task_processor processor ' +
            ' INNER JOIN ' +
            '     t_task ' +
            '     ON  t_task.taskid = processor.taskid ' +
            '     AND t_task.applyid = $1 ' +
            ' INNER JOIN ( ' +
            '     SELECT ' +
            '         personcd ' +
            '       , MAX(validstartdate) validstartdate ' +
            '     FROM ' +
            '         o_person_name ' +
            '     WHERE ' +
            '         deleteflg = FALSE ' +
            '     GROUP BY personcd ' +
            ' ) personname ' +
            '     ON  personname.personcd = processor.personcd ' +
            ' INNER JOIN ' +
            '     o_person_name ' +
            '     ON  o_person_name.personcd = personname.personcd ' +
            '     AND o_person_name.validstartdate = personname.validstartdate ' +
            ' LEFT JOIN ' +
            '     person_r_account_main ' +
            '     ON  person_r_account_main.personcd = processor.personcd ' +
            ' LEFT JOIN ( ' +
            '     SELECT ' +
            '         accountcd ' +
            '       , MAX(validstartdate) validstartdate ' +
            '     FROM ' +
            '         o_account_name ' +
            '     WHERE ' +
            '         o_account_name.deleteflg = FALSE ' +
            '     AND o_account_name.validstartdate <= current_timestamp ' +
            '     GROUP BY accountcd ' +
            ' ) account ' +
            '     ON account.accountcd = person_r_account_main.accountcd ' +
            ' LEFT JOIN ' +
            '     o_account_name ' +
            '     ON  o_account_name.accountcd = account.accountcd ' +
            '     AND o_account_name.validstartdate = account.validstartdate ' +
            ' ORDER BY t_task.orderno ASC ';
        return client.query(
            sql,
            [
                req.applyId
            ]
        );
    },
    /**
     * 所属：処理者情報を取得
     * @param  {Object} req
     * @param  {Integer} req.applyId 申請ID
     * @param  {Object} [client] プール又はクライアントインスタンス
     * @return {Object} Promise
     */
    prmsGetAccountProcessorInfo: function (req, client) {
        var sql;
        client = client || pool;
        sql =
            ' SELECT ' +
            '     processor.taskid ' +
            '     , processor.accountcd ' +
            '     , o_account_name.accountname ' +
            ' FROM ' +
            '     Task_R_Account_Processor processor ' +
            ' LEFT JOIN ( ' +
            '     SELECT ' +
            '         accountcd ' +
            '         , MAX(validstartdate) validstartdate ' +
            '     FROM ' +
            '         o_account_name ' +
            '     WHERE ' +
            '         o_account_name.deleteflg = FALSE ' +
            '         AND o_account_name.validstartdate <= current_timestamp ' +
            '     GROUP BY accountcd ' +
            ' ) account ' +
            '     ON account.accountcd = processor.accountcd ' +
            ' LEFT JOIN ' +
            '     o_account_name ' +
            '     ON o_account_name.accountcd = account.accountcd ' +
            '     AND o_account_name.validstartdate = account.validstartdate ' +
            ' INNER JOIN ' +
            '     t_task ' +
            '     ON t_task.taskid = processor.taskid ' +
            '     AND t_task.applyid = $1 ' +
            ' ORDER BY taskid ASC ';
        return client.query(
            sql,
            [
                req.applyId
            ]
        );
    },

    /**
     * 区分：処理者情報を取得
     * @param  {Object} req
     * @param  {Integer} req.applyId 申請ID
     * @param  {Object} [client] プール又はクライアントインスタンス
     * @return {Object} Promise
     */
    prmsGetPersonTypeProcessorInfo: function (req, client) {
        var sql;
        client = client || pool;
        sql =
            ' SELECT ' +
            '     processor.taskid ' +
            '     , processor.persontypecd ' +
            '     , c_persontype.persontypename ' +
            ' FROM ' +
            '     Task_R_PersonType_Processor processor ' +
            ' INNER JOIN ' +
            '     c_persontype ' +
            '     ON  c_persontype.persontypecd = processor.persontypecd ' +
            '     AND c_persontype.deleteflg = FALSE ' +
            '     AND c_persontype.validstartdate <= current_timestamp ' +
            ' INNER JOIN ' +
            '     t_task ' +
            '     ON t_task.taskid = processor.taskid ' +
            '     AND t_task.applyid = $1 ' +
            ' ORDER BY taskid ASC ';
        return client.query(
            sql,
            [
                req.applyId
            ]
        );
    },

    /**
     * 役職：処理者情報を取得
     * @param  {Object} req
     * @param  {Integer} req.applyId 申請ID
     * @param  {Object} [client] プール又はクライアントインスタンス
     * @return {Object} Promise
     */
    prmsGetPositionProcessorInfo: function (req, client) {
        var sql;
        client = client || pool;
        sql =
            ' SELECT ' +
            '     processor.taskid ' +
            '     , processor.positioncd ' +
            '     , o_position_name.positionname ' +
            ' FROM ' +
            '     task_r_position_processor processor ' +
            ' LEFT JOIN ( ' +
            '     SELECT ' +
            '         positioncd ' +
            '         , MAX(validstartdate) validstartdate ' +
            '     FROM ' +
            '         o_position_name ' +
            '     WHERE ' +
            '         o_position_name.deleteflg = FALSE ' +
            '         AND o_position_name.validstartdate <= current_timestamp ' +
            '     GROUP BY positioncd ' +
            ' ) position ' +
            '     ON position.positioncd = processor.positioncd ' +
            ' LEFT JOIN ' +
            '     o_position_name ' +
            '     ON o_position_name.positioncd = position.positioncd ' +
            '     AND o_position_name.validstartdate = position.validstartdate ' +
            ' INNER JOIN ' +
            '     t_task ' +
            '     ON t_task.taskid = processor.taskid ' +
            '     AND t_task.applyid = $1 ' +
            ' ORDER BY taskid ASC ';
        return client.query(
            sql,
            [
                req.applyId
            ]
        );
    },

    /**
     * 役割：処理者情報を取得
     * @param  {Object} req
     * @param  {Integer} req.applyId 申請ID
     * @param  {Object} [client] プール又はクライアントインスタンス
     * @return {Object} Promise
     */
    prmsGetRoleProcessorInfo: function (req, client) {
        var sql;
        client = client || pool;
        sql =
            ' SELECT ' +
            '     processor.taskid ' +
            '     , processor.rolecd ' +
            '     , o_role_name.rolename ' +
            ' FROM ' +
            '     Task_R_Role_Processor processor ' +
            ' INNER JOIN ( ' +
            '     SELECT ' +
            '         rolecd ' +
            '         , MAX(validstartdate) validstartdate ' +
            '     FROM ' +
            '         o_role_name ' +
            '     WHERE ' +
            '         deleteflg =FALSE ' +
            '         AND validstartdate <= current_timestamp ' +
            '     GROUP BY rolecd ' +
            ' ) roleName ' +
            '     ON roleName.rolecd = processor.rolecd ' +
            ' INNER JOIN ' +
            '     o_role_name ' +
            '     ON o_role_name.rolecd = roleName.rolecd ' +
            '     AND o_role_name.validstartdate = roleName.validstartdate ' +
            ' INNER JOIN ' +
            '     t_task ' +
            '     ON t_task.taskid = processor.taskid ' +
            '     AND t_task.applyid = $1 ' +
            ' ORDER BY taskid ASC ';
        return client.query(
            sql,
            [
                req.applyId
            ]
        );
    },

    /**
     * 参照者情報を取得
     * @param  {Object} req
     * @param  {Integer} req.applyId 申請ID
     * @param  {Object} [client] プール又はクライアントインスタンス
     * @return {Object} Promise
     */
    prmsGetReferencesInfo: function (req, client) {
        var sql;
        client = client || pool;
        sql =
            ' SELECT ' +
            '     reference.taskid ' +
            '     , e_process.processname' +
            '     , reference.personcd ' +
            '     , o_person_name.personname ' +
            '     , o_account_name.accountcd ' +
            '     , o_account_name.accountname ' +
            ' FROM ' +
            '     person_r_Task_reference reference ' +
            ' INNER JOIN ( ' +
            '     SELECT ' +
            '         personcd ' +
            '         , MAX(validstartdate) validstartdate ' +
            '     FROM ' +
            '         o_person_name ' +
            '     WHERE ' +
            '         deleteflg = FALSE ' +
            '     GROUP BY personcd ' +
            ' ) personName ' +
            '     ON personName.personcd = reference.personcd ' +
            ' INNER JOIN ' +
            '     o_person_name ' +
            '     ON o_person_name.personcd = personName.personcd ' +
            '     AND o_person_name.validstartdate = personName.validstartdate ' +
            ' LEFT JOIN ' +
            '     person_r_account_main ' +
            '     ON  person_r_account_main.personcd = reference.personcd ' +
            ' LEFT JOIN ( ' +
            '     SELECT ' +
            '         accountcd ' +
            '         , MAX(validstartdate) validstartdate ' +
            '     FROM ' +
            '         o_account_name ' +
            '     WHERE ' +
            '         o_account_name.deleteflg = FALSE ' +
            '         AND o_account_name.validstartdate <= current_timestamp ' +
            '     GROUP BY accountcd ' +
            ' ) account ' +
            ' ON account.accountcd = person_r_account_main.accountcd ' +
            ' LEFT JOIN ' +
            '     o_account_name ' +
            '     ON o_account_name.accountcd = account.accountcd ' +
            '     AND o_account_name.validstartdate = account.validstartdate ' +
            ' INNER JOIN ' +
            '     t_task ' +
            '     ON t_task.taskid = reference.taskid ' +
            '     AND t_task.applyid = $1 ' +
            ' INNER JOIN ' +
            '     e_process ' +
            '     ON e_process.processcd = t_task.processcd ' +
            ' ORDER BY t_task.orderno ASC ';
        return client.query(
            sql,
            [
                req.applyId
            ]
        );
    },

    /**
     * 所属：参照情報を取得
     * @param  {Object} req
     * @param  {Integer} req.applyId 申請ID
     * @param  {Object} [client] プール又はクライアントインスタンス
     * @return {Object} Promise
     */
    prmsGetAccountReferenceInfo: function (req, client) {
        var sql;
        client = client || pool;
        sql =
            ' SELECT ' +
            '     reference.taskid ' +
            '     , reference.accountcd ' +
            '     , o_account_name.accountname ' +
            ' FROM ' +
            '     Task_R_Account_Reference reference ' +
            ' LEFT JOIN ( ' +
            '     SELECT ' +
            '         accountcd ' +
            '         , MAX(validstartdate) validstartdate ' +
            '     FROM ' +
            '         o_account_name ' +
            '     WHERE ' +
            '         o_account_name.deleteflg = FALSE ' +
            '         AND o_account_name.validstartdate <= current_timestamp ' +
            '     GROUP BY accountcd ' +
            ' ) account ' +
            '     ON account.accountcd = reference.accountcd ' +
            ' LEFT JOIN ' +
            ' o_account_name ' +
            '     ON o_account_name.accountcd = account.accountcd' +
            '     AND o_account_name.validstartdate = account.validstartdate ' +
            ' INNER JOIN ' +
            '     t_task ' +
            '     ON t_task.taskid = reference.taskid ' +
            '     AND t_task.applyid = $1 ' +
            ' ORDER BY taskid ASC ';
        return client.query(
            sql,
            [
                req.applyId
            ]
        );
    },

    /**
     * パーソンタイプ：参照情報を取得
     * @param  {Object} req
     * @param  {Integer} req.applyId 申請ID
     * @param  {Object} [client] プール又はクライアントインスタンス
     * @return {Object} Promise
     */
    prmsGetPersonTypeReferenceInfo: function (req, client) {
        var sql;
        client = client || pool;
        sql =
            ' SELECT ' +
            '     reference.taskid ' +
            '     , reference.persontypecd ' +
            '     , c_persontype.persontypename ' +
            ' FROM ' +
            '     Task_R_PersonType_Reference reference ' +
            ' INNER JOIN ' +
            '     c_persontype ' +
            '     ON  c_persontype.persontypecd = reference.persontypecd ' +
            '     AND c_persontype.deleteflg = FALSE ' +
            '     AND c_persontype.validstartdate <= current_timestamp ' +
            ' INNER JOIN ' +
            '     t_task ' +
            '     ON t_task.taskid = reference.taskid ' +
            '     AND t_task.applyid = $1 ' +
            ' ORDER BY taskid ASC ';
        return client.query(
            sql,
            [
                req.applyId
            ]
        );
    },

    /**
     * 役職：参照情報を取得
     * @param  {Object} req
     * @param  {Integer} req.applyId 申請ID
     * @param  {Object} [client] プール又はクライアントインスタンス
     * @return {Object} Promise
     */
    prmsGetPositionReferenceInfo: function (req, client) {
        var sql;
        client = client || pool;
        sql =
            ' SELECT ' +
            '     reference.taskid ' +
            '     , reference.positioncd ' +
            '     , o_position_name.positionname ' +
            ' FROM ' +
            '     Task_R_Position_Reference reference ' +
            ' LEFT JOIN ( ' +
            '     SELECT ' +
            '         positioncd ' +
            '         , MAX(validstartdate) validstartdate ' +
            ' FROM ' +
            '     o_position_name ' +
            ' WHERE ' +
            '     o_position_name.deleteflg = FALSE ' +
            ' AND o_position_name.validstartdate <= current_timestamp ' +
            ' GROUP BY positioncd ' +
            ' ) position ' +
            '     ON position.positioncd = reference.positioncd ' +
            ' LEFT JOIN ' +
            '     o_position_name ' +
            '     ON o_position_name.positioncd = position.positioncd ' +
            '     AND o_position_name.validstartdate = position.validstartdate ' +
            ' INNER JOIN ' +
            '     t_task ' +
            '     ON t_task.taskid = reference.taskid ' +
            '     AND t_task.applyid = $1 ' +
            ' ORDER BY taskid ASC ';
        return client.query(
            sql,
            [
                req.applyId
            ]
        );
    },

    /**
     * 役割：参照情報を取得
     * @param  {Object} req
     * @param  {Integer} req.applyId 申請ID
     * @param  {Object} [client] プール又はクライアントインスタンス
     * @return {Object} Promise
     */
    prmsGetRoleReferenceInfo: function (req, client) {
        var sql;
        client = client || pool;
        sql =
            ' SELECT ' +
            '     reference.taskid ' +
            '     , reference.rolecd ' +
            '     , o_role_name.rolename ' +
            ' FROM ' +
            '     Task_R_Role_Reference reference ' +
            ' INNER JOIN ( ' +
            '     SELECT ' +
            '         rolecd ' +
            '         , MAX(validstartdate) validstartdate ' +
            '     FROM ' +
            '         o_role_name ' +
            '     WHERE ' +
            '         deleteflg =FALSE ' +
            '     AND validstartdate <= current_timestamp ' +
            ' GROUP BY rolecd ' +
            ' ) roleName ' +
            '     ON roleName.rolecd = reference.rolecd ' +
            ' INNER JOIN ' +
            '     o_role_name ' +
            '     ON o_role_name.rolecd = roleName.rolecd ' +
            '     AND o_role_name.validstartdate = roleName.validstartdate ' +
            ' INNER JOIN ' +
            '     t_task ' +
            '     ON t_task.taskid = reference.taskid ' +
            '     AND t_task.applyid = $1 ' +
            ' ORDER BY taskid ASC ';
        return client.query(
            sql,
            [
                req.applyId
            ]
        );
    },

    /**
     * タスクIDから処理者情報を取得
     * @param  {Object} req
     * @param  {Integer} req.taskId タスクID
     * @param  {Object} [client] プール又はクライアントインスタンス
     * @return {Object} Promise
     */
    prmsGetProcessorsInfoByTaskId: function (req, client) {
        var sql;
        client = client || pool;
        sql =
            'SELECT ' +
            '    personcd ' +
            'FROM ' +
            '    person_r_task_processor ' +
            'WHERE ' +
            '    taskid = $1 ';
        return client.query(
            sql,
            [
                req.taskid
            ]
        );
    },

    /**
     * タスクIDから参照者情報を取得
     * @param  {Object} req
     * @param  {Integer} req.taskId タスクID
     * @param  {Object} [client] プール又はクライアントインスタンス
     * @return {Object} Promise
     */
    prmsGetReferenceInfoByTaskId: function (req, client) {
        var sql;
        client = client || pool;
        sql =
            'SELECT ' +
            '    personcd ' +
            'FROM ' +
            '    person_r_task_reference ' +
            'WHERE ' +
            '    taskid = $1 ';
        return client.query(
            sql,
            [
                req.taskid
            ]
        );
    },

    /**
     * タスクIDから所属：処理者情報を取得
     * @param  {Object} req
     * @param  {Integer} req.taskId タスクID
     * @param  {Object} [client] プール又はクライアントインスタンス
     * @return {Object} Promise
     */
    prmsGetAccountProcessorInfoByTaskId: function (req, client) {
        var sql;
        client = client || pool;
        sql =
            'SELECT ' +
            '    accountcd ' +
            'FROM ' +
            '    Task_R_Account_Processor ' +
            'WHERE ' +
            '    taskid = $1 ';
        return client.query(
            sql,
            [
                req.taskid
            ]
        );
    },

    /**
     * タスクIDから所属：参照者情報を取得
     * @param  {Object} req
     * @param  {Integer} req.taskId タスクID
     * @param  {Object} [client] プール又はクライアントインスタンス
     * @return {Object} Promise
     */
    prmsGetAccountReferenceInfoByTaskId: function (req, client) {
        var sql;
        client = client || pool;
        sql =
            'SELECT ' +
            '    accountcd ' +
            'FROM ' +
            '    task_r_account_reference ' +
            'WHERE ' +
            '    taskid = $1 ';
        return client.query(
            sql,
            [
                req.taskid
            ]
        );
    },

    /**
     * タスクIDからパーソンタイプ：処理者情報を取得
     * @param  {Object} req
     * @param  {Integer} req.taskId タスクID
     * @param  {Object} [client] プール又はクライアントインスタンス
     * @return {Object} Promise
     */
    prmsGetPersonTypeProcessorInfoByTaskId: function (req, client) {
        var sql;
        client = client || pool;
        sql =
            'SELECT ' +
            '    persontypecd ' +
            'FROM ' +
            '    Task_R_PersonType_Processor ' +
            'WHERE ' +
            '    taskid = $1 ';
        return client.query(
            sql,
            [
                req.taskid
            ]
        );
    },

    /**
     * タスクIDからパーソンタイプ：参照者情報を取得
     * @param  {Object} req
     * @param  {Integer} req.taskId タスクID
     * @param  {Object} [client] プール又はクライアントインスタンス
     * @return {Object} Promise
     */
    prmsGetPersonTypeReferenceInfoByTaskId: function (req, client) {
        var sql;
        client = client || pool;
        sql =
            'SELECT ' +
            '    persontypecd ' +
            'FROM ' +
            '    task_r_persontype_reference ' +
            'WHERE ' +
            '    taskid = $1 ';
        return client.query(
            sql,
            [
                req.taskid
            ]
        );
    },

    /**
     * タスクIDから役職：処理者情報を取得
     * @param  {Object} req
     * @param  {Integer} req.taskId タスクID
     * @param  {Object} [client] プール又はクライアントインスタンス
     * @return {Object} Promise
     */
    prmsGetPositionProcessorInfoByTaskId: function (req, client) {
        var sql;
        client = client || pool;
        sql =
            'SELECT ' +
            '    positioncd ' +
            'FROM ' +
            '    task_r_position_processor ' +
            'WHERE ' +
            '    taskid = $1 ';
        return client.query(
            sql,
            [
                req.taskid
            ]
        );
    },

    /**
     * タスクIDから役職：参照者情報を取得
     * @param  {Object} req
     * @param  {Integer} req.taskId タスクID
     * @param  {Object} [client] プール又はクライアントインスタンス
     * @return {Object} Promise
     */
    prmsGetPositionReferenceInfoByTaskId: function (req, client) {
        var sql;
        client = client || pool;
        sql =
            'SELECT ' +
            '    positioncd ' +
            'FROM ' +
            '    task_r_position_reference ' +
            'WHERE ' +
            '    taskid = $1 ';
        return client.query(
            sql,
            [
                req.taskid
            ]
        );
    },

    /**
     * タスクIDから役割：処理者情報を取得
     * @param  {Object} req
     * @param  {Integer} req.taskId タスクID
     * @param  {Object} [client] プール又はクライアントインスタンス
     * @return {Object} Promise
     */
    prmsGetRoleProcessorInfoByTaskId: function (req, client) {
        var sql;
        client = client || pool;
        sql =
            'SELECT ' +
            '    rolecd ' +
            'FROM ' +
            '    Task_R_Role_Processor ' +
            'WHERE ' +
            '    taskid = $1 ';
        return client.query(
            sql,
            [
                req.taskid
            ]
        );
    },

    /**
     * タスクIDから役割：参照者情報を取得
     * @param  {Object} req
     * @param  {Integer} req.taskId タスクID
     * @param  {Object} [client] プール又はクライアントインスタンス
     * @return {Object} Promise
     */
    prmsGetRoleReferenceInfoByTaskId: function (req, client) {
        var sql;
        client = client || pool;
        sql =
            'SELECT ' +
            '    rolecd ' +
            'FROM ' +
            '    task_r_role_reference ' +
            'WHERE ' +
            '    taskid = $1 ';
        return client.query(
            sql,
            [
                req.taskid
            ]
        );
    },

    /**
     * プロセスCDで指定して、特定のタスクを取得する
     * @param  {Object} req
     * @param  {Integer} req.applyId 申請ID
     * @param  {Integer} req.processCd プロセスCD
     * @param  {Object} [client] プール又はクライアントインスタンス
     * @return {Object} Promise
     */
    prmsGetTaskIdByProcessCd: function (req, client) {
        var sql;
        client = client || pool;
        sql =
            'SELECT ' +
            '    taskid ' +
            'FROM ' +
            '    t_task ' +
            'WHERE ' +
            '    applyId = $1 ' +
            '    AND processcd = $2 ';
        return client.query(
            sql,
            [
                req.applyId,
                req.processCd,
            ]
        );
    },
};
