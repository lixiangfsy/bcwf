/*global Promise */
'use strict';
var logger = require('../function/logger')();

var pool = require('../dao/dao').pool();

module.exports = {
    /**
     * ファイルpath取得
     * @param  {Object} req
     * @param  {Object} req.fileId テンプファイルID
     * @param  {Object} [client] プール又はクライアントインスタンス
     * @return {Object} Promise
     */
    prmsSelectFilePathByFileId: function (req, client) {
        var sql;
        client = client || pool;
        sql =
            'SELECT filepath FROM t_attachment WHERE fileid = $1';
        return client.query(
            sql,
            [
                req.fileId
            ]
        );
    },
    /**
     * ファイルID取得
     * @param  {Object} [req]
     * @param  {Object} [client] プール又はクライアントインスタンス
     * @return {Object} Promise
     */
    prmsSelectFileIdSeq: function (req, client) {
        var sql;
        client = client || pool;
        sql =
            'SELECT nextval(\'t_attachment_fileid_seq\') AS fileid';
        return client.query(
            sql,
            []
        );
    },
    /**
     * 添付ファイルテーブル追加
     * @param  {Object} req
     * @param  {Object} req.fileId
     * @param  {Object} req.applyId
     * @param  {Object} req.personCd
     * @param  {Object} req.attachDate
     * @param  {Object} req.filePath
     * @param  {Object} req.fileName
     * @param  {Object} req.fileSize
     * @param  {Object} [client] プール又はクライアントインスタンス
     * @return {Object} Promise
     */
    prmsInsertTAttachment: function (req, client) {
        var sql;
        client = client || pool;
        sql =
            'INSERT ' +
            'INTO t_attachment( ' +
            '    fileid ' +
            '  , applyid ' +
            '  , personcd ' +
            '  , attachdate ' +
            '  , filepath ' +
            '  , filename ' +
            '  , size ' +
            ') VALUES (' +
            '    $1 ' +
            '  , $2 ' +
            '  , $3 ' +
            '  , $4 ' +
            '  , $5 ' +
            '  , $6 ' +
            '  , $7) ';
        return client.query(
            sql,
            [
                req.fileId,
                req.applyId,
                req.personCd,
                req.attachDate,
                req.filePath,
                req.fileName,
                req.fileSize
            ]
        );
    },
    /**
     * 添付ファイルテーブル追加削除
     * @param  {Object} req
     * @param  {Object} req.fileId テンプファイルID
     * @param  {Object} [client] プール又はクライアントインスタンス
     * @return {Object} Promise
     */
    prmsDeleteTAttachment: function (req, client) {
        var sql;
        client = client || pool;
        sql =
            ' DELETE ' +
            ' FROM ' +
            '     t_attachment ' +
            ' WHERE ' +
            '     fileid = $1 ';
        return client.query(
            sql,
            [
                req.fileId,
            ]
        );
    },

    /**
     * 添付ファイル情報を取得
     * @param  {Object} req
     * @param  {Integer} req.applyId 申請ID
     * @param  {Object} [client] プール又はクライアントインスタンス
     * @return {Object} Promise
     */
    prmsGetAttachmentInfo: function (req, client) {
        var sql;
        client = client || pool;
        sql =
            ' SELECT ' +
            '     t_attachment.fileid ' +
            '   , t_attachment.personcd ' +
            '   , o_person_name.personname ' +
            '   , t_attachment.attachdate ' +
            '   , t_attachment.filepath ' +
            '   , t_attachment.filename ' +
            '   , t_attachment.size ' +
            ' FROM ' +
            '     t_attachment ' +
            ' INNER JOIN ( ' +
            '     SELECT ' +
            '         personcd ' +
            '       , MAX(validstartdate) validstartdate ' +
            '     FROM ' +
            '         o_person_name ' +
            '     WHERE ' +
            '         deleteflg = FALSE ' +
            '     GROUP BY personcd ' +
            ' ) personname ' +
            '     ON ' +
            '     personname.personcd = t_attachment.personcd ' +
            ' INNER JOIN ' +
            '     o_person_name ' +
            '     ON ' +
            '         o_person_name.personcd = personname.personcd ' +
            '     AND ' +
            '         o_person_name.validstartdate = personname.validstartdate ' +
            '  ' +
            ' WHERE ' +
            '     t_attachment.applyid = $1 ' +
            ' ORDER BY t_attachment.attachDate DESC ';
        return client.query(
            sql,
            [
                req.applyId
            ]
        );
    },

    /**
     * 添付ファイル情報を取得
     * @param  {Object} req
     * @param  {Integer} req.applyId 申請ID
     * @param  {Object} [client] プール又はクライアントインスタンス
     * @return {Object} Promise
     */
    prmsGetLatestAttachmentInfo: function (req, client) {
        var sql;
        client = client || pool;
        sql =
            ' SELECT ' +
            '     t_attachment.fileid ' +
            '   , t_attachment.personcd ' +
            '   , o_person_name.personname ' +
            '   , t_attachment.attachdate ' +
            '   , t_attachment.filepath ' +
            '   , t_attachment.filename ' +
            '   , t_attachment.size ' +
            ' FROM ' +
            '     t_attachment ' +
            ' INNER JOIN ( ' +
            '     SELECT ' +
            '         personcd ' +
            '       , MAX(validstartdate) validstartdate ' +
            '     FROM ' +
            '         o_person_name ' +
            '     WHERE ' +
            '         deleteflg = FALSE ' +
            '     GROUP BY personcd ' +
            ' ) personname ' +
            '     ON ' +
            '     personname.personcd = t_attachment.personcd ' +
            ' INNER JOIN ' +
            '     o_person_name ' +
            '     ON ' +
            '         o_person_name.personcd = personname.personcd ' +
            '     AND ' +
            '         o_person_name.validstartdate = personname.validstartdate ' +
            '  ' +
            ' WHERE ' +
            '     t_attachment.applyid = $1 ' +

            ' AND ' +
            '     t_attachment.attachdate = ( ' +
            '         SELECT ' +
            '             MAX(t_attachment.attachdate) ' +
            '         FROM ' +
            '             t_attachment ' +
            '         WHERE ' +
            '             t_attachment.applyid = $1 ' +
            '     ) ' +

            ' ORDER BY t_attachment.attachDate DESC ';
        return client.query(
            sql,
            [
                req.applyId
            ]
        );
    },
};
