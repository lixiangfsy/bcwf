/*global Promise */
'use strict';
var pool = require('../dao/dao').pool();

module.exports = {
    /**
     * コメントテーブル追加
     * @param  {Object} req
     * @param  {Object} req.applyId
     * @param  {Object} req.personCd
     * @param  {Object} req.saveDate
     * @param  {Object} req.comment
     * @param  {Object} [client] プール又はクライアントインスタンス
     * @return {Object} Promise
     */
    prmsInsertTComment: function (req, client) {
        var sql;
        client = client || pool;
        sql =
            ' INSERT ' +
            ' INTO t_comment( ' +
            '     applyid ' +
            '     , personcd ' +
            '     , commentdate ' +
            '     , comment ' +
            ' ) VALUES ($1, $2, $3, $4) ';
        return client.query(
            sql,
            [
                req.applyId,
                req.personCd,
                req.saveDate,
                req.comment,
            ]
        );
    },

    // 瀋陽マージ

    /**
     * コメント情報を取得
     * @param  {Object} req
     * @param  {Integer} req.applyId 申請ID
     * @param  {Object} [client] プール又はクライアントインスタンス
     * @return {Object} Promise
     */
    prmsGetCommentInfo: function (req, client) {
        var sql;
        client = client || pool;
        sql =
            ' SELECT ' +
            '     t_comment.personcd ' +
            '   , o_person_name.personname ' +
            '   , t_comment.commentdate ' +
            '   , t_comment.comment ' +
            ' FROM ' +
            '     t_comment ' +
            ' INNER JOIN ( ' +
            '     SELECT ' +
            '         personcd ' +
            '       , MAX(validstartdate) validstartdate ' +
            '     FROM ' +
            '         o_person_name ' +
            '     WHERE ' +
            '         deleteflg = FALSE ' +
            '     GROUP BY personcd ' +
            ' ) personname ' +
            '     ON ' +
            '     personname.personcd = t_comment.personcd ' +
            ' INNER JOIN ' +
            '     o_person_name ' +
            '     ON ' +
            '         o_person_name.personcd = personname.personcd ' +
            '     AND ' +
            '         o_person_name.validstartdate = personname.validstartdate ' +
            '  ' +
            ' WHERE ' +
            '     t_comment.applyid = $1 ' +
            ' ORDER BY t_comment.commentdate DESC ';
        return client.query(
            sql,
            [
                req.applyId
            ]
        );
    },

    /**
     * 最新コメント情報を取得
     * @param  {Object} req
     * @param  {Integer} req.applyId 申請ID
     * @param  {Object} [client] プール又はクライアントインスタンス
     * @return {Object} Promise
     */
    prmsGetLatestCommentInfo: function (req, client) {
        var sql;
        client = client || pool;
        sql =
            ' SELECT ' +
            '     t_comment.personcd ' +
            '   , o_person_name.personname ' +
            '   , t_comment.commentdate ' +
            '   , t_comment.comment ' +
            ' FROM ' +
            '     t_comment ' +
            ' INNER JOIN ( ' +
            '     SELECT ' +
            '         personcd ' +
            '       , MAX(validstartdate) validstartdate ' +
            '     FROM ' +
            '         o_person_name ' +
            '     WHERE ' +
            '         deleteflg = FALSE ' +
            '     GROUP BY personcd ' +
            ' ) personname ' +
            '     ON ' +
            '     personname.personcd = t_comment.personcd ' +
            ' INNER JOIN ' +
            '     o_person_name ' +
            '     ON ' +
            '         o_person_name.personcd = personname.personcd ' +
            '     AND ' +
            '         o_person_name.validstartdate = personname.validstartdate ' +
            '  ' +
            ' WHERE ' +
            '     t_comment.applyid = $1 ' +

            ' AND ' +
            '     t_comment.commentdate = ( ' +
            '         SELECT ' +
            '             MAX(t_comment.commentdate) ' +
            '         FROM ' +
            '             t_comment ' +
            '         WHERE ' +
            '             t_comment.applyid = $1 ' +
            '     ) ' +

            ' ORDER BY t_comment.commentdate DESC ';
        return client.query(
            sql,
            [
                req.applyId
            ]
        );
    },
};
