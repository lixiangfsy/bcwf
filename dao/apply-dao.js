/*global Promise */
'use strict';
var logger = require('../function/logger')();

var pool = require('../dao/dao').pool();

module.exports = {
    /**
     * 申請テーブルを更新
     * @param  {Object} req
     * @param  {String} req.applyTitle 申請名称
     * @param  {Date} req.saveDate 保存日付
     * @param  {String} req.accountCd 所属CD
     * @param  {Integer} req.applyId 申請ID
     * @param  {Object} [client] プール又はクライアントインスタンス
     * @return {Object} Promise
     */
    prmsUpdateTApply: function (req, client) {
        var sql;
        client = client || pool;
        sql =
            ' UPDATE t_apply ' +
            ' SET applytitle = $1 ' +
            '     , applydatetime = $2 ' +
            '     , accountcd = $3 ' +
            ' WHERE' +
            '     applyid = $4 ';
        return client.query(
            sql,
            [
                req.applyTitle,
                req.saveDate,
                req.accountCd,
                req.applyId,
            ]
        );
    },

    /**
     * 申請ID取得
     * @param  {Object} req
     * @param  {Object} [client] プール又はクライアントインスタンス
     * @return {Object} Promise
     */
    prmsSelectApplyIdSeq: function (req, client) {
        var sql;
        client = client || pool;
        sql =
            ' SELECT nextval(\'t_apply_applyid_seq\') as applyid ';
        return client.query(
            sql,
            []
        );
    },

    /**
     * 申請テーブル追加
     * @param  {Object} req
     * @param  {Integer} req.applyId 申請ID
     * @param  {String} req.applyTitle 申請名称
     * @param  {Date} req.applyDatetime 申請日付
     * @param  {String} req.applyFormCd 諸届CD
     * @param  {Integer} req.startTaskId スタートタスクID
     * @param  {String} req.personCd パーソンCD
     * @param  {String} req.accountCd 所属CD
     * @param  {Object} [client] プール又はクライアントインスタンス
     * @return {Object} Promise
     */
    prmsInsertTApply: function (req, client) {
        var sql;
        client = client || pool;
        sql =
            ' INSERT ' +
            ' INTO t_apply( ' +
            '     applyid ' +
            '     , applytitle ' +
            '     , applydatetime ' +
            '     , applyformcd ' +
            '     , starttaskid ' +
            '     , personcd ' +
            '     , accountcd ' +
            ' ) VALUES ($1, $2, $3, $4, $5, $6, $7) ';
        return client.query(
            sql,
            [
                req.applyId,
                req.applyTitle,
                req.applyDatetime,
                req.applyFormCd,
                req.startTaskId,
                req.personCd,
                req.accountCd,
            ]
        );
    },

    // TODO テンプレートテーブルを取得する 利用しないので、後で削除します
    prmsSelectOTemplate: function (prmsSelectOTemplateReq, client) {
        var sql;
        client = client || pool;
        sql =
            'SELECT ' +
            '    templateid ' +
            '    , validstartdate ' +
            '    , filepath ' +
            '    , filename ' +
            'FROM ' +
            '    o_template ' +
            'WHERE ' +
            '    applyformcd = $1 ' +
            '    AND validstartdate <= $2 ';
        return client.query(
            sql,
            [
                prmsSelectOTemplateReq.applyFormCd,
                prmsSelectOTemplateReq.validStartDate
            ]
        );
    },

    // 添付ファイルテーブル追加
    prmsInsertTAttachment: function (prmsInsertTAttachmentReqReq, client) {
        var sql;
        client = client || pool;
        sql =
            'INSERT ' +
            'INTO t_attachment( ' +
            '    fileid ' +
            '    , applyid ' +
            '    , personcd ' +
            '    , attachdate ' +
            '    , filepath ' +
            '    , filename ' +
            '    , filesize ' +
            ') VALUES (' +
            '    $1 ' +
            '    , $2 ' +
            '    , $3 ' +
            '    , $4 ' +
            '    , $5 ' +
            '    , $6 ' +
            '    , $7) ';
        return client.query(
            sql,
            [
                prmsInsertTAttachmentReqReq.fileId,
                prmsInsertTAttachmentReqReq.applyId,
                prmsInsertTAttachmentReqReq.personCd,
                prmsInsertTAttachmentReqReq.attachDate,
                prmsInsertTAttachmentReqReq.filePath,
                prmsInsertTAttachmentReqReq.fileName,
                prmsInsertTAttachmentReqReq.fileSize
            ]
        );
    },

    /**
     * 申請基本情報を取得
     * @param  {Object} req
     * @param  {Integer} req.applyId 申請ID
     * @param  {Object} [client] プール又はクライアントインスタンス
     * @return {Object} Promise
     */
    prmsGetApplyInfo: function (req, client) {
        var sql;
        client = client || pool;
        sql =
            ' SELECT ' +
            '     t_apply.applyid ' +
            '     , t_apply.applytitle ' +
            '     , t_apply.applydatetime ' +
            '     , t_apply.applyformcd ' +
            '     , e_applyform.applyformname ' +
            '     , t_apply.starttaskid ' +
            '     , t_apply.personcd ' +
            '     , o_person_name.personname ' +
            '     , t_apply.accountcd ' +
            '     , o_account_name.accountname ' +
            ' FROM ' +
            '     t_apply ' +
            ' INNER JOIN ' +
            '     e_applyform ' +
            '     ON ' +
            '         e_applyform.applyformcd = t_apply.applyformcd ' +
            '     AND ' +
            '         e_applyform.validstartdate <= current_timestamp ' +
            '     AND ' +
            '         e_applyform.validenddate >= current_timestamp ' +
            ' INNER JOIN ( ' +
            '     SELECT ' +
            '         personcd ' +
            '       , MAX(validstartdate) validstartdate ' +
            '     FROM ' +
            '         o_person_name ' +
            '     WHERE ' +
            '         deleteflg = FALSE ' +
            '     GROUP BY personcd ' +
            ' ) personName ' +
            '     ON ' +
            '     personname.personcd = t_apply.personcd ' +
            ' INNER JOIN ' +
            '     o_person_name ' +
            '     ON ' +
            '         o_person_name.personcd = personname.personcd ' +
            '     AND ' +
            '         o_person_name.validstartdate = personname.validstartdate ' +
            ' INNER JOIN ( ' +
            '     SELECT ' +
            '         accountcd ' +
            '       , MAX(validstartdate) validstartdate  ' +
            '     FROM ' +
            '         o_account_name ' +
            '     WHERE ' +
            '         deleteflg = FALSE ' +
            '     GROUP BY accountcd ' +
            ' ) name ' +
            '     ON  name.accountcd = t_apply.accountcd ' +
            ' INNER JOIN ' +
            '     o_account_name ' +
            '     ON ' +
            '         o_account_name.accountcd = name.accountcd ' +
            '     AND ' +
            '         o_account_name.validstartdate = name.validstartdate ' +
            ' WHERE ' +
            '     t_apply.applyid = $1 ';
        return client.query(
            sql,
            [
                req.applyId
            ]
        );
    },

    /**
     * 自分が申請した申請書を取得（代理申請含む）
     * @param  {Object} req
     * @param  {String} req.personCd パーソンCD
     * @param  {Object} [client] プール又はクライアントインスタンス
     * @return {Object} Promise
     */
    prmsSelectActiveApplyList: function (req, client) {
        return new Promise(function (resolve, reject) {
            var sql;
            client = client || pool;
            sql =
                ' SELECT ' +
                '     apply.applyid ' +
                '     , apply.applytitle ' +
                '     , apply.applydatetime ' +
                '     , process.processname ' +
                ' FROM ' +
                '     t_apply apply ' +
                ' INNER JOIN ' +
                '     t_task task ' +
                ' ON ' +
                '     task.applyid = apply.applyid ' +
                '     AND task.activestatus = 1 ' +
                ' INNER JOIN ' +
                '     e_process process ' +
                '     ON process.processcd = task.processcd ' +
                ' WHERE ' +
                '     apply.personcd = $1 ' +
                ' ORDER BY' +
                '     apply.applydatetime DESC ' +
                '     , apply.applyid DESC ' +
                '     , process.processcd ';
            client.query(
                sql,
                [
                    req.personCd
                ]
            )
                .then(function (queryRes) {
                    var i,
                        prmsSelectActiveApplyListRes,
                        processname,
                        rowData;
                    if (queryRes.rowCount === 0) {
                        resolve(queryRes);
                    } else {
                        // 同じ申請番号のプロセス名をカンマ区切りの１つの文字列となるように加工する
                        // この結果を返すクエリを作ると重いクエリとなることが想定されるため無理をせずプログラムで実装する
                        // そのため、この部分はビジネスロジックとはみなさず、DAO内に実装する
                        prmsSelectActiveApplyListRes = {
                            rows: [],
                        };
                        rowData = {
                            applyid: queryRes.rows[0].applyid,
                            applytitle: queryRes.rows[0].applytitle,
                            applydatetime: queryRes.rows[0].applydatetime,
                        };
                        processname = [queryRes.rows[0].processname];
                        for (i = 1; i < queryRes.rowCount; i += 1) {
                            if (rowData.applyid !== queryRes.rows[i].applyid) {
                                rowData.processname = processname.join(',');
                                prmsSelectActiveApplyListRes.rows.push(rowData);
                                rowData = {
                                    applyid: queryRes.rows[i].applyid,
                                    applytitle: queryRes.rows[i].applytitle,
                                    applydatetime: queryRes.rows[i].applydatetime,
                                };
                                processname = [queryRes.rows[i].processname];
                            } else {
                                processname.push(queryRes.rows[i].processname);
                            }
                        }
                        rowData.processname = processname.join(',');
                        prmsSelectActiveApplyListRes.rows.push(rowData);

                        prmsSelectActiveApplyListRes.rowCount = prmsSelectActiveApplyListRes.rows.length;

                        resolve(prmsSelectActiveApplyListRes);
                    }
                })
                .catch(function (err) {
                    if (err !== undefined) {
                        logger.error(err);
                    }
                    reject();
                });
        });
    },

    /**
     * 承認一覧を取得
     * @param  {Object} req
     * @param  {Object} [client] プール又はクライアントインスタンス
     * @return {Object} Promise
     */
    prmsGetApproveList: function (prmsGetApproveListReq, client) {
        return new Promise(function (resolve, reject) {
            var sql;
            client = client || pool;
            sql =
                ' SELECT ' +
                '     apply.applyid ' +
                '     , apply.applytitle ' +
                '     , apply.applydatetime ' +
                '     , apply.personcd ' +
                '     , o_person_name.personname ' +
                '     , task.taskid ' +
                '     , task.activestatus ' +
                '     , process.actionname ' +
                ' FROM ' +
                '     t_apply apply ' +
                ' INNER JOIN ' +
                '     t_task task ' +
                '     ON task.applyid = apply.applyid ' +
                '     AND task.activestatus in (0, 1) ' +
                ' INNER JOIN ( ' +
                '     SELECT ' +
                '         personcd ' +
                '         , MAX(validstartdate) validstartdate ' +
                '     FROM ' +
                '         o_person_name ' +
                '     WHERE ' +
                '         deleteflg = FALSE ' +
                '     GROUP BY personcd ' +
                ' ) personName ' +
                '     ON personName.personcd = apply.personcd ' +
                ' INNER JOIN ' +
                '     o_person_name ' +
                '     ON o_person_name.personcd = personName.personcd ' +
                '     AND o_person_name.validstartdate = personName.validstartdate ' +
                ' LEFT JOIN ' +
                '     person_r_task_processor ' +
                '     ON person_r_task_processor.taskid = task.taskid ' +
                ' LEFT JOIN ' +
                '     task_r_account_processor ' +
                '     ON task_r_account_processor.taskid = task.taskid ' +
                ' LEFT JOIN ' +
                '     task_r_position_processor ' +
                '     ON task_r_position_processor.taskid = task.taskid ' +
                ' LEFT JOIN ' +
                '     task_r_persontype_processor ' +
                '     ON task_r_persontype_processor.taskid = task.taskid ' +
                ' INNER JOIN ' +
                '     t_task ' +
                '     ON t_task.taskid = apply.starttaskid ' +
                '     AND t_task.activestatus = 2 ' +
                ' LEFT JOIN ' +
                '     o_process_action process ' +
                '     ON process.processcd = task.processcd ' +
                ' WHERE ' +
                '     person_r_task_processor.personcd = $1 ' +
                '     OR  task_r_account_processor.accountcd = $2 ' +
                '     OR  task_r_position_processor.positioncd = $3 ' +
                '     OR  task_r_persontype_processor.persontypecd = $4 ' +
                ' ORDER BY ' +
                '     apply.applydatetime DESC ' +
                '     , apply.applyid DESC ' +
                '     , task.taskid ASC ';
            client.query(
                sql,
                [
                    prmsGetApproveListReq.personCd,
                    prmsGetApproveListReq.accountCd,
                    prmsGetApproveListReq.positionCd,
                    prmsGetApproveListReq.personTypeCd,
                ]
            )
                .then(function (queryRes) {
                    var actionname,
                        i,
                        prmsGetApproveListRes,
                        taskid,
                        rowData;
                    if (queryRes.rowCount === 0) {
                        resolve(queryRes);
                    } else {
                        // 同じ申請番号のプロセス名をカンマ区切りの１つの文字列となるように加工する
                        // この結果を返すクエリを作ると重いクエリとなることが想定されるため無理をせずプログラムで実装する
                        // そのため、この部分はビジネスロジックとはみなさず、DAO内に実装する
                        prmsGetApproveListRes = {
                            rows: [],
                        };
                        rowData = {
                            applyid: queryRes.rows[0].applyid,
                            applytitle: queryRes.rows[0].applytitle,
                            applydatetime: queryRes.rows[0].applydatetime,
                            personcd: queryRes.rows[0].personcd,
                            personname: queryRes.rows[0].personname,
                        };
                        taskid = [queryRes.rows[0].taskid];
                        actionname = [queryRes.rows[0].actionname];
                        for (i = 1; i < queryRes.rowCount; i += 1) {
                            if (rowData.applyid !== queryRes.rows[i].applyid) {
                                rowData.taskid = taskid.join(',');
                                rowData.actionname = actionname.join(',');
                                prmsGetApproveListRes.rows.push(rowData);
                                rowData = {
                                    applyid: queryRes.rows[i].applyid,
                                    applytitle: queryRes.rows[i].applytitle,
                                    applydatetime: queryRes.rows[i].applydatetime,
                                    personcd: queryRes.rows[i].personcd,
                                    personname: queryRes.rows[i].personname,
                                };
                                taskid = [queryRes.rows[i].taskid];
                                actionname = [queryRes.rows[i].actionname];
                            } else {
                                taskid.push(queryRes.rows[i].taskid);
                                actionname.push(queryRes.rows[i].actionname);
                            }
                        }
                        rowData.taskid = taskid.join(',');
                        prmsGetApproveListRes.rows.push(rowData);

                        prmsGetApproveListRes.rowCount = prmsGetApproveListRes.rows.length;

                        resolve(prmsGetApproveListRes);
                    }
                })
                .catch(function (err) {
                    if (err !== undefined) {
                        logger.error(err);
                    }
                    reject();
                });

        });
    },

    /**
     * 自分が申請した申請書の件数を取得（代理申請含む）
     * @param  {Object} req
     * @param  {String} req.personCd パーソンCD
     * @param  {Object} [client] プール又はクライアントインスタンス
     * @return {Object} Promise
     */
    prmsSelectActiveApplyCount: function (req, client) {
        var sql;
        client = client || pool;
        sql =
            'SELECT ' +
            '    COUNT(1) ' +
            'FROM ' +
            '    t_apply apply ' +
            'INNER JOIN ' +
            '    t_task task ' +
            '    ON task.applyid = apply.applyid ' +
            'WHERE ' +
            '    apply.personcd = $1 ' +
            '    AND task.activestatus = 1 ';
        return client.query(
            sql,
            [
                req.personCd
            ]
        );
    },

    /**
     * 承認一覧の件数を取得
     * @param  {Object} req
     * @param  {Object} [client] プール又はクライアントインスタンス
     * @return {Object} Promise
     */
    prmsGetApproveCount: function (req, client) {
        var sql;
        client = client || pool;
        sql =
            'SELECT ' +
            '    COUNT(1) ' +
            'FROM ' +
            '    t_apply apply ' +
            'INNER JOIN ' +
            '    t_task task ' +
            '    ON task.applyid = apply.applyid ' +
            '    AND task.activestatus = 1 ' +
            'LEFT JOIN ' +
            '    person_r_task_processor ' +
            '    ON person_r_task_processor.taskid = task.taskid ' +
            'WHERE ' +
            '    person_r_task_processor.personcd = $1';
        return client.query(
            sql,
            [
                req.personCd
            ]
        );
    },

    /**
     * 閲覧一覧の件数を取得
     * @param  {Object} req
     * @param  {Object} [client] プール又はクライアントインスタンス
     * @return {Object} Promise
     */
    prmsGetBrowseCount: function (req, client) {
        var sql;
        client = client || pool;
        sql =
            'SELECT ' +
            '    COUNT(1) ' +
            'FROM ' +
            '    t_apply apply ' +
            'INNER JOIN ' +
            '    t_task task ' +
            '    ON task.applyid = apply.applyid ' +
            '    AND task.activestatus = 1 ' +
            'LEFT JOIN ' +
            '    person_r_task_reference ' +
            '    ON person_r_task_reference.taskid = task.taskid ' +
            'WHERE ' +
            '    person_r_task_reference.personcd = $1';
        return client.query(
            sql,
            [
                req.personCd
            ]
        );
    },

    /**
     * 自分が申請した申請書を取得（代理申請含む）
     * @param  {Object} req
     * @param  {String} req.personCd パーソンCD
     * @param  {Object} [client] プール又はクライアントインスタンス
     * @return {Object} Promise
     */
    prmsSelectActiveApplyListByCondition: function (req, client) {
        return new Promise(function (resolve, reject) {
            var condition,
                paramArray,
                paramNum,
                sql;
            paramArray = [req.session.personcd];
            paramNum = 1;
            client = client || pool;
            condition = '';
            if (req.body.processname !== undefined &&  req.body.processname !== '') {
                paramNum += 1;
                condition = ' AND process.processname = $' + paramNum + ' ';
                paramArray.push(req.body.processname);
            }
            if (req.body.applytitle !== undefined &&  req.body.applytitle !== '') {
                paramNum += 1;
                condition += ' AND apply.applytitle like $' + paramNum + ' ';
                paramArray.push("%" + req.body.applytitle + "%");
            }
            if (req.body.dateStart !== undefined && req.body.dateStart !== '') {
                paramNum += 1;
                condition += ' AND apply.applydatetime >= $' + paramNum + ' ';
                paramArray.push(req.body.dateStart);
            }
            if (req.body.dateEnd !== undefined && req.body.dateEnd !== '') {
                paramNum += 1;
                condition += ' AND apply.applydatetime <= $' + paramNum + ' ';
                paramArray.push(req.body.dateEnd);
            }
            sql =
                ' SELECT ' +
                '     apply.applyid ' +
                '     , apply.applytitle ' +
                '     , apply.applydatetime ' +
                '     , process.processname ' +
                ' FROM ' +
                '     t_apply apply ' +
                ' INNER JOIN ' +
                '     t_task task ' +
                ' ON ' +
                '     task.applyid = apply.applyid ' +
                '     AND task.activestatus = 1 ' +
                ' INNER JOIN ' +
                '     e_process process ' +
                '     ON process.processcd = task.processcd ' +
                ' WHERE ' +
                '     apply.personcd = $1 ' +
                      condition +
                ' ORDER BY' +
                '     apply.applydatetime DESC ' +
                '     , apply.applyid DESC ' +
                '     , process.processcd ';
            client.query(
                sql,
                paramArray
            )
                .then(function (queryRes) {
                    var i,
                        prmsSelectActiveApplyListRes,
                        processname,
                        rowData;
                    if (queryRes.rowCount === 0) {
                        resolve(queryRes);
                    } else {
                        // 同じ申請番号のプロセス名をカンマ区切りの１つの文字列となるように加工する
                        // この結果を返すクエリを作ると重いクエリとなることが想定されるため無理をせずプログラムで実装する
                        // そのため、この部分はビジネスロジックとはみなさず、DAO内に実装する
                        prmsSelectActiveApplyListRes = {
                            rows: [],
                        };
                        rowData = {
                            applyid: queryRes.rows[0].applyid,
                            applytitle: queryRes.rows[0].applytitle,
                            applydatetime: queryRes.rows[0].applydatetime,
                        };
                        processname = [queryRes.rows[0].processname];
                        for (i = 1; i < queryRes.rowCount; i += 1) {
                            if (rowData.applyid !== queryRes.rows[i].applyid) {
                                rowData.processname = processname.join(',');
                                prmsSelectActiveApplyListRes.rows.push(rowData);
                                rowData = {
                                    applyid: queryRes.rows[i].applyid,
                                    applytitle: queryRes.rows[i].applytitle,
                                    applydatetime: queryRes.rows[i].applydatetime,
                                };
                                processname = [queryRes.rows[i].processname];
                            } else {
                                processname.push(queryRes.rows[i].processname);
                            }
                        }
                        rowData.processname = processname.join(',');
                        prmsSelectActiveApplyListRes.rows.push(rowData);

                        prmsSelectActiveApplyListRes.rowCount = prmsSelectActiveApplyListRes.rows.length;

                        resolve(prmsSelectActiveApplyListRes);
                    }
                })
                .catch(function (err) {
                    if (err !== undefined) {
                        logger.error(err);
                    }
                    reject();
                });
        });
    }
};
