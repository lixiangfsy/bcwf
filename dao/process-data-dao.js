/*global Promise */
'use strict';
var pool = require('../dao/dao').pool();

module.exports = {
    /**
     * プロセス引数を取得
     * @param  {Object} prmsSelectOProcessDataByTaskIdReq
     * @param  {String} prmsSelectOProcessDataByTaskIdReq.taskId タスクID
     * @param  {Object} [client] プール又はクライアントインスタンス
     * @return {Object} Promise
     */
    prmsSelectOProcessDataByTaskId: function (prmsSelectOProcessDataByTaskIdReq, client) {
        var sql;
        client = client || pool;
        sql =
            ' SELECT ' +
            '     O_Process_Data.ProcessData ' +
            ' FROM O_Process_Data ' +
            ' INNER JOIN T_Task ' +
            ' ON ' +
            '     T_Task.ProcessCd = O_Process_Data.ProcessCd ' +
            ' WHERE ' +
            '     T_Task.TaskId = $1 ';
        return client.query(
            sql,
            [
                prmsSelectOProcessDataByTaskIdReq.taskId,
            ]
        );
    },
};
