/*global Promise */
'use strict';
var logger = require('../function/logger')();
var proceed = require('../function/proceed');

module.exports = function (req) {
    return new Promise(function (resolve) {
        var objApplyInfo;

        proceed(req.body)
            .then(function (proceedRes) {
                objApplyInfo = proceedRes;
                logger.info('proceeded successfully');
                resolve({
                    resultCode: 0,
                    nextTasks: objApplyInfo.nextTasks.rows,
                });
            })
            .catch(function (error) {
                if (error !== undefined) {
                    logger.error(error);
                }
                // 失敗しててもrejectはしない
                logger.info('proceeding is rejected');
                resolve({
                    resultCode: -1,
                });
            });
    });
};
