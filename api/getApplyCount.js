/*global Promise */
'use strict';
var logger = require('../function/logger')();
var getApplyCount = require('../function/getApplyCount');

module.exports = function (req) {
    var activeApplyCount,
        approveCount,
        browseCount;
    return new Promise(function (resolve) {
        getApplyCount.getActiveApplyCount(req)
            .then(function (result) {
                activeApplyCount = result.rows[0];
                getApplyCount.getApproveCount(req)
                    .then(function (result) {
                        approveCount = result.rows[0];
                        getApplyCount.getBrowseCount(req)
                            .then(function (result) {
                                browseCount = result.rows[0];
                            })
                            .then(function () {
                                resolve({
                                    activeApplyCount: activeApplyCount,
                                    approveCount: approveCount,
                                    browseCount: browseCount
                                });
                            })
                            .catch(function (error) {
                                if (error !== undefined) {
                                    logger.error(error);
                                }
                            });
                    })
                    .catch(function (error) {
                        if (error !== undefined) {
                            logger.error(error);
                        }
                    });
            })
            .catch(function (error) {
                if (error !== undefined) {
                    logger.error(error);
                }
            });
    });
};
