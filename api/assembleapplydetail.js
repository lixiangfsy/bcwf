/*global Promise */
'use strict';
var logger = require('../function/logger')();
var assembleApplydetail = require('../function/assemble-applydetail');

module.exports = function (req) {
    return new Promise(function (resolve) {
        assembleApplydetail(req.body)
            .then(function (res) {
                resolve({
                    resultCode: 0,
                    result: res,
                });
            })
            .catch(function (error) {
                if (error !== undefined) {
                    logger.error(error);
                }
                // 失敗しててもrejectはしない
                logger.info('assemble failed');
                resolve({
                    resultCode: -1,
                });
            });
    });
};
