/*global Promise */
'use strict';
var logger = require('../function/logger')();
var mapPersoncd = require('../function/map-personcd');

var applyDao = require('../dao/apply-dao');

module.exports = function (req) {
    return new Promise(function (resolve) {
        mapPersoncd(req.body)
            .then(function (mapPersoncdRes) {
                return applyDao.prmsGetApproveList({
                    personCd: mapPersoncdRes.personCd,
                    accountCd: req.body.accountCd,
                    positionCd: req.body.positionCd,
                    personTypeCd: req.body.personTypeCd,
                });
            })
            .then(function (prmsGetApproveListRes) {
                resolve(prmsGetApproveListRes);
            })
            .catch(function (error) {
                if (error !== undefined) {
                    logger.error(error);
                }
                // 失敗しててもrejectはしない
                logger.info('cant make an approveList');
                resolve({
                    resultCode: -1,
                });
            });
    });
};
