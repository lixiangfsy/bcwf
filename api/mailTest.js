/*global Promise */
'use strict';
var logger = require('../function/logger')();
var searchApply = require('../task/send-mail-task');

module.exports = function (req) {
    return new Promise(function (resolve) {
        searchApply(req.body)
            .then(function (data) {
                resolve(data);
            })
            .catch(function (error) {
                if (error !== undefined) {
                    logger.error(error);
                }
            });
    });
};
