/*global Promise */
'use strict';
var logger = require('../function/logger')();
var saveApply = require('../function/save-apply');

module.exports = function (req) {
    return new Promise(function (resolve) {
        saveApply(req)
            .then(function () {
                logger.info('save success');
                resolve({
                    resultCode: 0,
                });
            })
            .catch(function (error) {
                if (error !== undefined) {
                    logger.error(error);
                }
                // 失敗しててもrejectはしない
                logger.info('save failed');
                resolve({
                    resultCode: -1,
                });
            });
    });
};
