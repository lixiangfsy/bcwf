/*global Promise */
'use strict';

module.exports = function (req) {
    return new Promise(function (resolve) {
        /* ここでいろいろな処理 */
        resolve({
            "abc": req.body.a,
            "def": 'b',
            "ghi": 'c'
        });
    });
};
