/*global Promise */
'use strict';
var logger = require('../function/logger')();
var makeApply = require('../function/make-apply');

module.exports = function (req) {
    return new Promise(function (resolve) {
        makeApply(req.body)
            .then(function (res) {
                logger.info('made the apply successfully');
                resolve({
                    resultCode: 0,
                    applyId: res.applyId,
                });
            })
            .catch(function (error) {
                if (error !== undefined) {
                    logger.error(error);
                }
                // 失敗しててもrejectはしない
                logger.info('make apply failed');
                resolve({
                    resultCode: -1,
                });
            });
    });
};
