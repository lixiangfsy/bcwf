/*global Promise */
'use strict';
var logger = require('../function/logger')();
var login = require('../function/login');
var session = require('express-session');
var personinfo = require('../dao/personinfo-dao');

module.exports = function (req) {
    var account,
        topContent,
        username;
    return new Promise(function (resolve) {
        login.userInfo(req.body)
            .then(function (query) {
                if (query.rowCount > 0) {
                    req.session.personcd = query.rows[0].personcd;
                    if (req.body.pageid === undefined || req.body.pageid === '') {
                        req.body.pageid = '1';
                    }
                    login.prmsGetTop(req.body)
                        .then(function (topContentData) {
                            topContent = topContentData;
                        })
                        .then(function () {
                            return personinfo.prmsGetAccount({
                                personCd: query.rows[0].personcd,
                                baseDate: new Date()
                            });
                        })
                        .then(function (result) {
                            account = result.rows[result.rowCount - 1].accountname;
                            req.session.account = account;
                            return personinfo.prmsGetPersonName({
                                personCd: query.rows[0].personcd
                            });
                        })
                        .then(function (result) {
                            username = result.rows[result.rowCount - 1].personname;
                            req.session.username = username;
                            resolve({
                                resultCode: 0,
                                topContent: topContent,
                                account: account,
                                username: username
                            });
                        })
                        .catch(function (error) {
                            if (error !== undefined) {
                                logger.error(error);
                            }
                        });
                } else {
                    resolve({
                        resultCode: -1
                    });
                }
            })
            .catch(function (error) {
                if (error !== undefined) {
                    logger.error(error);
                }
            });
    });
};
