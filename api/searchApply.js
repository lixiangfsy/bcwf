/*global Promise */
'use strict';
var logger = require('../function/logger')();
var searchApply = require('../function/search-apply');

module.exports = function (req) {
    return new Promise(function (resolve) {
        searchApply.selectActiveApplyListByCondition(req)
            .then(function (data) {
                resolve(data);
            })
            .catch(function (error) {
                if (error !== undefined) {
                    logger.error(error);
                }
            });
    });
};
