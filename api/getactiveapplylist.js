/*global Promise */
'use strict';
var logger = require('../function/logger')();
var mapPersoncd = require('../function/map-personcd');

var applyDao = require('../dao/apply-dao');

module.exports = function (req) {
    return new Promise(function (resolve) {
        mapPersoncd(req.body)
            .then(function (mapPersoncdRes) {
                return applyDao.prmsSelectActiveApplyList({
                    personCd: mapPersoncdRes.personCd
                });
            })
            .then(function (prmsSelectActiveApplyListRes) {
                resolve(prmsSelectActiveApplyListRes);
            })
            .catch(function (error) {
                if (error !== undefined) {
                    logger.error(error);
                }
                // 失敗しててもrejectはしない
                logger.info('cant make an applyList');
                resolve({
                    resultCode: -1,
                });
            });
    });
};
