/*global module:true */
(function () {
    'use strict';
    var app,
        cookieParser,
        createError,
        express,
        log4js,
        logger,
        MongoStore,
        path,
        passport,
        session;

    createError = require('http-errors');
    express = require('express');
    path = require('path');
    log4js = require('log4js');
    log4js.configure('./config/log4js.config.json');
    logger = log4js.getLogger('express');
    passport = require('passport');
    session = require('express-session');
    MongoStore = require('connect-mongo')(session);

    app = express();
    require('./public/js/passport.js');

    // view engine setup
    app.set('views', path.join(__dirname, 'views'));
    app.set('view engine', 'ejs');

    // logger settings
    app.use(log4js.connectLogger(logger));
    app.use(express.json());
    app.use(express.urlencoded({extended: false}));
    app.use(express.static(path.join(__dirname, './public')));
    app.use(session({
        secret: 'secret',
        store: new MongoStore({
            url: 'mongodb://localhost/session_user',
            db: 'session_user', // データベース名
            host: 'localhost', // データベースのアドレス
            clear_interval: 60 * 60 // 保存期間(sec)
        }),
        cookie: {
            httpOnly: true, // cookieへのアクセスをHTTPのみに制限
            maxAge: 60 * 60 * 1000 // クッキーの有効期限(msec)
        }
    }));
    app.use(passport.initialize());
    app.use(passport.session());
    // cookie parser
    cookieParser = require('cookie-parser');
    app.use(cookieParser());

    // routes
    app.use('/api', require('./routes/api'));
    app.use('/applydetail', require('./routes/applydetail'));
    app.use('/login', require('./routes/login'));

    app.get('/adsfLogin',passport.authenticate('saml', { failureRedirect: '/', failureFlash: true }));
    app.use('/adfsResponse', require('./routes/adfslogin'));

    // catch 404 and forward to error handler
    app.use(function (req, res, next) {
        next(createError(404));
    });

    module.exports = app;
}());
