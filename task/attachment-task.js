/*global Promise */
'use strict';
var logger = require('../function/logger')();

var duplicateFile = require('../function/duplicate-file');

var pool = require('../dao/dao').pool();
var processDataDao = require('../dao/process-data-dao');

/**
 * プロセスデータテーブルから添付ファイルを操作(アップロード、登録など)
 * @param  {Object} req
 * @param  {Object} [client] プール又はクライアントインスタンス
 * @return {Object} Promise
 */
module.exports = function (req, client) {
    return new Promise(function (resolve, reject) {
        client = client || pool;
        processDataDao.prmsSelectOProcessDataByTaskId(req, client)
            .then(function (prmsSelectOProcessDataByTaskIdRes) {
                var now;
                now = new Date();
                if (prmsSelectOProcessDataByTaskIdRes !== undefined && prmsSelectOProcessDataByTaskIdRes.rowCount > 0) {
                    return duplicateFile({
                        applyId: req.applyId,
                        personCd: 'pg0001',
                        saveDate: now.getFullYear() + '/' + ('0' + (now.getMonth() + 1)).slice(-2) + '/' + ('0' + now.getDate()).slice(-2) + ' ' + ('0' + now.getHours()).slice(-2) + ':' + ('0' + now.getMinutes()).slice(-2),
                        originFileInfo: JSON.parse(prmsSelectOProcessDataByTaskIdRes.rows[0].processdata).fileInfo,
                    }, client);
                }
            })
            .then(function () {
                logger.info('attachmentTask finished successfully');
                resolve();
            })
            .catch(function (error) {
                if (error !== undefined) {
                    logger.error(error);
                }
                reject();
            });
    });
};
