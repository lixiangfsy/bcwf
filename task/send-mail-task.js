/*global Promise */
'use strict';
var moment = require('moment');
var nodemailer = require('nodemailer');

var appConfig = require('../config/app.config.json');

var assembleProcessorinfo = require('../function/assemble-processorinfo');
var logger = require('../function/logger')();

var actionDao = require('../dao/action-dao');
var applyDao = require('../dao/apply-dao');
var attachmentDao = require('../dao/attachment-dao');
var commentDao = require('../dao/comment-dao');
var personinfoDao = require('../dao/personinfo-dao');
var processDao = require('../dao/process-dao');
var processDataDao = require('../dao/process-data-dao');
var sendMailDao = require('../dao/send-mail-dao');
var taskDao = require('../dao/task-dao');

/**
 * 予約語:関数リスト
 */
var prmsReplaceFunctions = {
    // メール送信タスクの前のアクション名
    'ACTION': function (req) {
        return new Promise(function (resolve, reject) {
            taskDao.prmsGetTaskInfo(req)
                .then(function (prmsGetTaskInfoRes) {
                    var j;
                    logger.info(prmsGetTaskInfoRes);
                    for (j = 0; j < prmsGetTaskInfoRes.rowCount; j += 1) {
                        if (Number(req.taskId) === prmsGetTaskInfoRes.rows[j].taskid) {
                            resolve(prmsGetTaskInfoRes.rows[j].actionname);
                        }
                    }
                    resolve('');
                })
                .catch(function (error) {
                    if (error !== undefined) {
                        logger.error(error);
                    }
                    reject(error);
                });
        });
    },

    // TODO メール送信タスクの前のアクションの実行日時（？proceed.js送信タスクを実行する後でアクション履歴を登録します？）
    'ACTION_TIME': function (req) {
        return new Promise(function (resolve, reject) {
            actionDao.prmsGetActionHistoryInfo(req)
                .then(function (prmsGetActionHistoryInfoRes) {
                    logger.info(prmsGetActionHistoryInfoRes);
                    var j;
                    for (j = 0; j < prmsGetActionHistoryInfoRes.rowCount; j += 1) {
                        if (Number(req.taskId) === prmsGetActionHistoryInfoRes.rows[j].taskid) {
                            resolve(moment(prmsGetActionHistoryInfoRes.rows[j].actiondate).format('YYYY/MM/DD hh:mm'));
                        }
                    }
                    resolve('');
                })
                .catch(function (error) {
                    if (error !== undefined) {
                        logger.error(error);
                    }
                    reject(error);
                });
        });
    },

    // 次のタスクのアクション名
    'ACTION_NEXT': function (req) {
        return new Promise(function (resolve, reject) {
            var getActionName,
                i,
                promises;
            getActionName = function (taskId) {
                return new Promise(function (resolve) {
                    taskDao.prmsGetTaskInfo(req)
                        .then(function (prmsGetTaskInfoRes) {
                            var j;
                            for (j = 0; j < prmsGetTaskInfoRes.rowCount; j += 1) {
                                if (Number(taskId) === prmsGetTaskInfoRes.rows[j].taskid) {
                                    resolve(prmsGetTaskInfoRes.rows[j].actionname);
                                }
                            }
                            resolve('');
                        });
                });
            };
            promises = [];
            for (i = 0; i < req.nextTasks.rowCount; i += 1) {
                promises.push(getActionName(req.nextTasks.rows[i].taskid));
            }
            Promise.all(promises)
                .then(function (res) {
                    logger.info(res);
                    resolve(res);
                })
                .catch(function (error) {
                    if (error !== undefined) {
                        logger.error(error);
                    }
                    reject(error);
                });
        });
    },

    // 特定のタスク（プロセスCDで指定）のアクション名
    'ACTION_PROCESSCD': function (req) {
        return new Promise(function (resolve, reject) {
            var taskId;
            taskDao.prmsGetTaskIdByProcessCd({
                applyId: req.applyId,
                processCd: req.processCd
            })
                .then(function (prmsGetTaskIdByProcessCdRes) {
                    taskId = prmsGetTaskIdByProcessCdRes.rows[0].taskid;
                    return taskDao.prmsGetTaskInfo(req);
                })
                .then(function (prmsGetTaskInfoRes) {
                    var j;
                    logger.info(prmsGetTaskInfoRes);
                    for (j = 0; j < prmsGetTaskInfoRes.rowCount; j += 1) {
                        if (Number(taskId) === prmsGetTaskInfoRes.rows[j].taskid) {
                            resolve(prmsGetTaskInfoRes.rows[j].actionname);
                        }
                    }
                    resolve('');
                })
                .catch(function (error) {
                    if (error !== undefined) {
                        logger.error(error);
                    }
                    reject(error);
                });
        });
    },

    // 所属名称
    'APPLY_ACCOUNT': function (req) {
        return Promise.resolve(req.applyInfo.rows[0].accountname);
    },

    // 申請日
    'APPLY_DATE': function (req) {
        return Promise.resolve(moment(req.applyInfo.rows[0].applydatetime).format('YYYY/MM/DD'));
    },

    // 申請ID
    'APPLY_ID': function (req) {
        return Promise.resolve(req.applyInfo.rows[0].applyid);
    },

    // 申請者
    'APPLY_PERSON': function (req) {
        return Promise.resolve(req.applyInfo.rows[0].personname);
    },

    // 申請者のメールアドレス
    'APPLY_PERSON_MAILADDRESS': function (req) {
        return new Promise(function (resolve, reject) {
            sendMailDao.prmsSelectOPersonMailaddress([req.applyInfo.rows[0].personcd])
                .then(function (prmsSelectOPersonMailaddressRes) {
                    resolve(prmsSelectOPersonMailaddressRes.rows[0].mailaddress);
                })
                .catch(function (error) {
                    if (error !== undefined) {
                        logger.error(error);
                    }
                    reject(error);
                });
        });
    },

    // 申請名称
    'APPLY_TITLE': function (req) {
        return Promise.resolve(req.applyInfo.rows[0].applytitle);
    },

     // TODO 直前の添付ファイル名
    'ATTACHMENT': function () {
        return Promise.resolve([]);
    },

    // TODO すべての添付ファイル名
    'ATTACHMENT_ALL': function () {
        return Promise.resolve([]);
    },

    // TODO 直前のコメント（コメント本文のみ）
    'COMMENT': function () {
        return Promise.resolve('');
    },

    // TODO コメント履歴全て
    'COMMENT_ALL': function () {
        return Promise.resolve('');
    },

    // 申請書名
    'FORM': function (req) {
        return Promise.resolve(req.applyInfo.rows[0].applyformname);
    },

    // メール送信タスクの前のタスクのプロセス名称
    'TASK': function (req) {
        return new Promise(function (resolve, reject) {
            processDao.prmsSelectProcessNameByTaskId(req)
                .then(function (prmsSelectProcessNameByTaskIdRes) {
                    resolve(prmsSelectProcessNameByTaskIdRes.rows[0].processname);
                })
                .catch(function (error) {
                    if (error !== undefined) {
                        logger.error(error);
                    }
                    reject(error);
                });
        });
    },

    // 次のタスクのプロセス名称
    'TASK_NEXT': function (req) {
        return new Promise(function (resolve, reject) {
            var getProcessName,
                i,
                promises;
            getProcessName = function (taskId) {
                return new Promise(function (resolve) {
                    processDao.prmsSelectProcessNameByTaskId({taskId: taskId})
                        .then(function (prmsSelectProcessNameByTaskIdRes) {
                            resolve(prmsSelectProcessNameByTaskIdRes.rows[0].processname);
                        });
                });
            };
            promises = [];
            for (i = 0; i < req.nextTasks.rowCount; i += 1) {
                promises.push(getProcessName(req.nextTasks.rows[i].taskid));
            }
            Promise.all(promises)
                .then(function (res) {
                    resolve(res);
                })
                .catch(function (error) {
                    if (error !== undefined) {
                        logger.error(error);
                    }
                    reject(error);
                });
        });
    },

    // 特定のタスク（プロセスCDで指定）のプロセス名称
    'TASK_PROCESSCD': function (req) {
        return new Promise(function (resolve, reject) {
            taskDao.prmsGetTaskIdByProcessCd({
                applyId: req.applyId,
                processCd: req.processCd
            })
                .then(function (prmsGetTaskIdByProcessCdRes) {
                    logger.info(prmsGetTaskIdByProcessCdRes);
                    return processDao.prmsSelectProcessNameByTaskId({taskId: prmsGetTaskIdByProcessCdRes.rows[0].taskid});
                })
                .then(function (prmsSelectProcessNameByTaskIdRes) {
                    logger.info(prmsSelectProcessNameByTaskIdRes);
                    resolve(prmsSelectProcessNameByTaskIdRes.rows[0].processname);
                })
                .catch(function (error) {
                    if (error !== undefined) {
                        logger.error(error);
                    }
                    reject(error);
                });
        });
    },

    // TODO メール送信タスクの前のタスクの実行者
    'PRACTITIONER': function () {
        return Promise.resolve('');
    },

    // TODO メール送信タスクの前のタスクの実行者の所属
    'PRACTITIONER_ACCOUNT': function () {
        return Promise.resolve('');
    },

    // TODO メール送信タスクの前のタスクの実行者のメールアドレス
    'PRACTITIONER_MAILADDRESS': function () {
        return Promise.resolve('');
    },

    // TODO 次のタスクの処理者(複数次のタスクがあって、同じ社員が存在する)
    'PROCESSOR_NEXT': function (req) {
        return new Promise(function (resolve, reject) {

            Promise.resolve()
                .then(function () {
                    var i,
                        promises;
                    promises = [];
                    for (i = 0; i < req.nextTasks.rowCount; i += 1) {
                        promises.push(assembleProcessorinfo.prmsGetIntensivePersonProcessorByTaskId(req.nextTasks.rows[i]));
                    }
                    return Promise.all(promises);
                })
                .then(function (res) {
                    var getPersonName,
                        j,
                        k,
                        promises2;
                    promises2 = [];
                    logger.info(res);
                    getPersonName = function (personCd) {
                        return new Promise(function (resolve, reject) {
                            logger.info(personCd);
                            personinfoDao.prmsGetPersonName({personCd: personCd})
                                .then(function (prmsGetPersonNameRes) {
                                    resolve(prmsGetPersonNameRes.rows[0].personname);
                                })
                                .catch(function (error) {
                                    if (error !== undefined) {
                                        logger.error(error);
                                    }
                                    reject(error);
                                });
                        });
                    };
                    for (k = 0; k < res.length; k += 1) {
                        for (j = 0; j < res[k].length; j += 1) {
                            promises2.push(getPersonName(res[k][j]));
                        }
                    }
                    return Promise.all(promises2);
                })
                .then(function (res) {
                    logger.info(res);
                    resolve(res);
                })
                .catch(function (error) {
                    if (error !== undefined) {
                        logger.error(error);
                    }
                    reject(error);
                });
        });
    },

    // 次のタスクの処理者のメールアドレス
    'PROCESSOR_NEXT_MAILADDRESS': function (req) {
        return new Promise(function (resolve, reject) {
            var i,
                promises;
            promises = [];
            for (i = 0; i < req.nextTasks.rowCount; i += 1) {
                promises.push(assembleProcessorinfo.prmsGetIntensivePersonProcessorByTaskId(req.nextTasks.rows[i]));
            }
            Promise.all(promises)
                .then(function (res) {
                    return sendMailDao.prmsSelectOPersonMailaddress(res);
                })
                .then(function (prmsSelectOPersonMailaddressRes) {
                    var arr,
                        cnt;
                    arr = [];
                    for (cnt = 0; cnt < prmsSelectOPersonMailaddressRes.rowCount; cnt += 1) {
                        arr.push(prmsSelectOPersonMailaddressRes.rows[cnt].mailaddress);
                    }
                    resolve(arr);
                })
                .catch(function (error) {
                    if (error !== undefined) {
                        logger.error(error);
                    }
                    reject(error);
                });
        });
    },

    // 特定のタスク（プロセスCDで指定）の処理者
    'PROCESSOR_PROCESSCD': function (req) {
        return new Promise(function (resolve, reject) {
            taskDao.prmsGetTaskIdByProcessCd({
                applyId: req.applyId,
                processCd: req.processCd
            })
                .then(function (prmsGetTaskIdByProcessCdRes) {
                    return assembleProcessorinfo.prmsGetIntensivePersonProcessorByTaskId(prmsGetTaskIdByProcessCdRes.rows[0]);
                })
                .then(function (prmsGetIntensivePersonProcessorByTaskIdRes) {
                    var getPersonName,
                        i,
                        promises;
                    promises = [];
                    getPersonName = function (personCd) {
                        return new Promise(function (resolve) {
                            logger.info(personCd);
                            personinfoDao.prmsGetPersonName({personCd: personCd})
                                .then(function (prmsGetPersonNameRes) {
                                    resolve(prmsGetPersonNameRes.rows[0].personname);
                                });
                        });
                    };
                    for (i = 0; i < prmsGetIntensivePersonProcessorByTaskIdRes.length; i += 1) {
                        promises.push(getPersonName(prmsGetIntensivePersonProcessorByTaskIdRes[i]));
                    }
                    return Promise.all(promises);
                })
                .then(function (res) {
                    resolve(res);
                })
                .catch(function (error) {
                    if (error !== undefined) {
                        logger.error(error);
                    }
                    reject(error);
                });
        });
    },

    // 特定のタスク（プロセスCDで指定）の処理者のメールアドレス
    'PROCESSOR_MAILADDRESS_PROCESSCD': function (req) {
        return new Promise(function (resolve, reject) {
            taskDao.prmsGetTaskIdByProcessCd({
                applyId: req.applyId,
                processCd: req.processCd
            })
                .then(function (prmsGetTaskIdByProcessCdRes) {
                    return assembleProcessorinfo.prmsGetIntensivePersonProcessorByTaskId(prmsGetTaskIdByProcessCdRes.rows[0]);
                })
                .then(function (prmsGetIntensivePersonProcessorByTaskIdRes) {
                    return sendMailDao.prmsSelectOPersonMailaddress(prmsGetIntensivePersonProcessorByTaskIdRes);
                })
                .then(function (prmsSelectOPersonMailaddressRes) {
                    var arr,
                        cnt;
                    arr = [];
                    logger.info(prmsSelectOPersonMailaddressRes);
                    for (cnt = 0; cnt < prmsSelectOPersonMailaddressRes.rowCount; cnt += 1) {
                        arr.push(prmsSelectOPersonMailaddressRes.rows[cnt].mailaddress);
                    }
                    resolve(arr);
                })
                .catch(function (error) {
                    if (error !== undefined) {
                        logger.error(error);
                    }
                    reject(error);
                });
        });
    },

    // TODO 次のタスクの参照者
    'REFERENCE_NEXT': function (req) {
        return new Promise(function (resolve, reject) {
            var i,
                promises;
            promises = [];
            for (i = 0; i < req.nextTasks.rowCount; i += 1) {
                promises.push(assembleProcessorinfo.prmsGetIntensivePersonReferenceByTaskId(req.nextTasks.rows[i]));
            }
            Promise.all(promises)
                .then(function (res) {
                    var getPersonName,
                        j,
                        k,
                        promises2;
                    promises2 = [];
                    logger.info(res);
                    getPersonName = function (personCd) {
                        return new Promise(function (resolve) {
                            logger.info(personCd);
                            personinfoDao.prmsGetPersonName({personCd: personCd})
                                .then(function (prmsGetPersonNameRes) {
                                    resolve(prmsGetPersonNameRes.rows[0].personname);
                                });
                        });
                    };
                    for (k = 0; k < res.length; k += 1) {
                        for (j = 0; j < res[k].length; j += 1) {
                            promises2.push(getPersonName(res[k][j]));
                        }
                    }
                    return Promise.all(promises2);
                })
                .then(function (res) {
                    logger.info(res);
                    resolve(res);
                })
                .catch(function (error) {
                    if (error !== undefined) {
                        logger.error(error);
                    }
                    reject(error);
                });
        });
    },

    // TODO 次のタスクの参照者のメールアドレス
    'REFERENCE_NEXT_MAILADDRESS': function (req) {
        return new Promise(function (resolve, reject) {
            var i,
                promises;
            promises = [];
            for (i = 0; i < req.nextTasks.rowCount; i += 1) {
                promises.push(assembleProcessorinfo.prmsGetIntensivePersonReferenceByTaskId(req.nextTasks.rows[i]));
            }
            Promise.all(promises)
                .then(function (res) {
                    return sendMailDao.prmsSelectOPersonMailaddress(res);
                })
                .then(function (prmsSelectOPersonMailaddressRes) {
                    var arr,
                        cnt;
                    arr = [];
                    for (cnt = 0; cnt < prmsSelectOPersonMailaddressRes.rowCount; cnt += 1) {
                        arr.push(prmsSelectOPersonMailaddressRes.rows[cnt].mailaddress);
                    }
                    resolve(arr);
                })
                .catch(function (error) {
                    if (error !== undefined) {
                        logger.error(error);
                    }
                    reject(error);
                });
        });
    },

    // 特定のタスク（プロセスCDで指定）の参照者
    'REFERENCE_PROCESSCD': function (req) {
        return new Promise(function (resolve, reject) {
            taskDao.prmsGetTaskIdByProcessCd({
                applyId: req.applyId,
                processCd: req.processCd
            })
                .then(function (prmsGetTaskIdByProcessCdRes) {
                    return assembleProcessorinfo.prmsGetIntensivePersonReferenceByTaskId(prmsGetTaskIdByProcessCdRes.rows[0]);
                })
                .then(function (prmsGetIntensivePersonReferenceByTaskIdRes) {
                    var getPersonName,
                        i,
                        promises;
                    promises = [];
                    getPersonName = function (personCd) {
                        return new Promise(function (resolve) {
                            logger.info(personCd);
                            personinfoDao.prmsGetPersonName({personCd: personCd})
                                .then(function (prmsGetPersonNameRes) {
                                    resolve(prmsGetPersonNameRes.rows[0].personname);
                                });
                        });
                    };
                    for (i = 0; i < prmsGetIntensivePersonReferenceByTaskIdRes.length; i += 1) {
                        promises.push(getPersonName(prmsGetIntensivePersonReferenceByTaskIdRes[i]));
                    }
                    return Promise.all(promises);
                })
                .then(function (res) {
                    resolve(res);
                })
                .catch(function (error) {
                    if (error !== undefined) {
                        logger.error(error);
                    }
                    reject(error);
                });
        });
    },

    // 特定のタスク（プロセスCDで指定）の参照者のメールアドレス
    'REFERENCE_MAILADDRESS_PROCESSCD': function (req) {
        return new Promise(function (resolve, reject) {
            taskDao.prmsGetTaskIdByProcessCd({
                applyId: req.applyId,
                processCd: req.processCd
            })
                .then(function (prmsGetTaskIdByProcessCdRes) {
                    return assembleProcessorinfo.prmsGetIntensivePersonReferenceByTaskId(prmsGetTaskIdByProcessCdRes.rows[0]);
                })
                .then(function (prmsGetIntensivePersonReferenceByTaskIdRes) {
                    return sendMailDao.prmsSelectOPersonMailaddress(prmsGetIntensivePersonReferenceByTaskIdRes);
                })
                .then(function (prmsSelectOPersonMailaddressRes) {
                    var arr,
                        cnt;
                    arr = [];
                    logger.info(prmsSelectOPersonMailaddressRes);
                    for (cnt = 0; cnt < prmsSelectOPersonMailaddressRes.rowCount; cnt += 1) {
                        arr.push(prmsSelectOPersonMailaddressRes.rows[cnt].mailaddress);
                    }
                    resolve(arr);
                })
                .catch(function (error) {
                    if (error !== undefined) {
                        logger.error(error);
                    }
                    reject(error);
                });
        });
    },

    // システム日付
    'SYSTEM_DATE': function () {
        var now;
        now = new Date();
        return Promise.resolve(now.getFullYear() + '/' + ('0' + (now.getMonth() + 1)).slice(-2) + '/' + ('0' + now.getDate()).slice(-2));
    },

    // TODO この申請書へのURL
    'URL': function () {
        return Promise.resolve('http://localhost:3000/applydetail');
    },
};

/**
 * 予約語を変換
 * @param  {Object} req
 * @param  {String} text 予約語を含むテキスト
 * @param  {String} applyInfo 申請情報
 * @return {String} text 変換したのテキスト
 */
var prmsReplaceKeyword = function (req, text, applyInfo) {
    return new Promise(function (resolve, reject) {
        var arrReg,
            matchKey,
            promises,
            re;
        re = /<%([^:(%>)]+)(:([^(%>)]+))?%>/g;
        promises = [];
        matchKey = {};

        Promise.resolve()
            .then(function () {
                while (arrReg = re.exec(text)) {
                    matchKey[arrReg[0]] = promises.length;
                    logger.info(arrReg[1]);
                    promises.push(prmsReplaceFunctions[arrReg[1]]({
                        taskId: req.taskId,
                        applyId: req.applyId,
                        applyInfo: applyInfo,
                        processCd: arrReg[3],
                        nextTasks: req.nextTasks,
                    }));
                }
                return Promise.all(promises);
            })
            .then(function (prmsReplaceFunctionsRes) {
                text = text.replace(re, function (match) {
                    return prmsReplaceFunctionsRes[matchKey[match]];
                });
                resolve(text);
            })
            .catch(function (error) {
                if (error !== undefined) {
                    logger.error(error);
                }
                reject(error);
            });
    });
};

/**
 * メール送信
 * @param  {Object} req
 * @return {[type]}                [description]
 */
module.exports = function (req, client) {
    var mailOptions,
        processData,
        smtp;
    mailOptions = {
        from: [],
        to: [],
        cc: [],
        bcc: [],
        attachments: [],
        subject: '',
        text: '',
    };
    processData = {};
    return new Promise(function (resolve, reject) {
        logger.info('mail 01');
        processDataDao.prmsSelectOProcessDataByTaskId({
            taskId: req.taskId,
        }, client)
            .then(function (prmsSelectOProcessDataByTaskIdRes) {
                logger.info('mail 02');
                if (prmsSelectOProcessDataByTaskIdRes.rows[0].processdata) {
                    mailOptions = JSON.parse(prmsSelectOProcessDataByTaskIdRes.rows[0].processdata).mailOptions;
                    if (mailOptions) {
                        return applyDao.prmsGetApplyInfo(req, client);
                    }
                }
                logger.error('メール送信設定がありません。');
                return Promise.reject();
            })
            .then(function (prmsGetApplyInfoRes) {
                var applyInfo,
                    promises;
                promises = [];
                applyInfo = prmsGetApplyInfoRes;
                logger.info('mail 03');
                promises.push(prmsReplaceKeyword(req, mailOptions.from || '', applyInfo));
                promises.push(prmsReplaceKeyword(req, mailOptions.to || '', applyInfo));
                promises.push(prmsReplaceKeyword(req, mailOptions.cc || '', applyInfo));
                promises.push(prmsReplaceKeyword(req, mailOptions.bcc || '', applyInfo));
                promises.push(prmsReplaceKeyword(req, mailOptions.subject || '', applyInfo));
                promises.push(prmsReplaceKeyword(req, mailOptions.text || '', applyInfo));
                // promises.push(prmsReplaceKeyword(req, mailOptions.attachments || '', applyInfo));
                return Promise.all(promises);
            })
            .then(function (prmsReplaceKeywordRes) {
                logger.info('mail 04');
                mailOptions.from = prmsReplaceKeywordRes[0];
                mailOptions.to = prmsReplaceKeywordRes[1];
                mailOptions.cc = prmsReplaceKeywordRes[2];
                mailOptions.bcc = prmsReplaceKeywordRes[3];
                mailOptions.subject = prmsReplaceKeywordRes[4];
                mailOptions.text = prmsReplaceKeywordRes[5];
                // mailOptions.attachments = prmsReplaceKeywordRes[6];
                mailOptions.attachments = [];
                return Promise.resolve(mailOptions);
            })
            .then(function (res) {
                logger.info('mail 05');
                if (res.from.length > 0) {
                    mailOptions.from = res.from.split(',');
                }
                if (res.to.length > 0) {
                    mailOptions.to = res.to.split(',');
                }
                if (res.cc.length > 0) {
                    mailOptions.cc = res.cc.split(',');
                }
                if (res.bcc.length > 0) {
                    mailOptions.bcc = res.bcc.split(',');
                }
                // if (res.attachments.length > 0) {
                //     mailOptions.attachments = res.attachments.split(',');
                // }
                logger.info(mailOptions);
                if (mailOptions.to.length === 0 && mailOptions.cc.length === 0) {
                    logger.error('メール送信先がありません。');
                    return Promise.reject();
                }
                return attachmentDao.prmsGetLatestAttachmentInfo(req);
            })
            .then(function (prmsGetAttachmentInfoRes) {
                var i;
                for (i = 0; i < prmsGetAttachmentInfoRes.rowCount; i += 1) {
                    mailOptions.attachments.push({
                        path: prmsGetAttachmentInfoRes.rows[i].filepath,
                        filename: prmsGetAttachmentInfoRes.rows[i].filename,
                    });
                }

                //SMTP接続・送信
                smtp = nodemailer.createTransport(
                    appConfig.mailSetting //SMTPの設定はconfigファイルにて指定
                );
                smtp.sendMail(mailOptions, function (err) {
                    //SMTP切断
                    smtp.close();
                    if (err) {
                        logger.error(err);
                        logger.error('メールを送信出来ませんでした。');
                        // reject(err);
                    } else {
                        logger.info('メールを送信しました。');
                        // resolve(info);
                    }
                });
            })
            .then(function (res) {
                logger.info('send-mail finish');
                resolve(res);
            })
            .catch(function (error) {
                logger.info('send-mail error');
                if (error !== undefined) {
                    logger.error(error);
                }
                reject(error);
            });
    });
};
