/*global $, window*/
/*jslint browser: true */
$(document).ready(function () {
    'use strict';
    var w;
    $('#login').on('click', function (event) {
        var $form;
        event.preventDefault();
        event.stopPropagation();
        if (w && w.closed === false) {
            w.close();
        }
        w = window.open('', 'bcwf');
        $form = $('<form />', {
            action: './login',
            target: 'bcwf',
            method: 'post',
            style: 'display: none;'
        });
        $form.append($('<input />', {
            type: 'hidden',
            name: 'userId',
            value: $('#userId').val()
        }));
        $form.append($('<input />', {
            type: 'hidden',
            name: 'password',
            value: $('#password').val()
        }));
        $form.append($('<input />', {
            type: 'hidden',
            name: 'pageid',
            value: $('#pageid').val()
        }));
        $('body').append($form);
        $form.submit();
        $form.remove();
        return false;
    });
});
