/*global $ */
/*jslint browser: true */
$(document).ready(function () {
    'use strict';
    var applyDetail,
        curFiles,
        deleteFileIds,
        fileHtml,
        isChanged,
        isChangedAttach,
        proceed;
    curFiles = [];
    deleteFileIds = [];
    Array.prototype.push.apply(curFiles, JSON.parse($('#temp').val()));

    // アコーディオン付加
    $('.panel-group').on('hide.bs.collapse show.bs.collapse', '.panel-collapse', function () {
        $(this).prev().find('span').toggleClass('span-down-arrow');
    });

    // 日付機能付加
    $('.input-date').datepicker({
        buttonImage: './images/calendar-icon.png',
        buttonImageOnly: true,
        changeMonth: true,
        changeYear: true,
        dateFormat: 'yy/mm/dd',
        showAnim: '',
        showButtonPanel: true,
        showOn: 'button',
        showOtherMonths: true
    });

    // ダイアログ
    $('#close-dialog').dialog({
        autoOpen: false,
        modal: true,
        title: '閉じる確認',
        width: 450,
        buttons: [
            {
                text: 'キャンセル',
                class: 'cancel-btn',
                click: function () {
                    $(this).dialog('close');
                }
            },
            {
                text: 'OK',
                class: 'ok-btn',
                click: function () {
                    isChanged = false;
                    window.open('about:blank', '_self').close();
                    $(this).dialog('close');//念のため
                }
            }
        ]
    });
    $('#unsaved-dialog').dialog({
        autoOpen: false,
        modal: true,
        title: '保存確認',
        width: 450,
        buttons: [
            {
                text: 'OK',
                class: 'ok-btn',
                click: function () {
                    $('#saveApplyBtn').click();
                    $(this).dialog('close');
                }
            },
            {
                text: 'キャンセル',
                class: 'cancel-btn',
                click: function () {
                    $(this).dialog('close');
                }
            }
        ]
    });
    $('#save-dialog').dialog({
        autoOpen: false,
        modal: true,
        title: '保存完了',
        width: 450,
        buttons: [
            {
                text: 'OK',
                class: 'ok-btn',
                click: function () {
                    applyDetail();
                    $(this).dialog('close');//念のため
                }
            }
        ]
    });
    $('#action-dialog').dialog({
        autoOpen: false,
        modal: true,
        title: 'アクション完了',
        width: 450,
        buttons: [
            {
                text: 'OK',
                class: 'ok-btn',
                click: function () {
                    window.opener.postMessage('reload a original page', '*');
                    window.open('about:blank', '_self').close();
                    $(this).dialog('close');//念のため
                }
            }
        ]
    });

    // アクションのボタン
    $('.actionButton, .do-apply').on('click', function () {
        if (isChanged) {
            $('#unsaved-dialog').dialog('open');
        } else {
            proceed();
        }
    });

    proceed = function () {
        var strData;
        strData = 'applyId=' + $('#applyId').html() + '&' +
            'taskId=' + $('#activeTask').html() + '&' +
            'personCd=' + $('#loginPersonCd').html();

        $.ajax({
            url: '/api/proceed',
            type: 'post',
            data: strData,
            success: function (res) {
                console.log(res);
            },
            error: function (err) {
                console.log(err);
            }
        });
        $('#action-dialog').dialog('open');
    };

    applyDetail = function () {
        var now,
            w,
            $form;
        event.preventDefault();
        event.stopPropagation();
        now = new Date();
        if (w && w.closed === false) {
            w.close();
        }
        w = window.open('', '_bcwf');
        $form = $('<form />', {
            action: '/applydetail',
            target: '_bcwf',
            method: 'post',
            style: 'display: none;'
        });
        $form.append($('<input />', {
            type: 'hidden',
            name: 'accessToken',
            value: '1'
        }));
        $form.append($('<input />', {
            type: 'hidden',
            name: 'applyId',
            value: $('#applyId').html()
        }));
        $form.append($('<input />', {
            type: 'hidden',
            name: 'personCd',
            value: $('#loginPersonCd').html()
        }));
        $form.append($('<input />', {
            type: 'hidden',
            name: 'baseDate',
            value: now.getFullYear() + '/' + ('0' + (now.getMonth() + 1)).slice(-2) + '/' + ('0' + now.getDate()).slice(-2)
        }));
        $('body').append($form);
        $form.submit();
        $form.remove();
        return false;
    };

    // 閉じるボタン
    $('.do-close').on('click', function () {
        if (isChanged) {
            $('#close-dialog').dialog('open');
        } else {
            window.open('about:blank', '_self').close();
        }
    });

    // 画面閉じられる
    $(window).on('beforeunload', function () {
        if (isChanged) {
            return '入力内容は保存されていません';
        }
    });

    // 入力内容の更新
    $('input, select, textarea, #uploadText').on('change', function () {
        isChanged = true;
    });

    // アップロードイメージをクリック
    $('#uploadFile').on('click', function () {
        $('#uploadText').click();
    });

    // ファイルをアップロード
    $('#uploadText').change(function () {
        var fileArr,
            tmpFiles;
        tmpFiles = [];
        fileArr = $(this)[0].files[0];
        tmpFiles.push(fileArr);
        Array.prototype.push.apply(tmpFiles, curFiles);
        curFiles = tmpFiles;
        isChangedAttach = true;
        fileHtml();
    });

    // 保存ボタン押下
    $('#saveApplyBtn').on('click', function () {
        var formData,
            i,
            j,
            now;
        now = new Date();
        formData = new FormData();
        for (i = 0, j = curFiles.length; i < j; i += 1) {
            if (curFiles[i].fileId === undefined) {
                formData.append('fileUpload[]', curFiles[i]);
            }
        }
        formData.append('applyId', $('#applyId').text());
        formData.append('personCd', $('#personCd').val());
        formData.append('deleteFileIds', $('#deleteFiles').val());
        formData.append('applyDate', $('#applyDate').val());
        formData.append('saveDate', now.getFullYear() + '/' + ('0' + (now.getMonth() + 1)).slice(-2) + '/' + ('0' + now.getDate()).slice(-2) + ' ' + ('0' + now.getHours()).slice(-2) + ':' + ('0' + now.getMinutes()).slice(-2) + ':' + ('0' + now.getSeconds()).slice(-2));
        formData.append('applyTitle', $('#applyNm').val());
        // formData.append('accountCd', $('#accountCd option:selected').val());
        formData.append('accountCd', '1');
        formData.append('comment', $('.txtarea').val());
        formData.append('taskId', $('#taskIdForSave').val());
        formData.append('action', '保存');
        formData.append('isChangedAttach', isChangedAttach);
        $.ajax({
            url: '/api/saveapply',
            type: 'post',
            enctype: 'multipart/form-data',
            data: formData,
            cache: false,
            processData: false,
            contentType: false,
            success: function (rs) {
                isChanged = false;
                isChangedAttach = false;
                console.log(rs);
                $('#save-dialog').dialog('open');
            },
            error: function (err) {
                console.log(err);
            }
        });
    });

    // 削除ボタン押下
    $('#deleteBtn').on('click', function () {
        var formData;
        formData = new FormData();
        formData.append('applyId', $('#applyId').text());
        $.ajax({
            url: '/api/delete',
            type: 'post',
            enctype: 'multipart/form-data',
            data: formData,
            cache: false,
            processData: false,
            contentType: false,
            success: function (rs) {
                console.log(rs);
            },
            error: function (err) {
                console.log(err);
            }
        });
    });

    // 取下、却下ボタン押下
    $('#takeDownBtn').on('click', function () {
        var formData;
        formData = new FormData();
        formData.append('applyId', $('#applyId').text());
        $.ajax({
            url: '/api/cancle',
            type: 'post',
            enctype: 'multipart/form-data',
            data: formData,
            cache: false,
            processData: false,
            contentType: false,
            success: function (rs) {
                console.log(rs);
            },
            error: function (err) {
                console.log(err);
            }
        });
    });

    // 添付削除処理
    $(document).on('click', '.close', function () {
        var index;
        index = this.children[2].value;
        if (curFiles[index].fileId !== undefined) {
            deleteFileIds.push(curFiles[index].fileId);
        }
        curFiles.splice(index, 1);
        isChanged = true;
        isChangedAttach = true;
        fileHtml();
    });

    // 添付一覧作成
    fileHtml = function () {
        var a,
            addHtml,
            addHtml2,
            button,
            div,
            i,
            index,
            input,
            item,
            label,
            strFilePath,
            span,
            sysdate;
        $('#attchment').empty();
        for (i = curFiles.length - 1; i >= 0; i -= 1) {
            sysdate = new Date();
            addHtml = $('<div />');
            if (i % 2 === 0) {
                addHtml.addClass('row row-color');
            } else {
                addHtml.addClass('row');
            }
            if (curFiles[i].fileId === undefined) {
                // 画面で追加データ
                item = curFiles[i];
                // 添付日付
                div = $('<div />');
                div.addClass('col-5 col-md-3 order-md-1  fixed-datetime-width');
                label = $('<label />', {text: sysdate.getFullYear() + '/' + ('0' + (sysdate.getMonth() + 1)).slice(-2) + '/' + ('0' + sysdate.getDate()).slice(-2) + ' ' + ('0' + sysdate.getHours()).slice(-2) + ':' + ('0' + sysdate.getMinutes()).slice(-2)});
                label.addClass('comment-line-height div-margin-bottom');
                div.append(label);
                addHtml.append(div);
                // 添付者
                div = $('<div />');
                div.addClass('col col-md-3 order-md-2');
                label = $('<label />', {text: $('#personName').val()});
                label.addClass('comment-line-height div-margin-bottom');
                div.append(label);
                addHtml.append(div);
                // 削除ボタン
                div = $('<div />');
                div.addClass('col-2 col-md-1 order-md-4');
                button = $('<button />', {type: 'button', 'aria-label': 'Close'});
                button.addClass('close close-btn');
                span = $('<span />', {text: '×', 'aria-hidden': 'true'});
                span.addClass('comment-line-height div-margin-bottom');
                input = $('<input />', {type: 'hidden', 'value': ''});
                button.append(span);
                button.append(input);
                input = $('<input />', {type: 'hidden', 'value': i});
                button.append(input);
                div.append(button);
                addHtml.append(div);
                // リンク
                div = $('<div />');
                div.addClass('col-12 col-md order-md-3');
                label = $('<label />', {text: item.name});
                label.addClass('comment-line-height div-margin-bottom');
                div.append(label);
                addHtml.append(div);
            } else {
                // DBからのデータ
                // 添付日付
                div = $('<div />');
                div.addClass('col-5 col-md-3 order-md-1  fixed-datetime-width');
                label = $('<label />', {title: curFiles[i].attachDate, text: curFiles[i].attachDate});
                label.addClass('comment-line-height div-margin-bottom');
                div.append(label);
                addHtml.append(div);
                // 添付者
                div = $('<div />');
                div.addClass('col col-md-3 order-md-2');
                label = $('<label />', {title: curFiles[i].personName, text: curFiles[i].personName});
                label.addClass('comment-line-height div-margin-bottom');
                div.append(label);
                addHtml.append(div);
                // 削除ボタン
                div = $('<div />');
                div.addClass('col-2 col-md-1 order-md-4');
                button = $('<button />', {type: 'button', 'aria-label': 'Close'});
                button.addClass('close close-btn');
                span = $('<span />', {text: '×', 'aria-hidden': 'true'});
                span.addClass('comment-line-height div-margin-bottom');
                input = $('<input />', {type: 'hidden', 'value': curFiles[i].fileId});
                button.append(span);
                button.append(input);
                input = $('<input />', {type: 'hidden', 'value': i});
                button.append(input);
                div.append(button);
                addHtml.append(div);
                // リンク
                index = curFiles[i].filePath.indexOf('uploads\\');
                strFilePath = curFiles[i].filePath.slice(index);
                div = $('<div />');
                div.addClass('col-12 col-md order-md-3');
                a = $('<a />', {href: '..\\' + strFilePath, download: curFiles[i].fileName, text: curFiles[i].fileName});
                a.addClass('comment-line-height div-margin-bottom');
                div.append(a);
                addHtml.append(div);
            }
            $('#attchment').prepend(addHtml);
        }
        // 削除データ
        addHtml2 = $('<div />');
        div = $('<div />');
        div.addClass('hidden');
        input = $('<input />', {type: 'hidden', id: 'deleteFiles', 'value': deleteFileIds});
        div.append(input);
        addHtml2.append(div);
        $('#attchment').prepend(addHtml2);
    };
});
