/*global $, window*/
/*jslint browser: true */
$(document).ready(function () {
    'use strict';
    var serverURL = 'http://localhost:3000';
    $.post(serverURL + '/api/getApplyCount', function (data) {
        $('#activeApplyCount').html(data.activeApplyCount.count);
        $('#approveCount').html(data.approveCount.count);
        $('#browseCount').html(data.browseCount.count);
    });
});
