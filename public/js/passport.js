var
    fs = require('fs')
  , passport = require('passport')
  , SamlStrategy = require('passport-saml').Strategy
;

passport.serializeUser(function(user, done) {
  done(null, user);
});
passport.deserializeUser(function(user, done) {
  done(null, user);
});

passport.use(new SamlStrategy({
    entryPoint: 'https://server-2012.mugen.com/adfs/ls/idpinitiatedsignon.aspx',
    issuer: 'https://192.168.18.85:3011',
    callbackUrl: 'https://192.168.18.85:3011/adfsResponse',
    identifierFormat: null,
    validateInResponseTo: false,
    disableRequestedAuthnContext: true
},
function(profile, done) {
    return done(null,profile);
}
));

module.exports = passport;