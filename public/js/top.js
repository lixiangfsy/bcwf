/*global $, window*/
/*jslint browser: true */
$(document).ready(function () {
    'use strict';
    var applyList,
        datepicker,
        getApplyCount,
        ISO8601toYMD,
        w;
    $('#processOne, #processTwo, #processThree, #processFour').on('click', function (event) {
        var id,
            now,
            $form,
            processNum;
        id = $(this).prop('id');
        if (id === 'processOne') {
            processNum = 1;
        } else if (id === 'processTwo') {
            processNum = 2;
        } else if (id === 'processThree') {
            processNum = 3;
        } else if (id === 'processFour') {
            processNum = 4;
        }
        event.preventDefault();
        event.stopPropagation();
        now = new Date();
        if (w && w.closed === false) {
            w.close();
        }
        w = window.open('', '_bcwf');
        $form = $('<form />', {
            action: './applydetail',
            target: '_bcwf',
            method: 'post',
            style: 'display: none;'
        });
        $form.append($('<input />', {
            type: 'hidden',
            name: 'accessToken',
            value: '1'
        }));
        $form.append($('<input />', {
            type: 'hidden',
            name: 'applyId',
            value: $(this).attr('href')
        }));
            $form.append($('<input />', {
            type: 'hidden',
            name: 'userId',
            value: 'fengshulx'
        }));
        $form.append($('<input />', {
            type: 'hidden',
            name: 'baseDate',
            value: now.getFullYear() + '/' + ('0' + (now.getMonth() + 1)).slice(-2) + '/' + ('0' + now.getDate()).slice(-2)
        }));
        // 以下は新規作成用
        $form.append($('<input />', {
            type: 'hidden',
            name: 'applyPersonCd',
            value: ''
        }));
        $form.append($('<input />', {
            type: 'hidden',
            name: 'applyFormCd',
            value: processNum
        }));
        $form.append($('<input />', {
            type: 'hidden',
            name: 'applyTitle',
            value: '諸届名－新規作成'
        }));
        $form.append($('<input />', {
            type: 'hidden',
            name: 'accountCd',
            value: '1'//画面上部のプルダウンで選択されている部署
        }));
        $('body').append($form);
        $form.submit();
        $form.remove();
        return false;
    });

    datepicker = function () {
        $( "#datepicker" ).datepicker({
          changeMonth: true,
          changeYear: true
        });
    };
    $('.datepicker').datepicker();

    ISO8601toYMD = function (ISO8601) {
        var date;
        date = new Date(ISO8601);
        return date.getFullYear() + '/' + ('0' + (date.getMonth() + 1)).slice(-2) + '/' + ('0' + date.getDate()).slice(-2) + ' ' + ('0' + date.getHours()).slice(-2) + ':' + ('0' + date.getMinutes()).slice(-2);
    };

    applyList = function () {
        $.post('./api/searchApply', {dateStart: $('#dateStart').val(), dateEnd: $('#dateEnd').val(), processname: $('#processname').val(), applytitle: $('#applytitle').val()}, function (data) {
            $('#bcwf-apply-list').html('');
            var i;
            for (i = 0; i < data.rowCount; i += 1) {
                $('#bcwf-apply-list').append('<tr><td>' + ISO8601toYMD(data.rows[i].applydatetime) + '</td><td><a href="' + data.rows[i].applyid + '" class="apply-list">' + data.rows[i].applytitle + '</a></td><td>' + data.rows[i].processname + '</td></tr>');
            }
            $('.apply-list, #new-apply').on('click',function (event) {
                var now,
                    $form;
                event.preventDefault();
                event.stopPropagation();
                now = new Date();
                if (w && w.closed === false) {
                    w.close();
                }
                w = window.open('', '_bcwf');
                $form = $('<form />', {
                    action: './applydetail',
                    target: '_bcwf',
                    method: 'post',
                    style: 'display: none;'
                });
                $form.append($('<input />', {
                    type: 'hidden',
                    name: 'accessToken',
                    value: '1'
                }));
                $form.append($('<input />', {
                    type: 'hidden',
                    name: 'applyId',
                    value: $(this).attr('href')
                }));
                    $form.append($('<input />', {
                    type: 'hidden',
                    name: 'userId',
                    value: 'fengshulx'
                }));
                $form.append($('<input />', {
                    type: 'hidden',
                    name: 'baseDate',
                    value: now.getFullYear() + '/' + ('0' + (now.getMonth() + 1)).slice(-2) + '/' + ('0' + now.getDate()).slice(-2)
                }));
                // 以下は新規作成用
                $form.append($('<input />', {
                    type: 'hidden',
                    name: 'applyPersonCd',
                    value: ''
                }));
                $form.append($('<input />', {
                    type: 'hidden',
                    name: 'applyFormCd',
                    value: 'Doc0003'
                }));
                $form.append($('<input />', {
                    type: 'hidden',
                    name: 'applyTitle',
                    value: '諸届名－新規作成'
                }));
                $form.append($('<input />', {
                    type: 'hidden',
                    name: 'accountCd',
                    value: '1'//画面上部のプルダウンで選択されている部署
                }));
                $('body').append($form);
                $form.submit();
                $form.remove();
                return false;
            });
        });
    };
    $('#searchApply').on('click', function (event) {
        applyList();
    });
    applyList();
    getApplyCount = function () {
        $.post('./api/getApplyCount', function (data) {
            $('#activeApplyCount').html(data.activeApplyCount.count);
            $('#approveCount').html(data.approveCount.count);
            $('#browseCount').html(data.browseCount.count);
        });
    };
    getApplyCount();
});
