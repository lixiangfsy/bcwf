(function () {
    'use strict';
    var app,
        assert,
        webBuilder,
        Builder,
        By,
        url,
        driver;

    app = require('../app.js');
    assert = require('assert');
    webBuilder = require('selenium-webdriver');
    Builder = webBuilder.Builder;
    By = webBuilder.By;
    url = 'http://localhost:3000/';
    driver = new Builder().forBrowser('chrome').build();

    before(function (done) {
        app.listen(3000);
        process.on('unhandledRejection', console.dir);
        done();
    });

    after(function (done) {
        done();
        return driver.quit();
    });

    describe('テスト①:トークン認証失敗', function () {
        'use strict';
        var errorMessage;
        this.timeout(15000);
        errorMessage = '';
        it('トークン情報がない。', async () => {
            await driver.get(url + '');
            await assert.equal(currentUrl, url + 'login');
        });
    });
}
